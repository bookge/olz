---
title: Windows+Subversion+ReviewBoard实现SVN项目管理与代码审核
tags:
  - windows
  - svn
categories:
  - svn
abbrlink: '3221'
date: 2018-02-05 16:52:47
---
## svnadmin 安装
>Svn Admin是一个Java开发的管理Svn服务器的项目用户的web应用。安装好Svn服务器端好，把Svn Admin部署好，就可以通过web浏览器管理Svn的项目，管理项目的用户，管理项目的权限。使得管理配置Svn简便，再也不需要每次都到服务器手工修改配置文件
>环境需求 MySQL Subversion Tomcat Java

### 下载jsvnadmin
>百度搜资源或[下载链接(要翻墙)](https://storage.googleapis.com/google-code-archive-downloads/v2/code.google.com/jsvnadmin/svnadmin-3.0.5.zip)
>解压`svnadmin-3.0.5.zip`到桌面

### 安装mysql
>安装过程中勾选添加环境变量到Path,防火墙放行3306端口

    User：   root
    passwd：  root

### 配置mysql

#### 登录数据库,查看字符集

    mysql -uroot -proot
    show variables like 'character%';

#### 修改字符集
>打开数据库安装目录下的`my.ini`文件

    [client]
    default-character-set=utf8
    
    [mysql]
    default-character-set=utf8
    
    [mysqld]
    default-character-set=utf8
    
>重启mysql服务[`计算机`→`管理`→`配置`→`服务`→`MySQL`→`重启动`]重新连接mysql查看字符集
    
#### 创建数据库
>创建数据库，导入svnadmin数据库

```mysql
# 创建数据库
create database if not exists svnadmin default character set utf8;

# 创建用户
create user 'svnadmin'@'localhost' identified by 'svnadmin';

# 给用户授权
grant all privileges on svnadmin.* to 'svnadmin'@'localhost';

# 选择svnadmin数据库
use svnadmin;

# 导入svnadmin数据库
source C:\Users\Administrator\Desktop\svnadmin-3.0.5\db\mysql5.sql

# 导入汉化语言
source C:\Users\Administrator\Desktop\svnadmin-3.0.5\db\lang\en.sql
```

### 安装Jdk

>`计算机`→`属性`→`高级系统设置`→`环境变量`→`系统环境变量`
>配置环境变量(创建环境变量`JAVA_HOME`,`CLASSPATH`,`Path变量新增路径`)[配置环境变量参考](https://jingyan.baidu.com/article/3c343ff70bc6ea0d377963df.html)

```cmd
JAVA_HOME=C:\Program Files (x86)\Java\jdk1.7.0_13
CLASSPATH=%JAVA_HOME%\lib\dt.jar;%JAVA_HOME%\lib\tools.jar
Path=;%JAVA_HOME%\bin;%JAVA_HOME%\jre\bin
```

### 安装配置apache-tomcat

>下载`apache-tomcat`解压到C盘[下载地址](http://mirrors.shu.edu.cn/apache/tomcat/tomcat-7/v7.0.84/bin/apache-tomcat-7.0.84.zip)
>将桌面`svnadmin-3.0.5`文件夹中的`svnadmin.war`,复制`apache-tomcat`的`webapps`目录[C:\apache-tomcat-7.0.84\webapps]
>打开`C:\apache-tomcat-7.0.84\bin`目录,双击`startup.bat`启动`apache-tomcat`

#### 数据库配置
>关闭`Tomcat`CMD窗口,打开`C:\apache-tomcat-7.0.84\webapps\svnadmin\WEB-INF`修改`jdbc.properties`

```properties
# 选择数据库类型 
db=MySQL

#MySQL; mysql://127.0.0.1:3306/svnadmin代表连接本地3306端口的svnadmin数据库
MySQL.jdbc.driver=com.mysql.jdbc.Driver
MySQL.jdbc.url=jdbc:mysql://127.0.0.1:3306/svnadmin?characterEncoding=utf-8
MySQL.jdbc.username=svnadmin
MySQL.jdbc.password=svnadmin
```
>打开`C:\apache-tomcat-7.0.84\bin`目录,双击`startup.bat`启动`apache-tomcat`
>打开浏览器输入`http://127.0.0.1:8080/svnadmin/`初始化`svnadmin`

### 安装Subversion
>默认安装直接下一步

#### 创建SVN仓库

```cmd
md D:\svndata
svnadmin create D:\svndata\svndemo
```

#### 配置svndemo仓库权限
>编辑`D:\svndata\svndemo\conf\svnserve.conf`文件;以下参数定义：

    anon-access: 非授权用户的访问权限, 有三种方式：none:限制访问, read:只读, write:读写权限，默认为 read
    auth-access: 授权用户的访问权限，有三种方式：none:限制访问, read:只读, write:读写权限，默认为 write
    password-db: 定义保存用户名和密码的文件名称, 这里为 passwd, 和该文件位于同一目录
    authz-db: 定义保存授权信息的文件名称，这里为 authz ，和该文件位于同一目录
    realm ：定义客户端连接是的“认证命名空间”， Subversion 会在认证提示里显示，并且作为凭证缓存的关键字

```conf
[general]
anno-access = read
auth-access = write
password-db = passwd
authz-db = authz
realm = My First svndemo
```

#### 配置账户
>编辑`D:\svndata\svndemo\conf\passwd`文件;记注意配置前面不能有空格,user = passwd

```conf
[users]
admin = admin
```

#### 配置授权信息

>参数定义：

    groups: 中指出了按分组来管理用户,目前只有一个用户admin,再加的话就在第二行,按user = passwd的格式.
    [/svndemo]: 中指出了这个项目的根版本库,对admin是可读,可写的
    [svndemo:/test]: 对test这个项目,admin同样是可读,可写的
    *=: 除了admin外,其他人都没权限.

```conf
[groups]
admin = admin

[/svndemo]
admin = rw
[respoity:/test]
@admin=rw
*=
```

#### 启动服务

    svnserve -d -r D:\svndata


#### 创建服务
    sc create svn binpath= "C:\Program Files (x86)\Subversion\bin\svnserve.exe --service -r D:\svndata" displayname= "Subversion 服务" depend= Tcpip


## 安装ReviewBoard
>ReviewBoard是一个开源工具，用于帮助源代码，文档，图像等的同行评审过程。它是基于网络的，可扩展的，可以使用预先提交的审查和提交后的审查方法与各种各样的环境和源代码管理系统一起工作。
>环境需求 Apache MySQL Python(MySQL-python;patch;setuptools;ReviewBoard)


### 安装软件

    httpd-2.2.21-win32: 安装完成后浏览器打开 http://127.0.0.1 显示It works!则安装成功
    python-2.7.10: 安装时勾选添加到系统环境变量;CMD窗口输入python --version测试命令是否有效
    setuptools: 默认安装
    patch-2.5.9-7: 安装路径C:\GnuWin32安装完成后手动将路径C:\GnuWin32\bin添加到Path系统变量
    memcached-win64-1.4.4-14: 解压到C盘根目录CMD进入该目录,执行memcached.exe -d install安装服务
    MySQL-python-1.2.5: 默认安装
    pycrypto-2.6.1: 选择安装到本地路径指向python 安装目录 C:\Python27\
    mod_wsgi-win32-ap22py27-3.3.so: 将此文件复制到,apache模块目录C:\Program Files (x86)\Apache Software Foundation\Apache2.2\modules更名为mod_wsgi.so
    python-memcached:   CMD: pip install python-memcached  #python2.7.10 默认已经安装此模块
    pip:  CMD: pip install -U pip  -i  https://pypi.tuna.tsinghua.edu.cn/simple 更新pip
    ReviewBoard:   CMD: pip install ReviewBoard -i  https://pypi.tuna.tsinghua.edu.cn/simple

### 配置数据库

```mysql
# 登录数据库
mysql -uroot -proot

# 查看字符集(如有问题更改my.ini)
show variables like 'character%';

# 创建数据库
create database reviewboard character set utf8;

# 创建用户
create user 'reviewboard'@'localhost' identified by 'reviewboard';

# 给用户授权
grant all privileges on reviewboard.* to 'reviewboard'@'localhost';
```

### 创建站点

    rb-site install C:\reviews      # 将站点文件生成到 C:\reviews
       Domain Name: audit.onewtech.com              [localhost] （站点域名）
       Root Path [/]:
       Database Type: 1                             [mysql] (数据库类型)
       Database Name [reviewboard]:                 [reviewboard] (数据库名)
       Database Server [localhost]: 127.0.0.1       [127.0.0.1] (使用localhost可能出现创建站点失败)
       Database Username: reviewboard               [reviewboard] (连接数据库的用户名)
       Database Password:                           [reviewboard] (连接数据库的密码)
       Confirm Database Password:                   [reviewboard] (确认密码)
       Memcache Server [localhost:11211]:       (memcache端口号)
       Username [admin]:      (web UI用户)
       Password:                                     [admin]   (web UI密码)
       Password:                                     [admin]   (确认密码)
       E-Mail Address: xiongjunfeng@onewtech.com     [xiongjunfeng@onewtech.com] (邮箱)
       Company/Organization Name (optional): ReviewBoard    [ReviewBoard] (站点名称)
       Allow us to collect support data? [Y/n]: Y     [Y]
       
### 配置apache

#### 虚拟主机文件配置

>打开`C:\reviews\conf`将`apache-wsgi.conf`文件内容复制到,`apache`虚拟主机配置文件`C:\Program Files (x86)\Apache Software Foundation\Apache2.2\conf\extra\httpd-vhosts.conf`

```bash
NameVirtualHost *:80

<VirtualHost *:80>
    ServerName audit.onewtech.com
    DocumentRoot "C:/reviews/htdocs"

    # Error handlers
    ErrorDocument 500 /errordocs/500.html

    WSGIPassAuthorization On
    WSGIScriptAlias "/" "C:/reviews/htdocs/reviewboard.wsgi/"

    <Directory "C:/reviews/htdocs">
        AllowOverride All
        Options -Indexes +FollowSymLinks
        Allow from all
    </Directory>

    # Prevent the server from processing or allowing the rendering of
    # certain file types.
    <Location "/media/uploaded">
        SetHandler None
        Options None

        AddType text/plain .html .htm .shtml .php .php3 .php4 .php5 .phps .asp
        AddType text/plain .pl .py .fcgi .cgi .phtml .phtm .pht .jsp .sh .rb

        <IfModule mod_php5.c>
            php_flag engine off
        </IfModule>

        # Force all uploaded media files to download.
        <IfModule mod_headers.c>
            Header set Content-Disposition "attachment"
        </IfModule>
    </Location>

    # Alias static media requests to filesystem
    Alias /media "C:/reviews/htdocs/media"
    Alias /static "C:/reviews/htdocs/static"
    Alias /errordocs "C:/reviews/htdocs/errordocs"
    Alias /favicon.ico "C:/reviews/htdocs/static/rb/images/favicon.png"
</VirtualHost>
```

#### apache配置文件    
>`C:\Program Files (x86)\Apache Software Foundation\Apache2.2\conf\httpd.conf`

    # 添加mod_wsgi模块(60行添加)
    LoadModule wsgi_module modules/mod_wsgi.so
    
    # 取消注释(470行)
    Include conf/extra/httpd-vhosts.conf

### 启动服务
>`计算机`→`管理`→`配置`→`服务`→`apache`→`重启/重新启动` 配置成开机自动启动
>`计算机`→`管理`→`配置`→`服务`→`memcached`→`重启/重新启动` 配置成开机自动启动


### web管理
>hosts文件写入 `127.0.0.1 audit.onewtech.com`
>浏览器打开`http://audit.onewtech.com` 用户名`admin`; 密码`admin`