title: OpenStack集群安装-00_虚拟机初始化
tags:
  - openstack
copyright: true
categories:
  - openstack
abbrlink: openstack-00
date: 2018-07-18 17:43:26
---
### 创建虚拟机

#### 网络配置

> VMware Workstation→编辑→虚拟网络编辑器

    VMnet0：
        类型：桥接模式  桥接到本地能正常上网的物理网卡
    
    VMnet8:
        类型：NAT模式 关闭DHCP服务 子网IP 10.0.0.0/24 默认网关(不用修改) 10.0.0.2

#### 创建虚拟机配置如下

>系统安装成初次进入系统后,验证能上外网, 关机, 创建快照


    SystemType :  CentOS 7 64位
    CPU        :  数量 1 内核 4
    Memory     :  4G
    NetWork    :  VMnet0  ens32
    NetWork2   :  VMnet8  ens33
    Image      :  CentOS-7-x86_64-Minimal-1708.iso
    TimeZone   :  Asia/Shanghai
    ens32      :  IP:192.168.0.10/24  GW:192.168.0.1  DNS:223.5.5.5
    ens33      :  IP:10.0.0.10/24     GW:10.0.0.2     DNS:223.5.5.5
    rootps     :  redhat


### 网络规划

    controller1  :  ens32：192.168.10.11/24    ens33：10.0.0.11/24
    controller2  :  ens32：192.168.10.12/24    ens33：10.0.0.12/24
    controller3  :  ens32：192.168.10.13/24    ens33：10.0.0.13/24
    nfs          :  ens32：192.168.10.21/24    ens33：10.0.0.21/24
    cinder1      :  ens32：192.168.10.21/24    ens33：10.0.0.21/24
    computer01   :  ens32：192.168.10.31/24    ens33：10.0.0.31/24
    computer02   :  ens32：192.168.10.32/24    ens33：10.0.0.32/24

### 克隆虚拟机

>克隆虚拟机, 更改主机名,网络配置


#### 克隆虚拟机为 controller1

>打开`controller1`电源 SSH连接主机 192.168.0.10 修改 主机名, IP参数

    # 更改主机名
    hostnamectl set-hostname controller1
    
    # 删除网卡配置文件,重新生成配置
    NetName=ens32
    rm -f /etc/sysconfig/network-scripts/ifcfg-$NetName
    nmcli con add con-name $NetName ifname $NetName autoconnect yes type ethernet ip4 192.168.0.11/24 ipv4.dns "223.5.5.5 1.1.1.1" ipv4.gateway "192.168.0.1"
    nmcli connection reload
    
    # 删除网卡配置文件,重新生成配置
    NetName=ens33
    rm -f /etc/sysconfig/network-scripts/ifcfg-$NetName
    nmcli con add con-name $NetName ifname $NetName autoconnect yes type ethernet ip4 10.0.0.11/24 ipv4.dns "223.5.5.5 1.1.1.1" ipv4.gateway "10.0.0.2"
    nmcli connection reload
    
    # 关闭虚拟机创建快照
    
#### 克隆虚拟机为 controller2

>打开`controller1`电源 SSH连接主机 192.168.0.10 修改 主机名, IP参数

    # 更改主机名
    hostnamectl set-hostname controller2
    
    # 删除网卡配置文件,重新生成配置
    NetName=ens32
    rm -f /etc/sysconfig/network-scripts/ifcfg-$NetName
    nmcli con add con-name $NetName ifname $NetName autoconnect yes type ethernet ip4 192.168.0.12/24 ipv4.dns "223.5.5.5 1.1.1.1" ipv4.gateway "192.168.0.1"
    nmcli connection reload
    
    # 删除网卡配置文件,重新生成配置
    NetName=ens33
    rm -f /etc/sysconfig/network-scripts/ifcfg-$NetName
    nmcli con add con-name $NetName ifname $NetName autoconnect yes type ethernet ip4 10.0.0.12/24 ipv4.dns "223.5.5.5 1.1.1.1" ipv4.gateway "10.0.0.2"
    nmcli connection reload
    
    # 关闭虚拟机创建快照

#### 克隆虚拟机为 controller3

>打开`controller3`电源 SSH连接主机 192.168.0.10 修改 主机名, IP参数

    # 更改主机名
    hostnamectl set-hostname controller3
    
    # 删除网卡配置文件,重新生成配置
    NetName=ens32
    rm -f /etc/sysconfig/network-scripts/ifcfg-$NetName
    nmcli con add con-name $NetName ifname $NetName autoconnect yes type ethernet ip4 192.168.0.13/24 ipv4.dns "223.5.5.5 1.1.1.1" ipv4.gateway "192.168.0.1"
    nmcli connection reload
    
    # 删除网卡配置文件,重新生成配置
    NetName=ens33
    rm -f /etc/sysconfig/network-scripts/ifcfg-$NetName
    nmcli con add con-name $NetName ifname $NetName autoconnect yes type ethernet ip4 10.0.0.13/24 ipv4.dns "223.5.5.5 1.1.1.1" ipv4.gateway "10.0.0.2"
    nmcli connection reload
    
    # 关闭虚拟机创建快照

#### 克隆虚拟机为 nfs

> 打开nfs电源 SSH连接主机 192.168.0.10 修改 主机名, IP参数

    # 更改主机名
    hostnamectl set-hostname nfs

    # 删除网卡配置文件,重新生成配置
    NetName=ens32
    rm -f /etc/sysconfig/network-scripts/ifcfg-$NetName
    nmcli con add con-name $NetName ifname $NetName autoconnect yes type ethernet ip4 192.168.0.20/24 ipv4.dns "223.5.5.5 1.1.1.1" ipv4.gateway "192.168.0.1"
    nmcli connection reload

    # 删除网卡配置文件,重新生成配置
    NetName=ens33
    rm -f /etc/sysconfig/network-scripts/ifcfg-$NetName
    nmcli con add con-name $NetName ifname $NetName autoconnect yes type ethernet ip4 10.0.0.20/24 ipv4.dns "223.5.5.5 1.1.1.1" ipv4.gateway "10.0.0.2"
    nmcli connection reload

    # 关闭虚拟机创建快照

#### 克隆虚拟机为 cinder1

>此节点为存储 编辑配置添加 1块100G硬盘 后打开`cinder1`电源 SSH连接主机 192.168.0.10 修改 主机名, IP参数

    # 更改主机名
    hostnamectl set-hostname cinder1
    
    # 删除网卡配置文件,重新生成配置
    NetName=ens32
    rm -f /etc/sysconfig/network-scripts/ifcfg-$NetName
    nmcli con add con-name $NetName ifname $NetName autoconnect yes type ethernet ip4 192.168.0.21/24 ipv4.dns "223.5.5.5 1.1.1.1" ipv4.gateway "192.168.0.1"
    nmcli connection reload
    
    # 删除网卡配置文件,重新生成配置
    NetName=ens33
    rm -f /etc/sysconfig/network-scripts/ifcfg-$NetName
    nmcli con add con-name $NetName ifname $NetName autoconnect yes type ethernet ip4 10.0.0.21/24 ipv4.dns "223.5.5.5 1.1.1.1" ipv4.gateway "10.0.0.2"
    nmcli connection reload
    
    # 关闭虚拟机创建快照

#### 克隆虚拟机为 computer01

>打开`computer01`电源 SSH连接主机 192.168.0.10 修改 主机名, IP参数

    # 更改主机名
    hostnamectl set-hostname computer01
    
    # 删除网卡配置文件,重新生成配置
    NetName=ens32
    rm -f /etc/sysconfig/network-scripts/ifcfg-$NetName
    nmcli con add con-name $NetName ifname $NetName autoconnect yes type ethernet ip4 192.168.0.31/24 ipv4.dns "223.5.5.5 1.1.1.1" ipv4.gateway "192.168.0.1"
    nmcli connection reload
    
    # 删除网卡配置文件,重新生成配置
    NetName=ens33
    rm -f /etc/sysconfig/network-scripts/ifcfg-$NetName
    nmcli con add con-name $NetName ifname $NetName autoconnect yes type ethernet ip4 10.0.0.31/24 ipv4.dns "223.5.5.5 1.1.1.1" ipv4.gateway "10.0.0.2"
    nmcli connection reload
    
    # 关闭虚拟机创建快照

#### 克隆虚拟机为 computer02

>打开`computer02`电源 SSH连接主机 192.168.0.10 修改 主机名, IP参数

    # 更改主机名
    hostnamectl set-hostname computer02
    
    # 删除网卡配置文件,重新生成配置
    NetName=ens32
    rm -f /etc/sysconfig/network-scripts/ifcfg-$NetName
    nmcli con add con-name $NetName ifname $NetName autoconnect yes type ethernet ip4 192.168.0.32/24 ipv4.dns "223.5.5.5 1.1.1.1" ipv4.gateway "192.168.0.1"
    nmcli connection reload
    
    # 删除网卡配置文件,重新生成配置
    NetName=ens33
    rm -f /etc/sysconfig/network-scripts/ifcfg-$NetName
    nmcli con add con-name $NetName ifname $NetName autoconnect yes type ethernet ip4 10.0.0.32/24 ipv4.dns "223.5.5.5 1.1.1.1" ipv4.gateway "10.0.0.2"
    nmcli connection reload
    
    # 关闭虚拟机创建快照
