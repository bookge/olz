title: 腾讯对象存储
author: 亦 漩
abbrlink: 139a
tags:
  - 腾讯cos
  - python
  - coscmd
  - 数据备份
categories:
  - 腾讯cos
date: 2018-06-28 13:01:00
---
### 备份数据到腾讯对象存储

> [官方文档](https://cloud.tencent.com/document/product/436/12269)

#### 安装python-pip

    yum -y install python-pip

#### 安装cos-sdk

    pip install -U cos-python-sdk-v5

#### 创建简单上传脚本

    mkdir /usr/local/cos
    cat <<'EOF' >/usr/local/cos/cos.upload.py
    #!/bin/env python
    # -*- coding=utf-8
    from qcloud_cos import CosConfig
    from qcloud_cos import CosS3Client
    from qcloud_cos import CosServiceError
    from qcloud_cos import CosClientError

    import sys
    import logging

    logging.basicConfig(level=logging.INFO, stream=sys.stdout)

    if ( len(sys.argv) > 5 ):
        region     = sys.argv[1].decode('utf-8')
        secret_id  = sys.argv[2].decode('utf-8')
        secret_key = sys.argv[3].decode('utf-8')
        bucket     = sys.argv[4].decode('utf-8')
        domain     = sys.argv[5].decode('utf-8')
        filePath   = sys.argv[6].decode('utf-8')
        fileName   = filePath.split("/")[-1]
    else:
        print("Example: python %s region secret_id secret_key Bucket RemoteDirectory LocalFile.zip" % sys.argv[0])
        exit()

    token = ''                 # 使用临时秘钥需要传入Token，默认为空,可不填

    config = CosConfig(Region=region, Secret_id=secret_id, Secret_key=secret_key, Token=token)  # 获取配置对象
    client = CosS3Client(config)

    response = client.upload_file(bucket, '/%s/%s' % ( domain, fileName ), filePath)
    print response['ETag']
    EOF
    
    # 简单使用方法
    python /usr/local/cos/cos.upload.py 存储桶地区 secret_id secret_key bucket RemoteDirectory LocalFile.zip


#### shell 备份脚本

    cat <<'EOF' >/usr/local/cos/cos.sh
    #!/bin/sh
    ####################################################################
    #  Web Backup version 1.0.0 Author: Jager <ge@zhangge.net>         #
    # For more information please visit https://zhangge.net/5117.html  #
    #----------------------------------------------------------------- #
    #  Copyright ©2016 zhangge.net. All rights reserved.               #
    #  Update version 1.1.0 Auth: yixuan SDK_Version: cos-python-sdk-v5#
    ####################################################################
     
    isDel=y
    args=$#
    isDel=${!args}
    # 设置压缩包解压密码
    #mypassword=123456
    #add_pass="-P$mypassword"
     
    test -f /etc/profile && . /etc/profile >/dev/null 2>&1
    baseDir=$(dirname $(readlink `which cos`))
    #baseDir=$(cd $(dirname $0) && pwd)
    zip --version >/dev/null || yum install -y zip
    ZIP=$(which zip)
    TODAY=`date +%u`
    PYTHON=$(which python)
    MYSQLDUMP=$(which mysqldump)
     
    # 新增的COS上传文件函数,请按照实际情况修改appID，认证KEY、认证密钥和Bucket名称!!!
    uploadToCOS()
    {
        #             调用SDK路径      Bucket地域             secret_id                         secret_key                 Bucket名称    远程文件夹 本地文件
        $PYTHON $baseDir/cos.upload.py ap-shanghai AKIDUvt7MYBu7TRdnuq21z5xreTX5yKfNKhX DRqlidYPRQQMIJFB7377DvbPEGHtbM4n backup-1252143340  $1 $2
        if [[ $? -eq 0 ]] &&  [[ "$isDel" == "y" ]]
        then
            test -f $2 && rm -f $2
        fi
    }
     
    printHelp()
    {
    clear
    printf '
    =====================================Help infomation=========================================
    1. Use For Backup database:
    The $1 must be [db]
        $2: [domain]
        $3: [dbname]
        $4: [mysqluser]
        $5: [mysqlpassword]
        $6: [back_path]
        $7: [isDel]
     
    For example:cos db zhangge.net zhangge_db zhangge 123456 /home/wwwbackup/zhangge.net
     
    2. Use For Backup webfile:
    The $1 must be [file]:
        $2: [domain]
        $3: [site_path]
        $4: [back_path]
        $5: [isDel]
     
    For example:cos file zhangge.net /home/wwwroot/zhangge.net /home/wwwbackup/zhangge.net
    =====================================End of Hlep==============================================
     
    '
    exit 0
    }
     
    backupDB()
    {
        domain=$1
        dbname=$2
        mysqluser=$3
        mysqlpd=$4
        back_path=$5
        test -d $back_path || (mkdir -p $back_path || echo "$back_path not found! Please CheckOut Or feedback to zhangge.net..." && exit 2)
        cd $back_path
        #如果是要备份远程MySQL，则修改如下语句中localhost为远程MySQL地址
        $MYSQLDUMP -hlocalhost -u$mysqluser -p$mysqlpd $dbname --skip-lock-tables --default-character-set=utf8 >$back_path/$domain\_db_$TODAY\.sql
        test -f $back_path/$domain\_db_$TODAY\.sql || (echo "MysqlDump failed! Please CheckOut Or feedback to zhangge.net..." && exit 2)
        $ZIP -P$mypassword -m $back_path/$domain\_db_$TODAY\.zip $domain\_db_$TODAY\.sql && \
        uploadToCOS $domain $back_path/$domain\_db_$TODAY\.zip
    }
     
    backupFile()
    {
        domain=$1
        site_path=$2
        back_path=$3
        test -d $site_path || (echo "$site_path not found! Please CheckOut Or feedback to zhangge.net..." && exit 2)
        test -d $back_path || (mkdir -p $back_path || echo "$back_path not found! Please CheckOut Or feedback to zhangge.net..." && exit 2)
        test -f $back_path/$domain\_$TODAY\.zip && rm -f $back_path/$domain\_$TODAY\.zip
        $ZIP $add_pass -9r $back_path/$domain\_$TODAY\.zip $site_path && \
        uploadToCOS $domain $back_path/$domain\_$TODAY\.zip    
    }
     
    while [ $1 ]; do
        case $1 in
            '--db' | 'db' )
            backupDB $2 $3 $4 $5 $6
            exit
            ;;
            '--file' | 'file' )
            backupFile $2 $3 $4
            exit  
            ;;
            * )
            printHelp
            exit
            ;;
        esac
    done
    printHelp
    EOF

#### 使用方法

    # 创建软连接
    ln -s /usr/local/cos/cos.sh /usr/bin/cos
    
    # 添加执行权限
    chmod +x /usr/bin/cos
    
    # 使用方法：
        将 /usr/local/cos/cos.sh 文件中第30行中的参数修改为自己的即可只需要修改：
            Bucket地域：点击存储桶 → 基础配置 → 所属地域
            secret_id ：点击密钥管理 → 云API密钥 → SecretId
            secret_key: 点击密钥管理 → 云API密钥 → SecretKey
            Bucket名称：点击存储桶 → 基础配置 → 空间名称
    
    # 备份文件
    # file 备份文件; aria2_webui 远程目录 /tmp/aria2_webui 本地目录 /tmp 临时文件存储位置 y 备份完成后删除临时文件
    cos file aria2_webui /tmp/aria2_webui /tmp y
    
    # 备份数据库(如果需要为其他主机备份数据库请更改77行 -hlocalhost)
    # db 备份数据库 svnadmin 远程目录 svnadmin_db 数据库名 root 数据库用户名 123456 数据库用户密码 /tmp 临时文件存储位置 y 备份完成后删除临时文件
    cos db svnadmin svnadmin_db root 123456 /tmp y
    
    
    # 添加解压密码
        取消第14,15行注释 password 变量值为解压密码

### 腾讯cos对象存储ftp工具

> [官方文档](https://github.com/tencentyun/cos-ftp-server-V5)

    #下载源码
    git clone https://github.com/tencentyun/cos-ftp-server-V5.git
    
    # 安装
    cd cos-ftp-server-V5
    python setup.py install
    
    # 配置文件
    cp conf/vsftpd.conf.example conf/vsftpd.conf
    sed -i "s/cos_secretid = .*/cos_secretid = AKIDUvt7MYBu7TRdnuq21z5xreTX5yKfNKhX/" conf/vsftpd.conf
    sed -i "s/cos_secretkey = .*/cos_secretkey = DRqlidYPRQQMIJFB7377DvbPEGHtbM4n/" conf/vsftpd.conf
    sed -i "s/cos_bucket = .*/cos_bucket = backup-1252143340/" conf/vsftpd.conf
    sed -i "s/cos_region = .*/cos_region = ap-shanghai/" conf/vsftpd.conf
    
    # 创建工作目录 启动服务
    mkdir -p /home/cos_ftp/data
    python ftp_server.py
    
    # 登陆用户 conf/vsftpd.conf 第10行 login_users 变量
    user: user1   user2 
    pass: user1   pass2
    Auth: RW     RW
    
    # 访问地址
    ftp://ServerIP:2121/
    
### COSCMD 工具

>[官方文档](https://cloud.tencent.com/document/product/436/10976)

    # 安装pip 更新 pip 版本
    yum -y install python-pip
    pip install --upgrade pip
    
    # 安装coscmd 并更新
    pip install coscmd
    pip install coscmd -U
    
    # 创建默认配置文件
    cat <<'EOF' >~/.cos.conf
    [common]
    secret_id = AKIDUvt7MYBu7TRdnuq21z5xreTX5yKfNKhX
    secret_key = DRqlidYPRQQMIJFB7377DvbPEGHtbM4n
    bucket = backup-1252143340
    region = ap-shanghai
    max_thread = 5
    part_size = 1
    schema = https
    EOF
    
    # 命令使用
    # 创建bucket 后缀需要替换为自己的ID<1252143340>
    coscmd -b cos-1252143340 createbucket
    
    # 删除bucket
    coscmd -b cos-1252143340 deletebucket
    
    # 上传文件
    coscmd upload <localpath> <cospath>
    coscmd upload /home/aaa/123.txt bbb/123.txt
    coscmd upload /home/aaa/123.txt bbb/
    
    # 上传文件夹
    # COSCMD 支持大文件断点上传功能。当分片上传大文件失败时，重新上传该文件只会上传失败的分块，而不会从头开始（请保证重新上传的文件的目录以及内容和上传的目录保持一致）。
    # COSCMD 分块上传时会对每一块进行 MD5 校验。
    # COSMCD 上传默认会携带 x-cos-meta-md5 的头部，值为该文件的 md5 值。
    # 使用 -s 参数可以使用同步上传，跳过上传 md5 一致的文件(cos 上的原文件必须是由 1.8.3.2 之后的 COSCMD 上传的，默认带有 x-cos-meta-md5 的 header)。
    # 使用 -H 参数设置 HTTP header 时，请务必保证格式为 json，这里是个例子：coscmd upload -H '{"Cache-Control":"max-age=31536000","Content-Language":"zh-CN"}' <localpath> <cospath>。
    # 在上传文件夹时，使用--ignore 参数可以忽略某一类文件，支持 shell 通配规则，支持多条规则，用逗号分隔。
    # 目前只支持上传最大 40 T 的单文件。
    coscmd upload -r <localpath> <cospath>  //命令格式
    coscmd upload -r /home/aaa/ bbb/aaa  //操作示例
    coscmd upload -r /home/aaa/ bbb/  //操作示例
    coscmd upload -r /home/aaa/ /  //上传到 bucket 根目录
    coscmd upload -rs /home/aaa/ /home/aaa  //同步上传，跳过 md5 相同的文件
    coscmd upload -rs /home/aaa/ /home/aaa --ignore *.txt,*.doc //忽略 .txt 和 .doc 的后缀文件
    coscmd -b cos-1252143340 upload -r cos-ftp-server-V5/ /  /
    / 上传文件夹到指定存储桶
    
    # 下载文件
    coscmd download <cospath> <localpath>  //命令格式
    coscmd download bbb/123.txt /home/aaa/111.txt  //操作示例
    coscmd download bbb/123.txt /home/aaa/  //操作示例
    
    # 下载文件夹
    # 若本地存在同名文件，则会下载失败。使用 -f 参数覆盖本地文件。
    # download 接口使用分块下载，老版本的 mget 接口已经废除，请使用 download 接口。
    # 使用 -s 或者 --sync 参数，可以在下载文件夹时跳过本地已存在的相同文件 (前提是下载文件夹是通过 COSCMD 的 upload 接口上传的，文件携带有 x-cos-meta-md5 头部)。
    # 在下载文件夹时，使用 --ignore 参数可以忽略某一类文件，支持 shell 通配规则，支持多条规则，用逗号分隔。
    coscmd download -r <cospath> <localpath> //命令格式
    coscmd download -r /home/aaa/ bbb/aaa  //操作示例
    coscmd download -r /home/aaa/ bbb/  //操作示例
    coscmd download -rf / bbb/aaa  //覆盖下载当前bucket根目录下所有的文件
    coscmd download -rs / bbb/aaa  //同步下载当前bucket根目录下所有的文件，跳过md5校验相同的文件
    coscmd download -rs / bbb/aaa --ignore *.txt,*.doc //忽略.txt和.doc的后缀文件
     coscmd -b cos-1252143340 download -r cos-ftp-server-V5/ /tmp/111 // 下载
    指定存储桶中的文件夹到本地
    
    # 删除文件
    coscmd delete <cospath>  //命令格式
    coscmd delete bbb/123.txt  //操作示例
    
    # 删除文件夹命
    coscmd delete -r <cospath>  //命令格式
    coscmd delete -r bbb/  //操作示例
    coscmd delete -r /  //操作示例
    
    # 复制文件
    coscmd copy <sourcepath> <cospath>  //命令格式
    coscmd copy bucket-appid.cos.ap-guangzhou.myqcloud.com/a.txt aaa/123.txt  //操作示例
    
    # 复制文件夹
    coscmd copy -r <sourcepath> <cospath>  //命令格式
    coscmd copy -r bucket-appid.cos.ap-guangzhou.myqcloud.com/coscmd/ aaa //操作示例
    coscmd copy -r bucket-appid.cos.ap-guangzhou.myqcloud.com/coscmd/ aaa/ //操作示例
    
    # 打印文件列表
    # 使用-a打印全部文件。
    # 使用 -r 递归打印，并且会在末尾返回列出文件的数量和大小之和。
    # 使用 -n num 设置打印数量的最大值。
    coscmd list <cospath>  //命令格式
    coscmd list -a //操作示例
    coscmd list bbb/123.txt  -r -n 10 //操作示例
    
    # 显示文件信息
    coscmd info <cospath>  //命令格式
    coscmd info bbb/123.txt //操作示例
    
    # 获取带签名的下载 URL
    coscmd sigurl <cospath>  //命令格式
    coscmd signurl bbb/123.txt //操作示例
    coscmd signurl bbb/123.txt -t 100//操作示例
