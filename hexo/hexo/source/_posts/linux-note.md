---
title: Linux笔记不定期更新
tags:
  - shell
  - note
copyright: true
categories:
  - shell
abbrlink: note
date: 2018-08-06 00:51:49
---

## 账号安全

### 创建用户指定uid:1200; 自动建立用户的登入目录; 更改用户密码

    useradd feng -u 1200 -m
    echo $pass | passwd --stdin feng

### 禁止 root 用户远程 SSH 登录(line 38)

    sed -i 's/#PermitRootLogin yes/PermitRootLogin no/' /etc/ssh/sshd_config
    systemctl restart sshd

### 配置用户 feng 使用 sudo 权限

> NOPASSWD 使用 sudo 时无需输入密码(详情请执行 visudo 查看示例)

    ssh feng@192.168.0.2
    su - root
    echo "feng ALL=(ALL) ALL" | sudo tee /etc/sudoers.d/feng
    # echo "feng ALL=(ALL) NOPASSWD: ALL" | sudo tee /etc/sudoers.d/feng

### 更改 SSH 登录端口号(line 17  默认此行是被注释则登录端口为：22)

    sed -i 's/^#Port.*/Port 2200/' /etc/ssh/sshd_config
    systemctl restart sshd
    ssh -p 2200 feng@192.168.0.2

## 防火墙

### firewalld

> --permanent永久生效，没有此参数重启后失效[来自](https://www.cnblogs.com/hubing/p/6058932.html)
    
    # 查看区域信息
    firewall-cmd --get-active-zones
    
    # 查看指定接口所属区域
    firewall-cmd --get-zone-of-interface=ens33

    # 查看已启动的服务列表
    systemctl list-unit-files | grep enabled
    
    # 查看所有放行的端口
    firewall-cmd --zone=public --list-ports
    
    # 查看指定服务是否放行
    firewall-cmd --zone=public --query-service=http
    
    # 放行一个指定服务
    firewall-cmd --permanent --add-service=http
    
    # 查看指定端口是否允许
    firewall-cmd --zone=public --query-port=80/tcp
    
    # 防火墙放行 2200 端口
    firewall-cmd --zone=public --add-port=2200/tcp --permanent
    firewall-cmd --reload
    
    # 防火墙放行 80,443 端口
    firewall-cmd --zone=public --add-port=80/tcp --permanent
    firewall-cmd --zone=public --add-port=443/tcp --permanent
    firewall-cmd --reload
    
    # 删除规则
    firewall-cmd --zone=public --remove-port=443/tcp --permanent
    firewall-cmd --reload

### iptables

    待续。。。

## 端口扫描

> 扫描指定主机开放的端口  -A：使用所有高级扫描选项(命令执行时间稍长)

    yum install -y nmap
    nmap 192.168.0.2


## curl

> http下载文件
> 参数说明: -O 下载后保存为原文件名(-o filename 指定文件名); -C - 断点续传; --progress 显示进度条; --limit-rate 10M 下载速度最大不会超过 10M 每秒

    curl -O -C -  http://hostname/filename.iso --progress
---
> http上传文件

    curl --form "fileupload=@filename.txt" http://hostname/resource
---

> 从FTP服务器下载文件

    # 列出 public下的所有文件夹和文件
    curl -u user:pass -O ftp://ftp.testserver.com/public/
    
    # 下载 filename.iso
    curl -u user:pass -O ftp://ftp.testserver.com/public/filename.iso
---

> 上传文件到FTP服务器

    # 将myfile.txt文件上传到服务器
    curl -u user:pass -T file.txt ftp://ftp.testserver.com
    
    # 同时上传多个文件
    curl -u user:pass -T "{file1,file2}" ftp://ftp.testserver.com

## wget

    待续。。。


## ifconfig

> 手动配置网络参数 重启后失效

    yum install -y net-tools
    ifconfig ens34 192.168.0.22 netmask 255.255.255.0 up
    echo nameserver 202.103.24.68 >> /etc/resolv.conf
    route add default gw 192.168.2.1 dev ens32
    route add default gw 1​​92.168.0.1

> 配置子接口(多IP)

    ifconfig ens34:0 192.168.3.22 netmask 255.255.255.0 up
---

> 删除默认网关, 添加网关到指定接口,度量值为 100(越小越优先)

    ip route de​​l default
    ip route add default via 192.168.0.1
    ip route add default via 192.168.0.1 dev ens33
    ip route add default via 192.168.0.1 dev ens33 proto static metric 100
---

> 删除默认网关, 添加网关到指定接口,度量值为 100(越小越优先)

    route add default gw 192.168.0.1
    route add default gw 192.168.0.1 dev ens33
    route add default gw 192.168.0.1 dev ens33 metric 100
    
## route

> 添加默认路由

    route add -net 0.0.0.0 ens33
    route add -net 10.0.0.0/24 gw 10.0.0.2
---

> 环境 (Server1单网卡 192.168.0.2; Server2 双网卡 192.168.0.10; 10.0.0.11) 使Server1 ping 通 10.0.0.11
> 以下配置仅针对临时有效, 将相关配置设置开机启动或更改配置文件实现永久生效

    # 方法1: (Server1 只能访问10.0.0.11 这一个ip ,访问公网还是通过192.168.0.0网络的网关)
    #Server1: 添加路由
    route add -net 10.0.0.0/24 dev ens33
    route add -net 10.0.0.0/24 gw 192.168.0.22
    ----------------- END ---------------------
    
    # 方法2: (Server2开启路由,添加NAT规则; Server1 更改默认网关,本机访问外网也通过Server2, 能访问10.0.0.0 整个网络)
    #Server2: 开启路由功能, 添加NAT规则所有来自192.168.0.0/24 网络的请求 被转换为 10.0.0.11(10.0.0.11是可以上网的)
    echo 1 > /proc/sys/net/ipv4/ip_forward
    iptables -t nat -A POSTROUTING -s 192.168.0.0/24 -j SNAT --to 10.0.0.11
    
    # Server1: 更改网关为Server2同网段接口IP(192.168.0.10)重启网络(以下为手动配置请更改配置文件)
    route de​​l default
    route add default gw 192.168.0.10 dev ens33
    ----------------- END ---------------------
    
    # 方法3: (Server2开启路由,添加NAT规则; Server1 添加访问 Server2 10.0.0.0/24 网络的网关, 能访问10.0.0.0 整个网络)
    Server2:开启路由功能, 添加NAT规则所有来自192.168.0.0/24 网络的请求 被转换为 10.0.0.11(另一个网卡接口)
    echo 1 > /proc/sys/net/ipv4/ip_forward      #  echo 'net.ipv4.ip_forward = 1' >>/etc/sysctl.conf
    iptables -t nat -A POSTROUTING -s 192.168.0.0/24 -j SNAT --to 10.0.0.11
    
    # 如果请求目标是(-d DNAT)192.168.0.0网络则不用转发
    iptables -t nat -A POSTROUTING -s 192.168.0.0/24 ! -d 192.168.0.0/24 -j SNAT --to 10.0.0.11
    
    # Server1: 添加路由
    route add -net 10.0.0.0/24 gw 192.168.0.22
    ----------------- END---------------------
    
    # 方法4: (Server1, 两端的网络都能互相访问)
    #Server2
    iptables -t nat -A POSTROUTING -s 192.168.0.0/24 ! -d 192.168.0.0/24 -j SNAT --to 10.0.0.11
    iptables -t nat -A POSTROUTING -s 10.0.0.0/24 ! -d 10.0.0.0/24 -j SNAT --to 192.168.0.10
    
    # 192.168.0.0/24 网段添加路由
    route add -net 10.0.0.0/24 gw 192.168.0.22
    
    # 192.168.0.0/24 网段添加路由
    route add -net 192.168.0.0/24 gw 10.0.0.11
    ----------------- END---------------------
---

## nmcli

> 重新生成网络配置(网卡UUID也会改变)

    NetName=ens34
    rm -f /etc/sysconfig/network-scripts/ifcfg-$NetName
    nmcli con add con-name $NetName ifname $NetName autoconnect yes type ethernet ip4 192.168.0.22/24 ipv4.dns 202.103.24.68 ipv4.gateway 192.168.0.1
    nmcli connection reload
