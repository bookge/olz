title: Docker安装配置
tags:
  - docker
  - linux
copyright: true
categories:
  - docker
abbrlink: '2820'
date: 2018-05-17 17:52:16
---
### Docker 安装

#### 卸载旧版本及关联的依赖关系

```sh
yum -y remove docker \
    docker-client \
    docker-client-latest \
    docker-common \
    docker-latest \
    docker-latest-logrotate \
    docker-logrotate \
    docker-selinux \
    docker-engine-selinux \
    docker-engine
```

#### YUM方式安装

##### 安装依赖

```sh
yum -y install yum-utils \
    device-mapper-persistent-data \
    lvm2
```

##### 添加安装源

```sh
yum-config-manager \
    --add-repo \
    https://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo
```

##### 安装 Docker CE

```sh
yum makecache fast
yum -y install docker-ce
```

#### 使用脚本自动安装

> 在测试或开发环境中 Docker 官方为了简化安装流程，提供了一套便捷的安装脚本，CentOS 系统上可以使用这套脚本安装：

```sh
curl -fsSL get.docker.com -o get-docker.sh
sh get-docker.sh --mirror Aliyun
```

### 验证安装

```sh
docker -v
docker version
```

### 配置 Docker镜像加速

#### 脚本配置

```sh
mkdir /etc/docker
curl -sSL https://get.daocloud.io/daotools/set_mirror.sh | sh -s http://3272dd08.m.daocloud.io
```

#### 手动配置

```sh
# daocloud
mkdir /etc/docker
cat <<! >/etc/docker/daemon.json
{
  "registry-mirrors": ["http://3272dd08.m.daocloud.io"]
}
!

systemctl restart docker



# 清华源
mkdir /etc/docker
cat <<! >/etc/docker/daemon.json
{
  "registry-mirrors": ["https://docker.mirrors.ustc.edu.cn/"]
}
!

systemctl restart docker
```

### 服务管理

#### 启动服务开机自启动

```sh
systemctl start docker
systemctl enable docker
```

### Docker


> [15个Docker要点](https://www.hi-linux.com/posts/25523.html)