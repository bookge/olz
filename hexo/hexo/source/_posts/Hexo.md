title: Hexo安装
tags:
  - Hexo安装
  - Hexo优化
categories:
  - Hexo
abbrlink: '3e61'
date: 2017-12-08 11:35:50
---
## 摘要

>Hexo 是一个快速、简洁且高效的博客框架。`Hexo` 使用 `Markdown`（或其他渲染引擎）解析文章，在几秒内，即可利用靓丽的主题生成静态网页.
>[Tips:]本文中所有Hexo命令都是在/Hexo目录下执行,否则或出现报错信息,或者使用 hexo --cwd /Hexo <command> 指定在某个路径执行.

## 安装 Git

    yum -y install git-core

## 安装 Node.js

   ### 安装 Wget,nvm

    yum -y install wget
    wget -qO- https://raw.github.com/creationix/nvm/master/install.sh | sh

   ### 重启终端,安装 `Node.js`

    su - root
    nvm install stable

## 安装 Hexo

    npm --registry https://registry.npm.taobao.org install -g hexo-cli

## 创建站点
>在`/Hexo`目录内创建站点

    hexo init /Hexo
    cd /Hexo
    npm install

## 安装 NexT 主题
>执行命令

    git clone https://github.com/iissnan/hexo-theme-next /Hexo/themes/next

## 配置站点信息

   ### author
   >网站作者,修改 `/Hexo/_config.yml` `author` 的值

    sed -i "s/author:.*/author: Yi Xuan/" /Hexo/_config.yml

   ### 更改语言
   >主题语言,修改`/Hexo/_config.yml` `language` 的值对应主题文件下 `language` 文件夹内的文件名

    sed -i "s/language:.*/language: zh-Hans/" /Hexo/_config.yml

   ### 修改网站地址
   >替换网站地址(生成静态页面上传github后需要绝对链接,否则会出现无法打开链接)

    sed -i "s@url:.*@url: https://bookgh.github.io@" /Hexo/_config.yml

   ### 文章的永久链接格式
   >文章链接格式,修改`/Hexo/_config.yml` `permalink` 的值

    sed -i "s/permalink:.*/permalink: post\/:title.html/" /Hexo/_config.yml

   ### 设置主题
   >修改`/Hexo/_config.yml` `theme` 的值对应 `/Hexo/theme/` 目录下对应的主题

    sed -i "s/theme:.*/theme: next/" /Hexo/_config.yml

   ### 开启访问统计
   >搜索配置文件`/Hexo/themes/next/_config.yml`中`busuanzi_count`设置中的`enable`

    enable: true
 
   ### 底部加入本站运行时间

```bash
sed -i '21a\  <br/><span><i class="fa fa-bomb"></i></span><span id="showDays"></span>'  /Hexo/themes/next/layout/_partials/footer.swig

cat <<EOF >>/Hexo/themes/next/layout/_partials/footer.swig

<script>
  var seconds = 1000;
  var minutes = seconds * 60;
  var hours = minutes * 60;
  var days = hours * 24;
  var years = days * 365;
  var birthDay = Date.UTC(2017,12,07,10,00,00); // 这里设置建站时间
  setInterval(function() {
    var today = new Date();
    var todayYear = today.getFullYear();
    var todayMonth = today.getMonth()+1;
    var todayDate = today.getDate();
    var todayHour = today.getHours();
    var todayMinute = today.getMinutes();
    var todaySecond = today.getSeconds();
    var now = Date.UTC(todayYear,todayMonth,todayDate,todayHour,todayMinute,todaySecond);
    var diff = now - birthDay;
    var diffYears = Math.floor(diff/years);
    var diffDays = Math.floor((diff/days)-diffYears*365);
    var diffHours = Math.floor((diff-(diffYears*365+diffDays)*days)/hours);
    var diffMinutes = Math.floor((diff-(diffYears*365+diffDays)*days-diffHours*hours)/minutes);
    var diffSeconds = Math.floor((diff-(diffYears*365+diffDays)*days-diffHours*hours-diffMinutes*minutes)/seconds);
      document.getElementById('showDays').innerHTML="本站已运行 "+diffYears+" 年 "+diffDays+" 天 "+diffHours+" 小时 "+diffMinutes+" 分钟 "+diffSeconds+" 秒";
  }, 1000);
</script>
EOF
```

   ### 关闭底部信息
   >搜索配置文件`/Hexo/themes/next/_config.yml`中`footer`→`powered`→`enable`,`footer`→`theme`

    footer:
      powered: false
      theme:
        # Theme & scheme info link (Theme - NexT.scheme).
        enable: false
      
   ### 添加动态背景

```bash
sed -i '89a\  <script type="text/javascript" src="//cdn.bootcss.com/canvas-nest.js/1.0.0/canvas-nest.min.js"></script>'  /Hexo/themes/next/layout/_layout.swig
hexo clean
hexo g
hexo s
```

   ### 配置留言板
   >在`leancloud.cn`注册账户,创建应用,在设置中获取`appid`,`appkey`,然后在配置文件`/Hexo/themes/next/_config.yml`中查找`valine`:

    valine:
      enable: true
      appid:  DWwnpdDBrLDM***Kf8OXopJu-gzGzoHsz
      appkey:  7sfOhetJ1Wf***W7MF0xdPOw

   ### 开启字数统计
   >切换到`/Hexo`安装`heox-wordcount`

    npm install hexo-wordcount --save

   >配置文件`/Hexo/themes/next/_config.yml`中搜索`post_wordcount`:

    post_wordcount:
      item_text: true
      wordcount: true
      min2read: true

## NexT主题配置

   ### 更换主题样式
   >NexT主题有四种样式`Muse`,`Mist`,`Pisces`,`Gemini` 修改`/Hexo/themes/next/_config.yml` `scheme` 的值

    sed -i "s/scheme: Muse/#scheme: Muse/" /Hexo/themes/next/_config.yml
    sed -i "s/#scheme: Gemini/scheme: Gemini/" /Hexo/themes/next/_config.yml

   ### 更改语法高亮样式
   >语法高亮样式有五种分别为`normal`,`night`,`night eighties`,`night blue`,`night bright`

    sed -i  "s/highlight_theme:.*/highlight_theme: night/" /Hexo/themes/next/_config.yml

   ### 设置头像
   >将选中的图标文件放到 `/Hexo/themes/next/source/images` 目录下，命名为`avatar.png` `/Hexo/_config.yml` 新增字段`avatar: /images/avatar.png`

    sed -i "/author: /a avatar: \/images\/avatar.png" /Hexo/_config.yml


   ### 阅读全文
   >编辑主题配置文件，修改`auto_excerpt`字段：(将enable设为true，之后每篇文章会自动在150处截断)

    sed -i '210,215 s/enable:.*/enable: true/' /Hexo/themes/next/_config.yml

   ### 添加社交链接
   >编辑`/Hexo/themes/next/_config.yml`的social字段，增加你要添加的社交名称和URL

    sed -i "s/#social:/social:/" /Hexo/themes/next/_config.yml
    sed -i "s@#GitHub:.*@GitHub: https://github.com/bookgh || github@" /Hexo/themes/next/_config.yml

## 创建页面

   ### 创建标签页面
   >执行命令

    hexo new page tags

   ### 修改 tags 页面类型
   >`/Hexo/source/tags/index.md` 文件内添加字段`type: tags` 且不显示评论`comments: false`

    sed -i "/date:/a type: tags" /Hexo/source/tags/index.md
    sed -i "/type:/a comments: false" /Hexo/source/tags/index.md


   ### 创建分类页面
   >执行命令

    hexo new page categories

   ### 修改 categories 页面类型
   >`/Hexo/source/categories/index.md` 文件内添加字段`type: categories` 且不显示评论`comments: false`

    sed -i "/date:/a type: categories" /Hexo/source/categories/index.md
    sed -i "/type:/a comments: false" /Hexo/source/categories/index.md

   ### 创建关于页面
   >执行命令

    hexo new page about

   ### 修改about 页面
   >`/Hexo/source/categories/index.md` 文件加上关于自己的简介，可随便填写

   ### 创建图片页面
   >执行命令

    hexo new page photos

   ### 修改 photos 页面类型
   >`/Hexo/source/photos/index.md` 文件内添加字段`type: photos` 增加图片展示代码

```sh
sed -i "/date:/a type: photos" /Hexo/source/photos/index.md
cat <<EOF >>/Hexo/source/photos/index.md
{% raw %}
<style>
.photo img{
    border: 1px solid #999;
    height:150px;
    width: 150px;
}
.photo li{
    margin: 10px;
    float: left;
    list-style: none;
    display: block;
}
</style>
<div class="photo">
{% endraw %}

* ![](http://oevo99fcp.bkt.clouddn.com/1fd85d5f74fe5b5ebaf6189f1cb71561.jpg)
* ![](http://oevo99fcp.bkt.clouddn.com/1fd85d5f74fe5b5ebaf6189f1cb71561.jpg)
* ![](/images/avatar.png)
* ![](http://192.168.3.3/images/avatar.png)
* ![](http://192.168.3.3/images/avatar.png)
* ![](http://192.168.3.3/images/avatar.png)
* ![](http://192.168.3.3/images/avatar.png)
* ![](http://192.168.3.3/images/avatar.png)

{% raw %}
<div style="clear:both"></div>
</div>
{% endraw %}
EOF
```

## 添加页面到导航

   ### 编辑主题配置文件，修改menu菜单下的字段 
   >取消`tags`,`about`,`categories`字段的注释,新增photos字段

    sed -i "s/#about:/about:/" /Hexo/themes/next/_config.yml
    sed -i "s/#tags:/tags:/" /Hexo/themes/next/_config.yml
    sed -i "s/#categories:/categories:/" /Hexo/themes/next/_config.yml
    sed -i "/categories:/a \ \ photos: \/photos\/ || photo" /Hexo/themes/next/_config.yml


   ### 添加导航photos的翻译
   >主题语言文件 `/Hexo/themes/next/languages/zh-Hans.yml` 在`menu`菜单下添加photos字段的翻译

    sed -i "/commonweal:/a \ \ photos: 相册" /Hexo/themes/next/languages/zh-Hans.yml

   ### 开启分享插件
   >`/Hexo/themes/next/_config.yml` 配置文件中查找`needmoreshare2`字段修改下面的两个`enable`字段的值改为`true`

    needmoreshare2:
      enable: true
      postbottom:
        enable: true


## 插件安装

   ### RSS订阅
   >`/Hexo`目录执行命令

    npm --registry https://registry.npm.taobao.org  install hexo-generator-feed --save

   ### Local Search搜索
   >`/Hexo`目录执行命令,编辑`/Hexo/themes/next/_config.yml` 修改`local_search:`菜单`enabled`的值设置为`true`

    npm install hexo-generator-search --save
    sed -i "600,610 s/enable:.*/enable: true/" /Hexo/themes/next/_config.yml

   ### GitHub集成
   >安装github集成插件,配置repo地址(自己的GitHub项目地址)

    npm install hexo-deployer-git --save
    sed -i "s/type:/type: git/" /Hexo/_config.yml
    sed -i "/type:/a \ \ repo: https://github.com/bookgh/bookgh.github.io.git" /Hexo/_config.yml
    sed -i "/repo:/a \ \ branch: master" /Hexo/_config.yml

   #### 将内容同步到GitHub
   >清理缓存(hexo clean),生成静态文件(hexo g),上传到GitHub(hexo d)
   >输入hexo d 命令后会弹出输入用户名和密码(GitHub账户名密码)

    hexo clean
    hexo d

## 启动Hexo服务

    hexo server -p 80

   ### 修改默认监听端口

    sed -i "13a server:" /Hexo/_config.yml  
    sed -i "/server:/a \ \ port: 80" /Hexo/_config.yml
    sed -i "/port:/a \ \ compress: true" /Hexo/_config.yml
    sed -i "/compress:/a \ \ header: true" /Hexo/_config.yml
    sed -i "/header:/G" /Hexo/_config.yml

   ### 设置开机自启动
   >开机启动hexo时无法找到`hexo,node`环境变量所以需要创建创建软连接
   >--cwd 指定服务启动目录

    ln -sf /root/.nvm/versions/node/v9.2.0/bin/hexo  /bin/hexo
    ln -sf /root/.nvm/versions/node/v9.2.0/bin/node  /bin/node
    echo "/usr/bin/nohup /root/.nvm/versions/node/v9.2.0/bin/hexo server --cwd /Hexo/  >out.out 2>&1 &" >>/etc/rc.d/rc.local
    chmod +x /etc/rc.d/rc.local

更多资料:
    [lifeColder](https://segmentfault.com/u/adam_home)
    [hexo的next主题个性化配置教程](https://segmentfault.com/a/1190000009544924)
    [hexo官网](https://hexo.io/docs/)
    [theme-next](http://theme-next.iissnan.com/)