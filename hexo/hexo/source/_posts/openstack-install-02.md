title: OpenStack集群安装-02_Haproxy集群高可用
tags:
  - openstack
  - haproxy
copyright: true
categories:
  - openstack
  - ''
abbrlink: openstack-02
date: 2018-07-19 11:16:00
---
    启动 controller1, controller2, controller3
    在在以上控制节点主机安装 http+corosync+pacemaker+pcs+haproxy
    

### 安装配置pscd

> 所有节点 安装集群软件,配置服务跟随系统启动,设置集群账户密码(此密码需要所有节点相同)

    for HOST in controller{1..3}; do
        echo "--------------- $HOST PSCD ---------------"
        ssh -T $HOST <<'EOF'
        # 安装 corosync, pacemaker, pcs
        yum -y install corosync pacemaker pcs fence-agents resource-agents  

        # 服务并配置其随系统启动
        systemctl enable pcsd  
        systemctl start pcsd  

        # 配置群集账户密码
        echo centos | passwd --stdin hacluster  
    EOF
    done


### 创建集群

> 验证所有控制节点主状态为：Authorized 才能开始创建集群

    pcs cluster auth -u hacluster -p centos controller1 controller2 controller3
---

> 创建集群

    pcs cluster setup --start --name my_cluster controller1 controller2 controller3

### 配置集群

> 服务管理

    pcs cluster enable --all      # 集群随系统启动
    pcs cluster start  --all      # 启动集群
    pcs cluster status            # 集群状态 所有控制节点状态为：Online
---

> 集群配置

    pcs property set stonith-enabled=false      # 禁用STONITH
    pcs property set no-quorum-policy=ignore    # 无仲裁时，选择忽略
---

> 创建 VIP 资源

    pcs resource create vip ocf:heartbeat:IPaddr2 ip=10.0.0.10 cidr_netmask=24 op monitor interval=28s
---

> 验证状态

    corosync-cfgtool -s              #验证corosync
    corosync-cmapctl | grep members  #查看成员
    pcs status corosync              #查看corosync状态

### 安装配置http

    for HOST in controller{1..3}; do
        echo "--------------- $HOST HTTPD ---------------"
        ssh -T $HOST <<EOF
        # 安装 httpd
        yum -y install httpd

        # 配置 http
        [ -f /etc/httpd/conf/httpd.conf.bak ] || cp /etc/httpd/conf/httpd.conf{,.bak}                              # 备份默认配置文件
        sed -i 's#^Listen 80#Listen 8080#'  /etc/httpd/conf/httpd.conf    # 修改监听端口
        echo "ServerName $HOST:8080" >>/etc/httpd/conf/httpd.conf    # 配置站点域名为主机名
        echo $HOST >/var/www/html/index.html                          # 生成测试页

        # 启动服务,随系统启动
        systemctl start httpd
        systemctl enable httpd
    EOF
    doen

> 验证服务状态

    curl http://controller1:8080   # controller1
    curl http://controller2:8080   # controller2
    curl http://controller3:8080   # controller3
    
### 安装配置haproxy

    for HOST in controller{1..3}; do
        echo "--------------- $HOST HAPROXY ---------------"
        ssh -T $HOST <<EOF
        # 安装 haproxy
        yum -y install haproxy

        # 允许没VIP时启动(如果没有配置此项且应用会导致 haproxy 服务无法启动)
        echo "net.ipv4.ip_nonlocal_bind = 1" >>/etc/sysctl.conf
        sysctl -p

        # 备份默认配置
        [ -f /etc/haproxy/haproxy.cfg.bak] || cp /etc/haproxy/haproxy.cfg{,.bak}

        # 生成 haproxy 配置文件
        cat <<! >/etc/haproxy/haproxy.cfg
        ############ 全局配置 ############
        global
        log 127.0.0.1 local0
        log 127.0.0.1 local1 notice
        daemon
        nbproc 1       # 进程数量
        maxconn 4096   # 最大连接数
        user haproxy   # 运行用户
        group haproxy  # 运行组
        chroot /var/lib/haproxy
        pidfile /var/run/haproxy.pid

        ############ 默认配置 ############
        defaults
        log global
        mode http            # 默认模式{ tcp|http|health }
        option httplog       # 日志类别,采用httplog
        option dontlognull   # 不记录健康检查日志信息
        retries 2            # 2次连接失败不可用
        option forwardfor    # 后端服务获得真实ip
        option httpclose     # 请求完毕后主动关闭http通道
        option abortonclose  # 服务器负载很高，自动结束比较久的链接
        maxconn 4096         # 最大连接数
        timeout connect 5m   # 连接超时
        timeout client 1m    # 客户端超时
        timeout server 31m   # 服务器超时
        timeout check 10s    # 心跳检测超时
        balance roundrobin   # 负载均衡方式，轮询

        ########## 统计页面配置 ##########
        listen stats
          bind 0.0.0.0:1080
          mode http
          option httplog
          log 127.0.0.1 local0 err
          maxconn 10               # 最大连接数
          stats refresh 30s
          stats uri /admin         #状态页面 http//ip:1080/admin 访问
          stats realm Haproxy\ Statistics
          stats auth admin:admin   # 用户和密码:admin
          stats hide-version       # 隐藏版本信息  
          stats admin if TRUE      # 设置手工启动/禁用

        # haproxy web 代理配置
        ############ WEB ############
        listen dashboard_cluster  
          bind controller:80
          balance  roundrobin  
          option  tcpka  
          option  httpchk  
          option  tcplog  
          server controller1 controller1:8080 check port 8080 inter 2000 rise 2 fall 5
          server controller2 controller2:8080 check port 8080 inter 2000 rise 2 fall 5
          server controller3 controller3:8080 check port 8080 inter 2000 rise 2 fall 5
    !

        # 配置 haproxy 日志

        echo <<! >/etc/rsyslog.d/haproxy.conf
        $ModLoad imudp
        $UDPServerRun 514
        $template Haproxy,"%rawmsg% \n"
        local0.=info -/var/log/haproxy.log;Haproxy
        local0.notice -/var/log/haproxy-status.log;Haproxy
    !

        # 重启日志服务
        systemctl restart rsyslog

        # 启动haproxy ,跟随系统启动
        systemctl start haproxy.service     # 启动服务
        systemctl enable haproxy.service    # 随系统启动
        systemctl status haproxy.service    # 运行状态
    EOF
    done


### 验证集群

> 浏览器打开以下URL验证 Haproxy节点状态

    http://192.168.0.11:1080/admin 
    http://192.168.0.12:1080/admin 
    http://192.168.0.13:1080/admin

    用户名:密码 / admin:admin
---

> HA验证

    # http://10.0.0.10/ 浏览器打开此URL刷新可以看到服务器轮询切换

### 使用脚本

    site='http://home.onlycloud.xin'
    wget $site/code/openstack-02_haproxy-cluster.sh -O openstack-ha.sh
    sh openstack-ha.sh
