title: Mysql源码编译安装
author: 亦 漩
abbrlink: '1e53'
tags:
  - mysql
  - linux
categories:
  - mysql
date: 2018-05-03 10:31:00
---
### 系统环境

    系统：CentOS7.4-1708-Mini
    源码路径：/usr/local/src
    安装路径：/usr/local
    依赖软件：cmake, ncurses, m4, bison
    优化：jemalloc(内存分配器)

### 准备工作

#### 基础环境

    yum -y install gcc gcc-c++ wget bzip2 vim ntpdate

#### 修改时区

    rm -f /etc/localtime
    ln -sf /usr/share/zoneinfo/Asia/Shanghai /etc/localtime

#### 关闭selinux

    setenforce 0
    sed -i 's/SELINUX=.*/SELINUX=disabled/' /etc/selinux/config
    

#### 时间同步

    ntpdate ntp.aliyun.com && hwclock -w
    echo "*/20 * * * * $(which ntpdate) pool.ntp.org > /dev/null 2>&1 && $(which hwclock) -w" >> /var/spool/cron/root
    chmod 600 /var/spool/cron/root

### 安装依赖环境

>官方文档[源码安装Mysql](https://dev.mysql.com/doc/refman/5.7/en/source-installation.html)

> Cmake: mysql使用cmake跨平台工具预编译源码，用于设置mysql的编译参数. [下载源](https://cmake.org/files/)

    cd
    wget -c https://cmake.org/files/v3.11/cmake-3.11.1.tar.gz
    tar xvf cmake-3.11.1.tar.gz -C /usr/local/src/
    cd /usr/local/src/cmake-3.11.1
    ./bootstrap && make && make install

> Ncurses: 字符终端处理库 [下载源](http://ftp.gnu.org/gnu/ncurses/)

    cd
    wget -c http://ftp.gnu.org/gnu/ncurses/ncurses-6.0.tar.gz
    tar xvf ncurses-6.0.tar.gz -C /usr/local/src/
    cd /usr/local/src/ncurses-6.0
    ./configure --with-shared --without-debug --without-ada --enable-overwrite  
    make && make install

> M4: GNU M4是传统Unix宏处理器的一个实现 [下载源](http://ftp.gnu.org/gnu/m4/)

    cd
    wget -c http://ftp.gnu.org/gnu/m4/m4-1.4.18.tar.gz
    tar xvf m4-1.4.18.tar.gz -C /usr/local/src/
    cd /usr/local/src/m4-1.4.18
    ./configure --prefix=/usr/local/m4
    make && make install
    ln -sf /usr/local/m4/bin/m4 /usr/bin/

> Bison: Linux下C/C++语法分析器 [下载源](http://ftp.gnu.org/gnu/bison/)

    cd
    wget -c http://ftp.gnu.org/gnu/bison/bison-3.0.4.tar.gz
    tar xvf bison-3.0.4.tar.gz -C /usr/local/src/
    cd /usr/local/src/bison-3.0.4
    ./configure --prefix=/usr/local/bison
    make && make install
    ln -sf /usr/local/bison/bin/* /usr/bin/
    echo '/usr/local/bison/lib/' > /etc/ld.so.conf.d/bison.conf
    ldconfig

> Jemalloc [下载源](https://github.com/jemalloc/jemalloc/releases)

    cd
    wget -c https://github.com/jemalloc/jemalloc/releases/download/5.0.1/jemalloc-5.0.1.tar.bz2
    tar xvf jemalloc-5.0.1.tar.bz2 -C /usr/local/src/
    cd /usr/local/src/jemalloc-5.0.1
    ./configure --prefix=/usr/local
    make && make install
    ln -s /usr/local/lib/libjemalloc.so.2 /usr/lib64/libjemalloc.so.1
    [ -z "`grep /usr/local/lib /etc/ld.so.conf.d/*.conf`" ] && echo '/usr/local/lib' > /etc/ld.so.conf.d/local.conf
    ldconfig

### 编译安装mysql

#### 准备工作

> 创建用户

    useradd -M -s /sbin/nologin mysql

> 定义数据路径及密码

    dbrootpwd=root
    mkdir -p /data/mysql
    mysql_data_dir=/data/mysql
    mysql_install_dir=/usr/local/mysql

#### 编译安装

> mysql源码中用到了C++的Boost库,要求必须安装boost1.59.0或以上版本.

    cd
    # 下载Boost C++
    wget -c http://mirrors.linuxeye.com/oneinstack/src/boost_1_59_0.tar.gz
    tar xvf boost_1_59_0.tar.gz -C /usr/local/src/
    # 下载 mysql
    wget -c https://dev.mysql.com/get/Downloads/MySQL-5.7/mysql-5.7.22.tar.gz
    tar xvf mysql-5.7.22.tar.gz -C /usr/local/src/
    cd /usr/local/src/mysql-5.7.22/
    # 编译
    cmake . -DCMAKE_INSTALL_PREFIX=${mysql_install_dir} \
        -DMYSQL_DATADIR=${mysql_data_dir} \
        -DSYSCONFDIR=/etc \
        -DDOWNLOAD_BOOST=1 -DWITH_BOOST=/usr/local/src/boost_1_59_0 \
        -DWITH_INNOBASE_STORAGE_ENGINE=1 \
        -DWITH_PARTITION_STORAGE_ENGINE=1 \
        -DWITH_FEDERATED_STORAGE_ENGINE=1 \
        -DWITH_BLACKHOLE_STORAGE_ENGINE=1 \
        -DWITH_MYISAM_STORAGE_ENGINE=1 \
        -DWITH_EMBEDDED_SERVER=1 \
        -DENABLE_DTRACE=0 \
        -DENABLED_LOCAL_INFILE=1 \
        -DDEFAULT_CHARSET=utf8mb4 \
        -DDEFAULT_COLLATION=utf8mb4_general_ci \
        -DEXTRA_CHARSETS=all \
        -DCMAKE_EXE_LINKER_FLAGS='-ljemalloc'
    make -j $(grep processor /proc/cpuinfo | wc -l)
    make install

#### 配置服务

    /bin/cp ${mysql_install_dir}/support-files/mysql.server  /etc/init.d/mysqld
    sed -i "s@^basedir=.*@basedir=${mysql_install_dir}@" /etc/init.d/mysqld
    sed -i "s@^datadir=.*@datadir=${mysql_data_dir}@" /etc/init.d/mysqld
    chmod +x /etc/init.d/mysqld
    chkconfig --add mysqld
    chkconfig mysqld on

#### 配置文件

```sh
cat <<EOF  >/etc/my.cnf
[client]
port = 3306
socket = /tmp/mysql.sock
default-character-set = utf8mb4

[mysql]
prompt="MySQL [\\d]> "
no-auto-rehash

[mysqld]
port = 3306
socket = /tmp/mysql.sock

basedir = ${mysql_install_dir}
datadir = ${mysql_data_dir}
pid-file = ${mysql_data_dir}/mysql.pid
user = mysql
bind-address = 0.0.0.0
server-id = 1

init-connect = 'SET NAMES utf8mb4'
character-set-server = utf8mb4

skip-name-resolve
#skip-networking
back_log = 300

max_connections = 1000
max_connect_errors = 6000
open_files_limit = 65535
table_open_cache = 128
max_allowed_packet = 500M
binlog_cache_size = 1M
max_heap_table_size = 8M
tmp_table_size = 16M

read_buffer_size = 2M
read_rnd_buffer_size = 8M
sort_buffer_size = 8M
join_buffer_size = 8M
key_buffer_size = 4M

thread_cache_size = 8

query_cache_type = 1
query_cache_size = 8M
query_cache_limit = 2M

ft_min_word_len = 4

log_bin = mysql-bin
binlog_format = mixed
expire_logs_days = 7

log_error = ${mysql_data_dir}/mysql-error.log
slow_query_log = 1
long_query_time = 1
slow_query_log_file = ${mysql_data_dir}/mysql-slow.log

performance_schema = 0
explicit_defaults_for_timestamp

#lower_case_table_names = 1

skip-external-locking

default_storage_engine = InnoDB
#default-storage-engine = MyISAM
innodb_file_per_table = 1
innodb_open_files = 500
innodb_buffer_pool_size = 64M
innodb_write_io_threads = 4
innodb_read_io_threads = 4
innodb_thread_concurrency = 0
innodb_purge_threads = 1
innodb_flush_log_at_trx_commit = 2
innodb_log_buffer_size = 2M
innodb_log_file_size = 32M
innodb_log_files_in_group = 3
innodb_max_dirty_pages_pct = 90
innodb_lock_wait_timeout = 120

bulk_insert_buffer_size = 8M
myisam_sort_buffer_size = 8M
myisam_max_sort_file_size = 10G
myisam_repair_threads = 1

interactive_timeout = 28800
wait_timeout = 28800

[mysqldump]
quick
max_allowed_packet = 500M

[myisamchk]
key_buffer_size = 8M
sort_buffer_size = 8M
read_buffer = 4M
write_buffer = 4M
EOF
```

#### 数据库配置优化

```sh
Mem=`free -m | awk '/Mem:/{print $2}'`

sed -i "s@max_connections.*@max_connections = $((${Mem}/3))@" /etc/my.cnf
if [ ${Mem} -gt 1500 -a ${Mem} -le 2500 ]; then
    sed -i 's@^thread_cache_size.*@thread_cache_size = 16@' /etc/my.cnf
    sed -i 's@^query_cache_size.*@query_cache_size = 16M@' /etc/my.cnf
    sed -i 's@^myisam_sort_buffer_size.*@myisam_sort_buffer_size = 16M@' /etc/my.cnf
    sed -i 's@^key_buffer_size.*@key_buffer_size = 16M@' /etc/my.cnf
    sed -i 's@^innodb_buffer_pool_size.*@innodb_buffer_pool_size = 128M@' /etc/my.cnf
    sed -i 's@^tmp_table_size.*@tmp_table_size = 32M@' /etc/my.cnf
    sed -i 's@^table_open_cache.*@table_open_cache = 256@' /etc/my.cnf
elif [ ${Mem} -gt 2500 -a ${Mem} -le 3500 ]; then
    sed -i 's@^thread_cache_size.*@thread_cache_size = 32@' /etc/my.cnf
    sed -i 's@^query_cache_size.*@query_cache_size = 32M@' /etc/my.cnf
    sed -i 's@^myisam_sort_buffer_size.*@myisam_sort_buffer_size = 32M@' /etc/my.cnf
    sed -i 's@^key_buffer_size.*@key_buffer_size = 64M@' /etc/my.cnf
    sed -i 's@^innodb_buffer_pool_size.*@innodb_buffer_pool_size = 512M@' /etc/my.cnf
    sed -i 's@^tmp_table_size.*@tmp_table_size = 64M@' /etc/my.cnf
    sed -i 's@^table_open_cache.*@table_open_cache = 512@' /etc/my.cnf
elif [ ${Mem} -gt 3500 ]; then
    sed -i 's@^thread_cache_size.*@thread_cache_size = 64@' /etc/my.cnf
    sed -i 's@^query_cache_size.*@query_cache_size = 64M@' /etc/my.cnf
    sed -i 's@^myisam_sort_buffer_size.*@myisam_sort_buffer_size = 64M@' /etc/my.cnf
    sed -i 's@^key_buffer_size.*@key_buffer_size = 256M@' /etc/my.cnf
    sed -i 's@^innodb_buffer_pool_size.*@innodb_buffer_pool_size = 1024M@' /etc/my.cnf
    sed -i 's@^tmp_table_size.*@tmp_table_size = 128M@' /etc/my.cnf
    sed -i 's@^table_open_cache.*@table_open_cache = 1024@' /etc/my.cnf
fi
```

#### 初始化数据库

    ${mysql_install_dir}/bin/mysqld --initialize-insecure --user=mysql --basedir=${mysql_install_dir} --datadir=${mysql_data_dir}
    chmod 600 /etc/my.cnf
    chown mysql.mysql -R ${mysql_data_dir}

#### 配置环境变量

    [ -z "$(grep ^'export PATH=' /etc/profile)" ] && echo "export PATH=${mysql_install_dir}/bin:\$PATH" >> /etc/profile
    [ -n "$(grep ^'export PATH=' /etc/profile)" -a -z "$(grep ${mysql_install_dir} /etc/profile)" ] && sed -i "s@^export PATH=\(.*\)@export PATH=${mysql_install_dir}/bin:\1@" /etc/profile
    source /etc/profile

#### 授权root

> root仅本地登陆权限

    ${mysql_install_dir}/bin/mysql -e "grant all privileges on *.* to root@'127.0.0.1' identified by \"${dbrootpwd}\" with grant option;"
    ${mysql_install_dir}/bin/mysql -e "grant all privileges on *.* to root@'localhost' identified by \"${dbrootpwd}\" with grant option;"
    ${mysql_install_dir}/bin/mysql -uroot -p${dbrootpwd} -e "reset master;"

#### 链接库

    echo "${mysql_install_dir}/lib" > /etc/ld.so.conf.d/mysql.conf
    ldconfig

#### 启动服务

    service mysqld start

### 配置防火墙

> 放行端口(--permanent永久生效，没有此参数重启后失效)

    firewall-cmd --zone=public --add-port=22/tcp --permanent
    firewall-cmd --zone=public --add-port=3306/tcp --permanent

> 重新载入

    firewall-cmd --reload

> 查看

    firewall-cmd --zone= public --query-port=22/tcp
    firewall-cmd --zone= public --query-port=3306/tcp

> 删除

    firewall-cmd --zone= public --remove-port=3306/tcp --permanent

> 启用防火墙

    systemctl start firewalld
    systemctl enable firewalld
    
