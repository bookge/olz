---
title: windows下数据库+web定时备份
tags:
  - bat
  - windows
  - mysql备份
categories:
  - windows
abbrlink: 9f5d
date: 2018-01-12 17:33:43
---

### 定期自动备份
>系统环境 Windows Server 2008 R2
>每周六晚上自动备份mysql数据库及web数据
>自动删除100天前备份的数据

#### cmd批处理
>将此文件放到`C:\` 文件名为`backup_web_mysql.bat`

```cmd
@echo off

REM ------------------------------------------------------------------------------
REM 备份web服务器,Mysql
REM date 格式化输出日期
REM mysqldump mysqldump路径及登录用户信息
REM winrar 指定winrar压缩文件路径(由于路径有空格需用引号)
REM backuppath 备份路径
REM 自动删除100天以前的目录
REM ------------------------------------------------------------------------------

REM 设置变量 date 变量获取时间 backuppath 备份路径  mysqldump 备份程序登陆 winrar 压缩软件路径
set date=%date:~0,4%-%date:~5,2%-%date:~8,2%
set backuppath="E:\File_Softwart_Backup\web_0.6_backup\%date%"
set mysqldump=D:\xampp\mysql\bin\mysqldump.exe -uuser -ppasswd
set winrar="c:\Program Files\WinRAR\WinRAR.exe"

REM 创建备份目录
mkdir %backuppath%

REM 备份mysql,压缩备份后的文件,删除sql文件
%mysqldump% -A -B > %backuppath%\mysq.sql
%winrar% a -k -r -s -m1 %backuppath%\mysql.sql.rar %backuppath%\mysq.sql
del %backuppath%\*.sql

REM 备份网页数据
%winrar% a -k -r -s -m1 %backuppath%\htdocs.rar  D:\xampp\htdocs

REM 指定待删除文件的存放路径
set SrcDir=E:\File_Softwart_Backup\web_0.6_backup

REM 指定天数
set DaysAgo=100


REM 测试删除文件   (删除echo)  forfiles /p %SrcDir% /s /m *.* /d -%DaysAgo% /c "cmd /c echo del /f /q /a @path"
REM 测试删除文件夹 (删除echo)  forfiles /p %SrcDir% /s /m * /d -%DaysAgo% /c "cmd /c echo rd /s /q  @path"
forfiles /p %SrcDir% /s /m * /d -%DaysAgo% /c "cmd /c rd /s /q  @path"
```

#### 计划任务

打开`服务器管理器`→`配置`→`任务计划程序`→`任务计划程序库`

![服务器管理器](/images/backup_mysq_web_01.png)


输入计划任务`名称`;点击`更改用户或组`输入运行此批处理文件的账户;勾选`不管用户是否登陆都要运行`

![创建计划](/images/backup_mysq_web_02.png)

点击`触发器`选择`开始任务类型` 选择任务执行时间及周期;勾选左下角`启用`

![](/images/backup_mysq_web_03.png)

点击`操作`→`新建`选择触发任务类型

![](/images/backup_mysq_web_04.png)

`设置`

![](/images/backup_mysq_web_05.png)
