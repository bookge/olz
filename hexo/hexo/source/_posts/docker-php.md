title: Docker php环境
author: 亦 漩
abbrlink: a9ac
tags:
  - docker
  - docker web
  - docker nginx
  - docker php
categories:
  - docker
date: 2018-06-11 10:53:00
---
### 安装Docker环境

[coding source](https://coding.net/u/yx571304/p/Docker/git/tree/master/docker-lnmp)

#### dockr环境快速安装

    curl https://coding.net/u/yx571304/p/Docker/git/raw/master/install.sh | sh


#### 安装Docker ce

##### 卸载旧版

    yum -y remove docker docker-client docker-client-latest \
      docker-common docker-latest docker-latest-logrotate docker-engine

##### 安装依赖

    yum -y install yum-utils device-mapper-persistent-data lvm2

##### 添加安装源

    yum-config-manager --add-repo https://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo


##### 安装Docker CE

    yum makecache fast
    yum -y install docker-ce

##### 配置 Docker镜像加速

    mkdir /etc/docker
    cat <<! >/etc/docker/daemon.json
    {
      "registry-mirrors": ["http://3272dd08.m.daocloud.io"]
    }
    !

##### 启动服务

    systemctl start docker
    systemctl enable docker

##### 验证安装

    docker -v
    docker version

---

#### 安装Docker compose

##### 获取最新版本号

    new_ver=$(curl -s https://api.github.com/repos/docker/compose/releases | grep -o '"tag_name": ".*"' |head -n 1| sed 's/"//g;s/v//g' | sed 's/tag_name: //g')

##### 安装 docker compose

    curl -L https://github.com/docker/compose/releases/download/$new_ver/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose
    chmod +x /usr/local/bin/docker-compose

##### 验证安装

    docker-compose --version

### 安装Docker PHP环境

#### 创建文件结构

    mkdir -p /lnmp/{php,mysql,wwwroot,log,conf}
    mkdir /lnmp/log/{nginx,php,mysql}

#### 获取nginx默认配置文件

    docker run --name nginx -p 80:80 -d nginx
    docker cp nginx:/usr/share/nginx/html/ /lnmp/wwwroot/default/
    docker cp nginx:/etc/nginx/nginx.conf /lnmp/conf/nginx.conf
    docker cp nginx:/etc/nginx/conf.d/ /lnmp/conf/conf.d/

#### 修改nginx 路径

    sed -i 's#/usr/share/nginx/html#/var/www/html/default#' /lnmp/conf/conf.d/default.conf
    sed -i 's/#access_log/access_log/' /lnmp/conf/conf.d/default.conf

#### 删除容器

    docker stop nginx
    docker rm nginx

#### [Build PHP Docker](https://hub.docker.com/_/php/)

    mkdir /lnmp/php/php72
    
    cat <<'EOF' >/lnmp/php/php72/Dockerfile
    FROM php:7.2-fpm

    COPY ./sources.list.stretch /etc/apt/sources.list
    
    RUN apt-get update \
      && apt-get install -y libfreetype6-dev libjpeg62-turbo-dev libpng-dev \
      && docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ \
      && docker-php-ext-install gd \
      && :\
      && apt-get install -y libicu-dev \
      && docker-php-ext-install intl \
      && :\
      && apt-get install -y libxml2-dev \
      && apt-get install -y libxslt-dev \
      && docker-php-ext-install soap \
      && docker-php-ext-install xsl \
      && docker-php-ext-install xmlrpc \
      && docker-php-ext-install wddx \
      && :\
      && apt-get install -y libbz2-dev \
      && docker-php-ext-install bz2 \
      && :\
      && docker-php-ext-install zip \
      && docker-php-ext-install pcntl \
      && docker-php-ext-install pdo_mysql \
      && docker-php-ext-install mysqli \
      && docker-php-ext-install mbstring \
      && docker-php-ext-install exif \
      && docker-php-ext-install bcmath \
      && docker-php-ext-install calendar \
      && docker-php-ext-install sockets \
      && docker-php-ext-install gettext \
      && docker-php-ext-install shmop \
      && docker-php-ext-install sysvmsg \
      && docker-php-ext-install sysvsem \
      && docker-php-ext-install sysvshm \
      && docker-php-ext-install opcache \
      && :\
      && pecl install igbinary \
      && docker-php-ext-enable igbinary \
      && :\
      && pecl install redis \
      && docker-php-ext-enable redis
      #&& docker-php-ext-install pdo_firebird \
      #&& docker-php-ext-install pdo_dblib \
      #&& docker-php-ext-install pdo_oci \
      #&& docker-php-ext-install pdo_odbc \
      #&& docker-php-ext-install pdo_pgsql \
      #&& docker-php-ext-install pgsql \
      #&& docker-php-ext-install oci8 \
      #&& docker-php-ext-install odbc \
      #&& docker-php-ext-install dba \
      #&& docker-php-ext-install interbase \
      #&& :\
      #&& apt-get install -y libmcrypt-dev \
      #&& docker-php-ext-install mcrypt \
      #&& :\
      #&& apt-get install -y curl \
      #&& apt-get install -y libcurl3 \
      #&& apt-get install -y libcurl4-openssl-dev \
      #&& docker-php-ext-install curl \
      #&& :\
      #&& apt-get install -y libreadline-dev \
      #&& docker-php-ext-install readline \
      #&& :\
      #&& apt-get install -y libsnmp-dev \
      #&& apt-get install -y snmp \
      #&& docker-php-ext-install snmp \
      #&& :\
      #&& apt-get install -y libpspell-dev \
      #&& apt-get install -y aspell-en \
      #&& docker-php-ext-install pspell \
      #&& :\
      #&& apt-get install -y librecode0 \
      #&& apt-get install -y librecode-dev \
      #&& docker-php-ext-install recode \
      #&& :\
      #&& apt-get install -y libtidy-dev \
      #&& docker-php-ext-install tidy \
      #&& :\
      #&& apt-get install -y libgmp-dev \
      #&& ln -s /usr/include/x86_64-linux-gnu/gmp.h /usr/include/gmp.h \
      #&& docker-php-ext-install gmp \
      #&& :\
      #&& apt-get install -y postgresql-client \
      #&& apt-get install -y mysql-client \
      #&& :\
      #&& apt-get install -y libc-client-dev \
      #&& docker-php-ext-configure imap --with-kerberos --with-imap-ssl \
      #&& docker-php-ext-install imap \
      #&& :\
      #&& apt-get install -y libldb-dev \
      #&& apt-get install -y libldap2-dev \
      #&& docker-php-ext-configure ldap --with-libdir=lib/x86_64-linux-gnu \
      #&& docker-php-ext-install ldap \

      # Composer
      #RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/bin/ --filename=composer \
    EOF
    
    # Docker 软件源
    cat <<'EOF' >/lnmp/php/php72/sources.list.stretch
    deb http://mirrors.163.com/debian/ stretch main non-free contrib
    deb http://mirrors.163.com/debian/ stretch-updates main non-free contrib
    deb http://mirrors.163.com/debian/ stretch-backports main non-free contrib
    deb http://mirrors.163.com/debian-security/ stretch/updates main non-free contrib

    deb-src http://mirrors.163.com/debian/ stretch main non-free contrib
    deb-src http://mirrors.163.com/debian/ stretch-updates main non-free contrib
    deb-src http://mirrors.163.com/debian/ stretch-backports main non-free contrib
    deb-src http://mirrors.163.com/debian-security/ stretch/updates main non-free contrib
    EOF

#### Docker php 配置文件

    cd /lnmp/php/php72
    docker build -t php-fpm .
    docker run --name php-fpm -d php-fpm

    mkdir /lnmp/conf/php-fpm.d/
    docker cp php-fpm:/usr/local/etc/php-fpm.d/www.conf /lnmp/conf/php-fpm.d/www.conf
    
    sed -i 's#;catch_workers_output#catch_workers_output#' /lnmp/conf/php-fpm.d/www.conf
    sed -i "s#;php_admin_value\[error_log\].*#php_admin_value\[error_log\] = /var/log/php/fpm-php.www.log#g" /lnmp/conf/php-fpm.d/www.conf
    sed -i "s#;php_admin_flag\[log_errors\]#php_admin_flag\[log_errors\]#g" /lnmp/conf/php-fpm.d/www.conf

#### 删除容器

    docker stop php-fpm
    docker rm php-fpm

#### 获取Mariadb配置文件

    docker run --name mariadb -e MYSQL_ROOT_PASSWORD=root -d mariadb
    docker cp mariadb:/etc/mysql/my.cnf /lnmp/conf/my.cnf
    docker stop mariadb
    docker rm mariadb
    
#### 删除所有容器及镜像

    docker stop `docker ps -q`
    docker rm `docker ps -qa`
    docker rmi `docker images -q`


### [docker-compose.yml](https://coding.net/u/yx571304/p/Docker/git/tree/master/docker-lnmp)

    cat <<'EOF' >/lnmp/docker-compose.yml
    version: '3.6'
    services:

      nginx:
        image: nginx
        container_name: nginx
        ports:
          - "80:80"
          - "443:443"
        volumes:
          - /etc/localtime:/etc/localtime:ro
          - ./wwwroot/:/var/www/html/:rw
          - ./conf/conf.d:/etc/nginx/conf.d/:ro
          - ./conf/nginx.conf:/etc/nginx/nginx.conf:ro
          - ./log/nginx/:/var/log/nginx/:rw
        networks:
          - net-php
        restart: always

      php:
        build: ./php/php72/
        container_name: php-fpm
        expose:
          - "9000"
        volumes:
          - /etc/localtime:/etc/localtime:ro
          - ./wwwroot/:/var/www/html/:rw
          - ./conf/php.ini:/usr/local/etc/php/php.ini:ro
          - ./conf/php-fpm.d/www.conf:/usr/local/etc/php-fpm.d/www.conf:ro
          - ./log/php/:/var/log/php/:rw
        networks:
          - net-php
          - net-mariadb
          - net-redis
        restart: always

      mariadb:
        image: mariadb
        container_name: mariadb
        ports:
          - 3306:3306
        volumes:
          - /etc/localtime:/etc/localtime:ro
          - ./conf/my.cnf:/etc/mysql/my.cnf:ro
          - ./log/mysql/:/var/log/mysql/:rw
          - ./mysql/:/var/lib/mysql/:rw
        networks:
          - net-mariadb
        restart: always
        environment:
          MYSQL_ROOT_PASSWORD: root

      adminer:
        image: adminer
        container_name: adminer
        restart: always
        ports:
          - 8080:8080
        volumes:
          - /etc/localtime:/etc/localtime:ro
        networks:
          - net-mariadb
        restart: always

      redis:
        image: redis:latest
        container_name: redis
        ports:
          - 6379:6379
        volumes:
          - /etc/localtime:/etc/localtime:ro
        networks:
          - net-redis
        restart: always

    networks:
      net-php:
      net-mariadb:
      net-redis:

### Build

    docker-compose up --build
    docker-compose start
    docker-compose stop
