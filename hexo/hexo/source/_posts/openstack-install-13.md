---
title: 创建内网OpenStack源(pike版)
tags:
  - repo
  - yum
  - openstack
copyright: true
categories:
  - openstack
abbrlink: openstack-install-13
date: 2018-07-31 18:35:26
---

## 环境准备

    # CentOS 7 最小化安装

    # 关闭, 禁用 firewalld
    systemctl stop firewalld
    systemctl disable firewalld
    firewall-cmd --state

    # 关闭, 禁用 selinux
    setenforce 0
    sed -i '/^SELINUX=.*/c SELINUX=disabled' /etc/selinux/config
    grep --color=auto '^SELINUX' /etc/selinux/config

## YUM设置

> 更改YUM缓存路径, 安装完成后不删除已下载软件包

    mkdir -p /www/share/tmp
    cp /etc/yum.conf{,.bak}
    sed -i 's#^keepcache=0#keepcache=1#' /etc/yum.conf
    sed -i 's/^cachedir/#cachedir/' /etc/yum.conf
    sed -ir '3 icachedir=/www/share/tmp/$basearch/$releasever \n' /etc/yum.conf

## 配置 yum, epel, openstack 安装源

    rm -f /etc/yum.repos.d/*
    curl -so /etc/yum.repos.d/Centos-7.repo http://mirrors.aliyun.com/repo/Centos-7.repo
    sed -i '/aliyuncs.com/d' /etc/yum.repos.d/Centos-7.repo
    yum -y install epel-release
    rm -rf /etc/yum.repos.d/epel*
    curl -so /etc/yum.repos.d/epel-7.repo http://mirrors.aliyun.com/repo/epel-7.repo
    sed -i '/aliyuncs.com/d' /etc/yum.repos.d/epel-7.repo

    # 手动配置阿里云openstack源(关闭GPG验证 否则软件安装时会报错)
    cat <<'EOF' >/etc/yum.repos.d/centos-openstack.repo
    [centos-openstack-pike]
    name=CentOS-$releasever-openstack-pike
    enabled=1
    failovermethod=priority
    baseurl=http://mirrors.aliyun.com/centos/$releasever/cloud/$basearch/openstack-pike
    gpgcheck=0
    #gpgkey=http://mirrors.aliyun.com/centos/RPM-GPG-KEY-CentOS-7

    [centos-virt-kvm-common]
    name=CentOS-$releasever-virt-kvm-common
    enabled=1
    failovermethod=priority
    baseurl=https://mirrors.aliyun.com/centos/$releasever/virt/$basearch/kvm-common/
    gpgcheck=0
    #gpgkey=http://mirrors.aliyun.com/centos/RPM-GPG-KEY-CentOS-7
    EOF

## 更新系统并重启

> 系统更新完毕后需要重新执行上一步替换软装安装源

    yum -y update
    reboot

## 保下载的包

> 将 /www/share/yum/ 目录下载的包移动到 /www/share/yum

    mkdir /www/share/yum
    find /www/share/yum/ -name *.rpm |sed -r 's#.*#mv & /mnt/tmp/\n#'|bash

## 下载安装openstack 用到的包

    List='wget vim ntpdate net-tools lrzsz lsof glances htop corosync pacemaker pcs fence-agents 
      resource-agents httpd haproxy python-openstackclient openstack-selinux openstack-utils expect 
      mariadb mariadb-server mariadb-galera-server rabbitmq-server erlang socat openstack-keystone 
      httpd mod_wsgi memcached python-memcached nfs-utils rpcbind openstack-glance python-glance 
      openstack-nova-api openstack-nova-conductor openstack-nova-console openstack-nova-novncproxy 
      openstack-nova-scheduler openstack-nova-placement-api openstack-neutron openstack-neutron-ml2 
      openstack-neutron-linuxbridge python-neutronclient ebtables ipset openstack-dashboard 
      openstack-cinder targetcli python-keystone lvm2 nfs-utils rpcbind python2-PyMySQL 
      openstack-nova-compute openstack-neutron-linuxbridge ebtables ipset'

    for Package in $List; do
        yum install -y --downloadonly --downloaddir=/www/share/yum $Package
    done

## 生成数据库文件

    yum install -y createrepo
    createrepo -pdo  /www/share/yum /www/share/yum

## 安装 http

    yum install -y httpd
    cat <<EOF >/etc/httpd/conf.d/share.conf
    # http share
    Alias /share /www/share
    <Directory "/www/share">
        Options Indexes FollowSymLinks
        IndexOptions NameWidth=* DescriptionWidth=* FoldersFirst
        IndexOptions SuppressIcon HTMLTable Charset=UTF-8 SuppressHTMLPreamble
        Order allow,deny
        Allow from all
        Require all granted
    </Directory>
    EOF

    systemctl restart httpd
    systemctl enable httpd

## 创建repo

    cat <<EOF >/www/share/loacl.repo
    [local_yum]
    name=My_Souce
    baseurl=http://192.168.0.2/share/yum/
    gpgcheck=0
    enabled=1
    cost=88
    gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-7
    EOF

## 添加软件

    # 下载没有安装过的包
    yum install --downloadonly --downloaddir=/www/share/yum/ -y 包名称

    # 下载已经安装过的包
    yum reinstall --downloadonly --downloaddir=/www/share/yum/ -y 包名称

    # 生成数据库
    createrepo -pdo  /www/share/yum /www/share/yum
    createrepo --update -pdo /www/share/yum /www/share/yum

## 客户端使用

    curl http://192.168.0.2/share/loacl.repo -o /etc/yum.repos.d/Lan7.repo



[参考](http://www.cnblogs.com/elvi/p/7657770.html)    
