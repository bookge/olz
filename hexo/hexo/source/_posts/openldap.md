---
title: OpenLdap安装配置
tags:
  - openldap
  - ldap
copyright: true
categories:
  - ldap
abbrlink: openldap
date: 2018-09-25 03:25:09
---

[TOC]

## 环境准备

    系统：centos7
    软件版本：openldap-2.4.4
    配置文件: /etc/openldap/sldap.conf

    说明：ldap双主配置
    lab-01    ldap.test.com     IP: 10.0.0.11
    lab-02    ldap2.test.com    IP: 10.0.0.12
---

## 环境初始化

> 配置防火墙 放行`ldap, sldap` 端口

    firewall-cmd --zone=public --add-port=389/tcp --permanent
    firewall-cmd --zone=public --add-port=636/tcp --permanent
    firewall-cmd --reload
---

> 关闭, 禁用`selinux`

    setenforce 0
    sed -i '/^SELINUX=.*/SELINUX=disabled' /etc/selinux/config
---

> 配置软件安装源

    rm -f /etc/yum.repos.d/*
    curl -so /etc/yum.repos.d/Centos-7.repo http://mirrors.aliyun.com/repo/Centos-7.repo
    curl -so /etc/yum.repos.d/epel-7.repo http://mirrors.aliyun.com/repo/epel-7.repo
    sed -i '/aliyuncs.com/d' /etc/yum.repos.d/Centos-7.repo /etc/yum.repos.d/epel-7.repo 
---

> 配置时间同步

    yum -y install ntpdate
    /usr/sbin/ntpdate ntp6.aliyun.com ;hwclock -w
    echo -e "*/3 * * * * /usr/sbin/ntpdate ntp6.aliyun.com  >/dev/null 2>&1 && /usr/sbin/hwclock -w" > /tmp/crontab
    crontab /tmp/crontab
---

## lab-01 安装配置

> hostname(若不配置主机名或者配置错误会导致后面服务无法启动)

    # lab-01
    hostnamectl set-hostname ldap.test.com
---

> 配置`hosts`解析

    cat <<EOF   >>/etc/hosts

    # add ldap server
    10.0.0.11    ldap.test.com
    10.0.0.12    ldap2.test.com
    EOF
---

### 安装`OpenLdap`

> 安装`OpenLdap`与相关软件包

    yum install -y openldap openldap-devel openldap-servers openldap-clients openldap-devel compat-openldap
---

### 创建`CA`证书

> `CA`中心生成自身私钥

    cd /etc/pki/CA
    (umask 077;openssl genrsa -out private/cakey.pem 2048)
---

> `CA`签发自身公钥

    openssl req -new -x509 -key private/cakey.pem -out cacert.pem -days 3650 \
      -subj "/C=CN/ST=HuBei/L=WuHan/O=test/OU=test.com/CN=*.test.com/emailAddress=544025211@qq.com"
    touch serial index.txt
    echo "01" > serial
---

> 查看生成的根证书信息

    openssl x509 -noout -text -in /etc/pki/CA/cacert.pem
---

### 创建`ldap`证书

> `OpenLdap`服务端生成证书

    mkdir /etc/openldap/ssl
    cd /etc/openldap/ssl
    (umask 077;openssl genrsa -out ldapkey.pem 1024)
---

> `OpenLdap`服务端向`CA`申请证书签署请求

    openssl req -new -key ldapkey.pem -out ldap.csr -days 3650 \
      -subj "/C=CN/ST=HuBei/L=WuHan/O=test/OU=test.com/CN=*.test.com/emailAddress=544025211@qq.com"
---

> `CA`核实并签发证书

    openssl ca -in /etc/openldap/ssl/ldap.csr -out /etc/openldap/ssl/ldapcert.pem -days 3650
---

> 设置证书权限

    cp /etc/pki/CA/cacert.pem /etc/openldap/ssl/
    chown -R ldap.ldap /etc/openldap/ssl/*
    chmod -R 0400 /etc/openldap/ssl/*
---

### 配置`OpenLdap`

> 复制数据库配置文件

    cp /usr/share/openldap-servers/DB_CONFIG.example /var/lib/ldap/DB_CONFIG
---

> 设置配置文件,数据目录权限

    chown -R ldap.ldap /etc/openldap/
    chown -R ldap.ldap /var/lib/ldap/
---

> 启动服务,配置服务跟随系统启动

    systemctl start slapd
    systemctl enable slapd
---

> 生成管理员密码

    pass=linux
    slappasswd -s $pass -n > /etc/openldap/passwd
    cat /etc/openldap/passwd
    ldap_pass="$(cat /etc/openldap/passwd)"
---

> 创建slapd 配置文件

```
cat<<EOF  >/etc/openldap/slapd.conf
include     /etc/openldap/schema/corba.schema
include     /etc/openldap/schema/core.schema
include     /etc/openldap/schema/cosine.schema
include     /etc/openldap/schema/duaconf.schema
include     /etc/openldap/schema/dyngroup.schema
include     /etc/openldap/schema/inetorgperson.schema
include     /etc/openldap/schema/java.schema
include     /etc/openldap/schema/misc.schema
include     /etc/openldap/schema/nis.schema
include     /etc/openldap/schema/openldap.schema
include     /etc/openldap/schema/ppolicy.schema
include     /etc/openldap/schema/collective.schema

allow       bind_v2
pidfile     /var/run/openldap/slapd.pid
argsfile    /var/run/openldap/slapd.args

modulepath  /usr/lib64/openldap
moduleload  ppolicy.la

TLSCACertificateFile  /etc/openldap/ssl/cacert.pem
TLSCertificateFile  /etc/openldap/ssl/ldapcert.pem
TLSCertificateKeyFile  /etc/openldap/ssl/ldapkey.pem
TLSVerifyClient never

access to attrs=shadowLastChange,userPassword
      by self write
      by * auth

access to *
      by * read

database config
access to *
    by dn.exact="gidNumber=0+uidNumber=0,cn=peercred,cn=external,cn=auth" manage
    by * none

database monitor
access to *
    by dn.exact="gidNumber=0+uidNumber=0,cn=peercred,cn=external,cn=auth" read
        by dn.exact="cn=admin,dc=test,dc=com" read
        by * none

database    hdb
suffix      "dc=test,dc=com"
checkpoint  1024 15
rootdn      "cn=admin,dc=test,dc=com"
rootpw      $ldap_pass
directory   /var/lib/ldap

index    objectClass                       eq,pres
index    ou,cn,mail,surname,givenname      eq,pres,sub
index    uidNumber,gidNumber,loginShell    eq,pres
index    uid,memberUid                     eq,pres,sub
index    nisMapName,nisMapEntry            eq,pres,sub
loglevel    4095

# 主同步1 (若为配置单机版本则无需以下配置)
moduleload syncprov.la
modulepath /usr/lib64/openldap

index   entryCSN,entryUUID    eq

overlay syncprov
syncprov-checkpoint 100 10
syncprov-sessionlog 100

serverID  1  ldaps://ldap.test.com
serverID  2  ldaps://ldap2.test.com

syncrepl  rid=123
    provider=ldaps://ldap.test.com
    bindmethod=simple
    binddn="cn=admin,dc=test,dc=com"
    credentials=linux
    searchbase="dc=test,dc=com"
    schemachecking=on
    type=refreshAndPersist
    starttls=yes
    tls_key="/etc/openldap/ssl/ldapkey.pem"
    tls_cert="/etc/openldap/ssl/ldapcert.pem"
    tls_cacert="/etc/openldap/ssl/cacert.pem"
    retry="5 5 300 5"

syncrepl  rid=123
    provider=ldaps://ldap2.test.com
    bindmethod=simple
    binddn="cn=admin,dc=test,dc=com"
    credentials=linux
    searchbase="dc=test,dc=com"
    schemachecking=on
    type=refreshAndPersist
    starttls=yes
    tls_key="/etc/openldap/ssl/ldapkey.pem"
    tls_cert="/etc/openldap/ssl/ldapcert.pem"
    tls_cacert="/etc/openldap/ssl/cacert.pem"
    retry="5 5 300 5"

mirrormode TRUE
EOF
```
---

> 配置监听服务

    sed -i 's/SLAPD_URLS/#SLAPD_URLS/' /etc/sysconfig/slapd
    cat <<EOF   >>/etc/sysconfig/slapd

    # OpenLDAP Ldaps
    SLAPD_URLS="ldapi:/// ldap:/// ldaps:///"
    SLAPD_LDAP=yes
    SLAPD_LDAPI=yes
    SLAPD_LDAPS=yes
    EOF
---

> 删除默认配置,生成新的配置(依据slapd.conf配置项生成新的配置)

    rm -rf /etc/openldap/slapd.d/*
    slaptest -f /etc/openldap/slapd.conf -F /etc/openldap/slapd.d/
---

> 设置目录权限, 重启服务

    chown -R ldap.ldap /etc/openldap/slapd.d
    chown -R ldap.ldap /var/lib/ldap/
    systemctl restart slapd
---

> 配置日志

    cat <<EOF   >>/etc/rsyslog.conf

    # ldap log
    local4.*    /var/log/slapd/slapd.log
    EOF

    systemctl restart rsyslog
---

### 添加`用户`, `组`

> 创建本地测试用户

    for user in ldapuser{1..5} feng{1..5}; do
        useradd $user
        echo redhat | passwd --stdin $user
    done
---

> 通过`migration`工具生成`ldif`文件

    # 安装工具
    yum install -y migrationtools

    # 替换域信息
    sed -i "71,74 s/padl/test/g" /usr/share/migrationtools/migrate_common.ph
    sed -i "71,74 s/com/com/g" /usr/share/migrationtools/migrate_common.ph
    sed -i 's/EXTENDED_SCHEMA = 0/EXTENDED_SCHEMA = 1/' /usr/share/migrationtools/migrate_common.ph

    # 生成基础架构,根据密码文件生成用户信息,根据组文件生成组信息
    /usr/share/migrationtools/migrate_base.pl > ~/base.ldif
    /usr/share/migrationtools/migrate_passwd.pl  /etc/passwd > ~/passwd.ldif
    /usr/share/migrationtools/migrate_group.pl  /etc/group > ~/group.ldif
---

> 导入数据导入至`OpenLdap`目录树

    ldapadd -x -w $pass -D "cn=admin,dc=test,dc=com" -f ~/base.ldif
    ldapadd -x -w $pass -D "cn=admin,dc=test,dc=com" -f ~/passwd.ldif
    ldapadd -x -w $pass -D "cn=admin,dc=test,dc=com" -f ~/group.ldif
---

### 测试验证

> 搜索导入的用户

    ldapsearch -x cn=ldapuser1 -b dc=test,dc=com
    for user in ldapuser{1..5} feng{1..5}; do
        echo "---------- id $user ----------"
        ldapsearch -x cn=$user -b dc=test,dc=com
        sleep 1
        echo
    done
---

> 验证`OpenLdap`服务端证书的合法性

    openssl verify -CAfile /etc/openldap/ssl/cacert.pem /etc/openldap/ssl/ldapcert.pem
---

> 确认当前套接字是否能通过CA的验证

    openssl s_client -connect ldap.test.com:636 -showcerts -state -CAfile /etc/openldap/ssl/cacert.pem
---

## lab-02 安装配置

> hostname(若不配置主机名或者配置错误会导致后面服务无法启动)

    # lab-02
    hostnamectl set-hostname ldap2.test.com
---

> 配置`hosts`解析

    cat <<EOF   >>/etc/hosts

    # add ldap server
    10.0.0.11    ldap.test.com
    10.0.0.12    ldap2.test.com
    EOF
---

### 安装`OpenLdap`

> 安装`OpenLdap`与相关软件包

    yum install -y openldap openldap-devel openldap-servers openldap-clients openldap-devel compat-openldap
---

### 配置`OpenLdap`

> 下载lab-01 证书文件

    mkdir /etc/openldap/ssl/
    scp ldap.test.com:/etc/openldap/ssl/* /etc/openldap/ssl/
---

> 设置凭证权限

    chown -R ldap.ldap /etc/openldap/ssl/*
    chmod -R 0400 /etc/openldap/ssl/*
---

> 复制数据库配置文件

    cp /usr/share/openldap-servers/DB_CONFIG.example /var/lib/ldap/DB_CONFIG
---

> 设置配置文件,数据目录权限

    chown -R ldap.ldap /etc/openldap/
    chown -R ldap.ldap /var/lib/ldap/
---

> 启动服务,跟随系统启动

    systemctl start slapd
    systemctl enable slapd
---

> 生成密码管理账号密码

    pass=linux
    slappasswd -s $pass -n > /etc/openldap/passwd
    cat /etc/openldap/passwd
    ldap_pass="$(cat /etc/openldap/passwd)"
---

> 创建slapd 配置文件

```
cat<<EOF  >/etc/openldap/slapd.conf
include     /etc/openldap/schema/corba.schema
include     /etc/openldap/schema/core.schema
include     /etc/openldap/schema/cosine.schema
include     /etc/openldap/schema/duaconf.schema
include     /etc/openldap/schema/dyngroup.schema
include     /etc/openldap/schema/inetorgperson.schema
include     /etc/openldap/schema/java.schema
include     /etc/openldap/schema/misc.schema
include     /etc/openldap/schema/nis.schema
include     /etc/openldap/schema/openldap.schema
include     /etc/openldap/schema/ppolicy.schema
include     /etc/openldap/schema/collective.schema

allow       bind_v2
pidfile     /var/run/openldap/slapd.pid
argsfile    /var/run/openldap/slapd.args

modulepath  /usr/lib64/openldap
moduleload  ppolicy.la

TLSCACertificateFile  /etc/openldap/ssl/cacert.pem
TLSCertificateFile  /etc/openldap/ssl/ldapcert.pem
TLSCertificateKeyFile  /etc/openldap/ssl/ldapkey.pem
TLSVerifyClient never

access to attrs=shadowLastChange,userPassword
      by self write
      by * auth

access to *
      by * read

database config
access to *
    by dn.exact="gidNumber=0+uidNumber=0,cn=peercred,cn=external,cn=auth" manage
    by * none

database monitor
access to *
    by dn.exact="gidNumber=0+uidNumber=0,cn=peercred,cn=external,cn=auth" read
        by dn.exact="cn=admin,dc=test,dc=com" read
        by * none

database    hdb
suffix      "dc=test,dc=com"
checkpoint  1024 15
rootdn      "cn=admin,dc=test,dc=com"
rootpw      $ldap_pass
directory   /var/lib/ldap

index    objectClass                       eq,pres
index    ou,cn,mail,surname,givenname      eq,pres,sub
index    uidNumber,gidNumber,loginShell    eq,pres
index    uid,memberUid                     eq,pres,sub
index    nisMapName,nisMapEntry            eq,pres,sub
loglevel    4095

# 主同步2 (若为配置单机版本则无需以下配置)
moduleload syncprov.la
modulepath /usr/lib64/openldap

index entryCSN,entryUUID                eq

overlay syncprov
syncprov-checkpoint 100 10
syncprov-sessionlog 100

serverID  1  ldaps://ldap.test.com
serverID  2  ldaps://ldap2.test.com

syncrepl  rid=123
    provider=ldaps://ldap2.test.com
    bindmethod=simple
    binddn="cn=admin,dc=test,dc=com"
    credentials=$pass
    searchbase="dc=test,dc=com"
    schemachecking=on
    type=refreshAndPersist
    starttls=yes
    tls_key="/etc/openldap/ssl/ldapkey.pem"
    tls_cert="/etc/openldap/ssl/ldapcert.pem"
    tls_cacert="/etc/openldap/ssl/cacert.pem"
    retry="5 5 300 5"

syncrepl  rid=123
    provider=ldaps://ldap.test.com
    bindmethod=simple
    binddn="cn=admin,dc=test,dc=com"
    credentials=$pass
    searchbase="dc=test,dc=com"
    schemachecking=on
    type=refreshAndPersist
    starttls=yes
    tls_key="/etc/openldap/ssl/ldapkey.pem"
    tls_cert="/etc/openldap/ssl/ldapcert.pem"
    tls_cacert="/etc/openldap/ssl/cacert.pem"
    retry="5 5 300 5"

mirrormode TRUE
EOF
```
---

> 配置监听服务

    sed -i 's/SLAPD_URLS/#SLAPD_URLS/' /etc/sysconfig/slapd
    cat <<EOF   >>/etc/sysconfig/slapd

    # OpenLDAP Ldaps
    SLAPD_URLS="ldapi:/// ldap:/// ldaps:///"
    SLAPD_LDAP=yes
    SLAPD_LDAPI=yes
    SLAPD_LDAPS=yes
    EOF
---

> 删除默认配置,生成新的配置(依据slapd.conf配置项生成新的配置)

    rm -rf /etc/openldap/slapd.d/*
    slaptest -f /etc/openldap/slapd.conf -F /etc/openldap/slapd.d/
---

> 设置目录权限, 重启服务

    chown -R ldap.ldap /etc/openldap/slapd.d
    chown -R ldap.ldap /var/lib/ldap/
    systemctl restart slapd
---

> 配置日志

    cat <<EOF   >>/etc/rsyslog.conf

    # ldap log
    local4.*    /var/log/slapd/slapd.log
    EOF

    systemctl restart rsyslog
---

### 测试验证

> 搜索导入的用户

    ldapsearch -x cn=ldapuser1 -b dc=test,dc=com
    for user in ldapuser{1..5} feng{1..5}; do
        echo "---------- id $user ----------"
        ldapsearch -x cn=$user -b dc=test,dc=com
        sleep 1
        echo
    done
---

> 验证`OpenLdap`服务端证书的合法性

    openssl verify -CAfile /etc/openldap/ssl/cacert.pem /etc/openldap/ssl/ldapcert.pem
---

> 确认当前套接字是否能通过CA的验证

    openssl s_client -connect ldap2.test.com:636 -showcerts -state -CAfile /etc/openldap/ssl/cacert.pem
---

> 验证同步

    tail -f /var/log/slapd/slapd.log
    rm -f /var/lib/ldap/*; systemctl restart slapd;sleep 2; ls /var/lib/ldap/
---

## 客户端配置

> 配置`hosts`解析

    cat <<EOF   >>/etc/hosts

    # add ldap server
    10.0.0.11    ldap.test.com
    10.0.0.12    ldap2.test.com
    EOF
---

> 安装验证模块

    yum install -y sssd krb5-workstation
---

> 配置单服务器验证

    authconfig \
      --enableldap \
      --enableldaptls \
      --enableldapauth \
      --enablemkhomedir \
      --ldapserver='ldaps://ldap.test.com' \
      --ldapbasedn="dc=test,dc=com" \
      --ldaploadcacert=http://ldap.test.com/cacert.pem \
      --update
---

> 配置双服务器验证

    authconfig \
      --enableldap \
      --enableldaptls \
      --enableldapauth \
      --enablemkhomedir \
      --ldapserver='ldaps://ldap.test.com,ldaps://ldap2.test.com' \
      --ldapbasedn="dc=test,dc=com" \
      --ldaploadcacert=http://ldap.test.com/cacert.pem \
      --update
---

> 验证

    id ldapuser1
    ssh ldapuser1@127.0.0.1
---
