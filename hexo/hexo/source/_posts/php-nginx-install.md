title: php-nginx编译安装
tags:
  - lnmp
  - php
  - nginx
copyright: true
categories:
  - web
abbrlink: 7ae3
date: 2018-05-09 15:56:35
---
### 系统环境

    系统：CentOS7.4-1708-Mini
    源码路径：/usr/local/src/
    安装路径：/usr/local/
    依赖软件：zlib, openssl, c-ares, curl, libiconv, libmcrypt, mhash, mcrypt, icu, libxml2, libxslt, libjpeg, libpng, libwebp, freetype2, libXpm, libgd
    修复：pycurl (安装curl导致yum不能使用,pycurl库文件问题)
    

### 准备工作

>[官方文档](http://php.net/manual/zh/)


#### 基础环境

    yum -y install gcc gcc-c++ vim wget ntpdate lrzsz python-devel patch \
    unzip autoconf bzip2-devel
    mkdir /soft

#### 修改时区

    rm -f /etc/localtime
    ln -sf /usr/share/zoneinfo/Asia/Shanghai /etc/localtime

#### 关闭selinux

    setenforce 0
    sed -i 's/SELINUX=.*/SELINUX=disabled/' /etc/selinux/config
    

#### 时间同步

    ntpdate ntp.aliyun.com && hwclock -w
    echo "*/20 * * * * $(which ntpdate) pool.ntp.org > /dev/null 2>&1 && \
    $(which hwclock) -w" >> /var/spool/cron/root
    chmod 600 /var/spool/cron/root

### 安装依赖环境

#### zlib

```shc
# 下载 解压
cd /soft
wget -c http://www.zlib.net/zlib-1.2.11.tar.gz
tar xvf zlib-1.2.11.tar.gz -C /usr/local/src/
cd /usr/local/src/zlib-1.2.11/

# 编译 安装
CFLAGS="-O3 -fPIC" ./configure --prefix=/usr/local/zlib  --shared
make && make install

# 配置环境变量
export C_INCLUDE_PATH=/usr/local/zlib/include
echo '/usr/local/zlib/lib' >> /etc/ld.so.conf
ldconfig -v | grep libz
```

#### openssl

```sh
# 下载 解压
cd /soft
wget -c https://www.openssl.org/source/openssl-1.1.0h.tar.gz
tar xvf openssl-1.1.0h.tar.gz -C /usr/local/src/
cd /usr/local/src/openssl-1.1.0h/

# 编译 安装
./config -fPIC --prefix=/usr/local/openssl zlib-dynamic shared \
--openssldir=/usr/local/openssl
make && make install
wget -O /usr/local/openssl/cacert.pem http://curl.haxx.se/ca/cacert.pem

# 配置环境变量
echo '/usr/local/openssl/lib' >> /etc/ld.so.conf
ldconfig -v | grep libssl.so
ldconfig -v | grep libcrypto.so
```

#### C-ares

```sh
# 下载 解压
cd /soft
wget https://c-ares.haxx.se/download/c-ares-1.14.0.tar.gz
tar xvf c-ares-1.14.0.tar.gz -C /usr/local/src/
cd /usr/local/src/c-ares-1.14.0/

# 编译 安装
./configure --prefix=/usr/local/c-ares --disable-static
make && make install

# 配置环境变量
echo '/usr/local/c-ares/lib' >> /etc/ld.so.conf
ldconfig -v | grep libcares.so
```

#### curl 

```sh
# 下载 解压
cd /soft
wget -c https://curl.haxx.se/download/curl-7.59.0.tar.gz
tar xvf curl-7.59.0.tar.gz -C /usr/local/src/
cd /usr/local/src/curl-7.59.0

# 编译 安装
./configure --prefix=/usr/local/curl --disable-static \
    --enable-threaded-resolver --enable-ares=/usr/local/c-ares \
    --without-nss --with-ssl=/usr/local/openssl --with-zlib=/usr/local/zlib
make && make install

# 配置环境变量
echo '/usr/local/curl/lib' >> /etc/ld.so.conf
ldconfig -v | grep libcurl
```

#### pycurl

> 修复安装curl导致yum异常

```sh
# 下载 解压
cd /soft
wget -c https://dl.bintray.com/pycurl/pycurl/pycurl-7.43.0.1.tar.gz
tar xvf pycurl-7.43.0.1.tar.gz -C /usr/local/src/
cd /usr/local/src/pycurl-7.43.0.1/

# 编译 安装
python setup.py install --curl-config=/usr/local/curl/bin/curl-config \
--with-openssl --openssl-dir=/usr/local/openssl
ln -sf /usr/local/curl/bin/curl-config /usr/local/bin/curl-config
make && make install
```

#### libiconv

```sh
# 下载解压
cd /soft
wget -c ftp.gnu.org/pub/gnu/libiconv/libiconv-1.15.tar.gz
tar xvf libiconv-1.15.tar.gz -C /usr/local/src/
cd /usr/local/src/libiconv-1.15/

# 编译 安装
./configure --prefix=/usr/local/libiconv
make && make install

# 配置环境变量
echo '/usr/local/libiconv/lib' >> /etc/ld.so.conf
ldconfig -v | grep libiconv
ldconfig -v | grep libcharset
```

#### libmcrypt

```sh
# 下载 解压
cd /soft
wget -c https://jaist.dl.sourceforge.net/project/mcrypt/Libmcrypt/2.5.8/libmcrypt-2.5.8.tar.gz
tar xvf libmcrypt-2.5.8.tar.gz -C /usr/local/src/
cd /usr/local/src/libmcrypt-2.5.8

# 编译 安装libmcrypt
./configure --prefix=/usr/local/libmcrypt
make && make install

# 编译 安装libltdl
cd libltdl
./configure --prefix=/usr/local/libltdl --enable-ltdl-install
make && make install

# 配置环境变量
echo '/usr/local/libmcrypt/lib' >> /etc/ld.so.conf
echo '/usr/local/libltdl/lib' >> /etc/ld.so.conf
ldconfig -v | grep libmcrypt
ldconfig -v | grep libltdl
```

#### mhash 

```sh
# 下载 解压
cd /soft
wget -c https://jaist.dl.sourceforge.net/project/mhash/mhash/0.9.9.9/mhash-0.9.9.9.tar.gz
tar xvf mhash-0.9.9.9.tar.gz -C /usr/local/src/
cd /usr/local/src/mhash-0.9.9.9/

# 编译 安装
./configure --prefix=/usr/local/mhash
make && make install

# 配置环境变量
echo '/usr/local/mhash/lib' >> /etc/ld.so.conf
ldconfig -v | grep libmhash
```

#### mcrypt

```sh
# 下载 解压
cd /soft
wget -c https://sourceforge.net/projects/mcrypt/files/MCrypt/2.6.8/mcrypt-2.6.8.tar.gz
tar xvf mcrypt-2.6.8.tar.gz -C /usr/local/src/
cd /usr/local/src/mcrypt-2.6.8

# 配置编译变量
export LIBRARY_PATH=/usr/local/mhash/lib
export C_INCLUDE_PATH=/usr/local/mhash/include
ln -sf /usr/local/libmcrypt/bin/libmcrypt-config /usr/local/bin/libmcrypt-config

# 编译 安装
./configure --prefix=/usr/local/mcrypt --with-libmcrypt-prefix=/usr/local/libmcrypt \
    --with-libiconv-prefix=/usr/local/libiconv
make && make install

# 配置环境变量
mull
```

#### ICU 

```sh
# 下载 解压
cd /soft
wget -c http://download.icu-project.org/files/icu4c/60rc/icu4c-60rc-src.tgz
tar xvf icu4c-60rc-src.tgz -C /usr/local/src/
cd /usr/local/src/icu/source/

# 编译 安装
./configure --prefix=/usr/local/icu4c
make && make install

# 配置环境变量
echo '/usr/local/icu4c/lib' >> /etc/ld.so.conf
ldconfig -v | grep libicu
```

#### libxml2 

```sh
# 下载 解压
cd /soft
wget -c https://distfiles.macports.org/libxml2/libxml2-2.9.7.tar.gz
tar xvf libxml2-2.9.7.tar.gz -C /usr/local/src/
cd /usr/local/src/libxml2-2.9.7

# 编译 安装
./configure --prefix=/usr/local/libxml2 --disable-static --with-history --with-sax1 \
    --with-zlib=/usr/local/zlib --with-http --with-python=/usr/bin/python --with-iconv=/usr/local/libiconv
make && make install

# 配置环境变量
ln -sf /usr/local/libxml2/bin/xml2-config /usr/local/bin/xml2-config
echo '/usr/local/libxml2/lib' >> /etc/ld.so.conf
ldconfig -v | grep libxml2
```

#### libxslt 

```sh
# 下载 解压
cd /soft
wget -c http://distfiles.macports.org/libxslt/libxslt-1.1.32.tar.gz
tar xvf libxslt-1.1.32.tar.gz -C /usr/local/src/
cd /usr/local/src/libxslt-1.1.32

# 编译 安装
./configure --prefix=/usr/local/libxslt --disable-static --with-python=/usr/bin/python \
    --with-libxml-prefix=/usr/local/libxml2
make && make install

# 配置环境变量
ln -sf /usr/local/libxslt/bin/xslt-config /usr/local/bin/xslt-config
echo '/usr/local/libxslt/lib' >> /etc/ld.so.conf
ldconfig -v | grep libxslt
```

#### libjpeg

```sh
# 下载 解压
cd /soft
wget -c https://distfiles.macports.org/jpeg/jpegsrc.v9c.tar.gz
tar xvf jpegsrc.v9c.tar.gz -C /usr/local/src/
cd /usr/local/src/jpeg-9c

# 编译 安装
./configure --prefix=/usr/local/libjpeg --enable-shared --enable-static
make && make install

# 配置环境变量
echo '/usr/local/libjpeg/lib' >> /etc/ld.so.conf
ldconfig -v | grep libjpeg
```



#### libpng

```sh
# 下载 解压
cd /soft
wget -c http://distfiles.macports.org/libpng/libpng-1.6.34.tar.xz
tar xvf libpng-1.6.34.tar.xz -C /usr/local/src/
cd /usr/local/src/libpng-1.6.34

# 安装 补丁
wget -c https://downloads.sourceforge.net/sourceforge/libpng-apng/libpng-1.6.34-apng.patch.gz
gzip -d libpng-1.6.34-apng.patch.gz
patch -d . -p1 < libpng-1.6.34-apng.patch

# 编译 安装
export LIBRARY_PATH=/usr/local/zlib/lib
export C_INCLUDE_PATH=/usr/local/zlib/include
LIBS=-lpthread ./configure --prefix=/usr/local/libpng --disable-static --with-zlib-prefix=/usr/local/zlib
make && make install

# 配置环境变量
ln -sf /usr/local/libpng/bin/libpng-config /usr/local/bin/libpng-config
echo '/usr/local/libpng/lib' >> /etc/ld.so.conf
ldconfig -v | grep libpng

```

#### libwebp

```sh
# 下载 解压
cd /soft
wget -c http://downloads.webmproject.org/releases/webp/libwebp-1.0.0.tar.gz
tar xvf libwebp-1.0.0.tar.gz -C /usr/local/src/
cd /usr/local/src/libwebp-1.0.0/

# 编译 安装
./configure --prefix=/usr/local/libwebp --enable-libwebpmux --enable-libwebpdemux \
    --enable-libwebpdecoder --enable-libwebpextras --enable-swap-16bit-csp --disable-static \
    --with-jpeglibdir=/usr/local/libjpeg/lib --with-jpegincludedir=/usr/local/libjpeg/include \
    --with-pnglibdir=/usr/local/libpng/lib --with-pngincludedir=/usr/local/libpng/include
make && make install

# 配置环境变量
echo '/usr/local/libwebp/lib' >> /etc/ld.so.conf
ldconfig -v | grep libwebp
```

#### freetype2


```sh
# 下载 解压
cd /soft
wget -c https://download.savannah.gnu.org/releases/freetype/freetype-2.9.tar.gz
tar xvf freetype-2.9.tar.gz -C /usr/local/src/
cd /usr/local/src/freetype-2.9/

# 编译 安装
export LIBRARY_PATH=/usr/local/zlib/lib
export C_INCLUDE_PATH=/usr/local/zlib/include
./configure --prefix=/usr/local/freetype2  --disable-static --with-zlib=yes --with-png=yes --with-bzip2=yes
make && make install

# 配置环境变量
echo '/usr/local/freetype2/lib' >> /etc/ld.so.conf
ldconfig -v | grep libfreetype
ln -sf /usr/local/freetype2/include/freetype2/freetype/ /usr/local/freetype2/include/freetype
ln -sf /usr/local/freetype2/include/freetype2/ft2build.h /usr/local/freetype2/include/ft2build.h
```

#### libXpm

```sh
# 下载 解压
yum -y install libX11-devel
cd /soft
wget -c https://www.x.org/archive//individual/lib/libXpm-3.5.12.tar.gz
tar xvf libXpm-3.5.12.tar.gz -C /usr/local/src/
cd /usr/local/src/libXpm-3.5.12/

# 编译 安装
./configure --prefix=/usr/local/libXpm
make && make install

# 配置环境变量
echo '/usr/local/libXpm/lib' >> /etc/ld.so.conf
ldconfig -v | grep libXpm

```

#### libgd


```sh
# 下载 解压
cd /soft
wget -c https://github.com/libgd/libgd/releases/download/gd-2.2.5/libgd-2.2.5.tar.gz
tar xvf libgd-2.2.5.tar.gz -C /usr/local/src/
cd /usr/local/src/libgd-2.2.5/

# 编译 安装
export LIBS="-L/usr/local/freetype2/lib -lfreetype"
./configure --prefix=/usr/local/libgd --with-png=/usr/local/libpng \
    --with-webp=/usr/local/libwebp --with-freetype=/usr/local/freetype2 \
    --with-png=/usr/local/libpng --with-xpm=/usr/local/libXpm  \
    --with-jpeg=/usr/local/libjpeg --with-webp=/usr/local/libwebp \
    --with-libiconv-prefix=/usr/local/libiconv
make && make install

# 配置环境变量
echo '/usr/local/libgd/lib' >> /etc/ld.so.conf
ldconfig -v | grep libgd

```

### 安装php

> 创建运行用户

	run_user=www
	useradd -M -s /sbin/nologin ${run_user}

#### 安装

```sh
# 下载 解压
cd /soft
wget -c http://mirrors.sohu.com/php/php-7.1.17.tar.gz
tar xvf php-7.1.17.tar.gz -C /usr/local/src/
cd /usr/local/src/php-7.1.17/

# 编译 安装
./buildconf --force
export C_INCLUDE_PATH=/usr/local/libXpm/include:/usr/local/freetype2/include
./configure --prefix=/usr/local/php --with-config-file-path=/usr/local/php/etc \
    --with-config-file-scan-dir=/usr/local/php/etc/php.d --with-fpm-user=${run_user} \
    --with-fpm-group=${run_user} --enable-fpm --enable-opcache --enable-fileinfo \
    --enable-mysqlnd --with-mysqli=mysqlnd --with-pdo-mysql=mysqlnd \
    --with-iconv-dir=/usr/local/libiconv --with-freetype-dir=/usr/local/freetype2 \
    --with-jpeg-dir=/usr/local/libjpeg --with-png-dir=/usr/local/libpng --with-zlib=/usr/local/zlib \
    --with-webp-dir=/usr/local/libwebp --with-icu-dir=/usr/local/icu4c \
    --with-libxml-dir=/usr/local/libxml2 --enable-xml --disable-rpath --enable-bcmath \
    --enable-shmop --enable-exif --enable-sysvsem --enable-inline-optimization \
    --with-curl=/usr/local/curl --enable-mbregex --with-xpm-dir=/usr/local/libXpm \
    --enable-mbstring --with-mcrypt=/usr/local/libmcrypt --with-gd=/usr/local/libgd \
    --enable-gd-native-ttf --with-openssl=/usr/local/openssl --with-mhash=/usr/local/mhash \
    --enable-pcntl --enable-sockets --with-xmlrpc --enable-ftp --enable-intl --with-xsl=/usr/local/libxslt \
    --with-gettext --enable-zip --enable-soap --disable-debug 
make ZEND_EXTRA_LIBS='-liconv' && make install
# 配置环境变量
echo '/usr/local/php/lib' >> /etc/ld.so.conf
ldconfig -v | grep php
echo "export PATH=/usr/local/php/bin:\$PATH" >> /etc/profile
sed -i "s@^export PATH=\(.*\)@export PATH=/usr/local/php/sbin:\1@" /etc/profile
```

#### 配置`php.ini`

> 创建php.ini配置文件

	mkdir /usr/local/php/etc/php.d
	/bin/cp php.ini-production /usr/local/php/etc/php.ini

> 优化php.ini参数

```sh
# 获取系统内存参数
Mem=`free -m | awk '/Mem:/{print $2}'`

if [ $Mem -le 640 ]; then
    Mem_level=512M
    Memory_limit=64
    THREAD=1
elif [ $Mem -gt 640 -a $Mem -le 1280 ]; then
    Mem_level=1G
    Memory_limit=128
elif [ $Mem -gt 1280 -a $Mem -le 2500 ]; then
    Mem_level=2G
    Memory_limit=192
elif [ $Mem -gt 2500 -a $Mem -le 3500 ]; then
    Mem_level=3G
    Memory_limit=256
elif [ $Mem -gt 3500 -a $Mem -le 4500 ]; then
    Mem_level=4G
    Memory_limit=320
elif [ $Mem -gt 4500 -a $Mem -le 8000 ]; then
    Mem_level=6G
    Memory_limit=384
elif [ $Mem -gt 8000 ]; then
    Mem_level=8G
    Memory_limit=448
fi

# 优化参数
sed -i "s@^memory_limit.*@memory_limit = ${Memory_limit}M@" /usr/local/php/etc/php.ini
sed -i 's@^output_buffering =@output_buffering = On\noutput_buffering =@' /usr/local/php/etc/php.ini
sed -i 's@^;cgi.fix_pathinfo.*@cgi.fix_pathinfo=0@' /usr/local/php/etc/php.ini
sed -i 's@^short_open_tag = Off@short_open_tag = On@' /usr/local/php/etc/php.ini
sed -i 's@^expose_php = On@expose_php = Off@' /usr/local/php/etc/php.ini
sed -i 's@^request_order.*@request_order = "CGP"@' /usr/local/php/etc/php.ini
sed -i 's@^;date.timezone.*@date.timezone = Asia/Shanghai@' /usr/local/php/etc/php.ini
sed -i 's@^post_max_size.*@post_max_size = 100M@' /usr/local/php/etc/php.ini
sed -i 's@^upload_max_filesize.*@upload_max_filesize = 50M@' /usr/local/php/etc/php.ini
sed -i 's@^max_execution_time.*@max_execution_time = 600@' /usr/local/php/etc/php.ini
sed -i 's@^;realpath_cache_size.*@realpath_cache_size = 2M@' /usr/local/php/etc/php.ini
sed -i 's@^disable_functions.*@disable_functions = passthru,exec,system,chroot,chgrp,chown,shell_exec,proc_open,proc_get_status,ini_alter,ini_restore,dl,openlog,syslog,readlink,symlink,popepassthru,stream_socket_server,fsocket,popen@' /usr/local/php/etc/php.ini
[ -e /usr/sbin/sendmail ] && sed -i 's@^;sendmail_path.*@sendmail_path = /usr/sbin/sendmail -t -i@' /usr/local/php/etc/php.ini
```

#### 配置`php-opcache`

> 创建配置文件

```sh
cat <<EOF > /usr/local/php/etc/php.d/02-opcache.ini
[opcache]
zend_extension=opcache.so
opcache.enable=1
opcache.enable_cli=1
opcache.memory_consumption=${Memory_limit}
opcache.interned_strings_buffer=8
opcache.max_accelerated_files=100000
opcache.max_wasted_percentage=5
opcache.use_cwd=1
opcache.validate_timestamps=1
opcache.revalidate_freq=60
opcache.save_comments=0
opcache.fast_shutdown=1
opcache.consistency_checks=0
;opcache.optimization_level=0
EOF
```

#### 配置`php-fpm`

> 创建服务

	/bin/cp sapi/fpm/init.d.php-fpm /etc/init.d/php-fpm
	chmod +x /etc/init.d/php-fpm
	chkconfig --add php-fpm
	chkconfig php-fpm on

> 创建php-fpm配置文件


```sh
cat <<EOF > /usr/local/php/etc/php-fpm.conf 
;;;;;;;;;;;;;;;;;;;;;
; FPM Configuration ;
;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;
; Global Options ;
;;;;;;;;;;;;;;;;;;

[global]
pid = run/php-fpm.pid
error_log = log/php-fpm.log
log_level = warning

emergency_restart_threshold = 30
emergency_restart_interval = 60s
process_control_timeout = 5s
daemonize = yes

;;;;;;;;;;;;;;;;;;;;
; Pool Definitions ;
;;;;;;;;;;;;;;;;;;;;

[${run_user}]
listen = /dev/shm/php-cgi.sock
listen.backlog = -1
listen.allowed_clients = 127.0.0.1
listen.owner = ${run_user}
listen.group = ${run_user}
listen.mode = 0666
user = ${run_user}
group = ${run_user}

pm = dynamic
pm.max_children = 12
pm.start_servers = 8
pm.min_spare_servers = 6
pm.max_spare_servers = 12
pm.max_requests = 2048
pm.process_idle_timeout = 10s
request_terminate_timeout = 120
request_slowlog_timeout = 0

pm.status_path = /php-fpm_status
slowlog = log/slow.log
rlimit_files = 51200
rlimit_core = 0

catch_workers_output = yes
;env[HOSTNAME] = $HOSTNAME
env[PATH] = /usr/local/bin:/usr/bin:/bin
env[TMP] = /tmp
env[TMPDIR] = /tmp
env[TEMP] = /tmp
EOF
```
> `php-fpm`优化

```sh
if [ $Mem -le 3000 ]; then
    sed -i "s@^pm.max_children.*@pm.max_children = $(($Mem/3/20))@" /usr/local/php/etc/php-fpm.conf
    sed -i "s@^pm.start_servers.*@pm.start_servers = $(($Mem/3/30))@" /usr/local/php/etc/php-fpm.conf
    sed -i "s@^pm.min_spare_servers.*@pm.min_spare_servers = $(($Mem/3/40))@" /usr/local/php/etc/php-fpm.conf
    sed -i "s@^pm.max_spare_servers.*@pm.max_spare_servers = $(($Mem/3/20))@" /usr/local/php/etc/php-fpm.conf
elif [ $Mem -gt 3000 -a $Mem -le 4500 ]; then
    sed -i "s@^pm.max_children.*@pm.max_children = 50@" /usr/local/php/etc/php-fpm.conf
    sed -i "s@^pm.start_servers.*@pm.start_servers = 30@" /usr/local/php/etc/php-fpm.conf
    sed -i "s@^pm.min_spare_servers.*@pm.min_spare_servers = 20@" /usr/local/php/etc/php-fpm.conf
    sed -i "s@^pm.max_spare_servers.*@pm.max_spare_servers = 50@" /usr/local/php/etc/php-fpm.conf
elif [ $Mem -gt 4500 -a $Mem -le 6500 ]; then
    sed -i "s@^pm.max_children.*@pm.max_children = 60@" /usr/local/php/etc/php-fpm.conf
    sed -i "s@^pm.start_servers.*@pm.start_servers = 40@" /usr/local/php/etc/php-fpm.conf
    sed -i "s@^pm.min_spare_servers.*@pm.min_spare_servers = 30@" /usr/local/php/etc/php-fpm.conf
    sed -i "s@^pm.max_spare_servers.*@pm.max_spare_servers = 60@" /usr/local/php/etc/php-fpm.conf
elif [ $Mem -gt 6500 -a $Mem -le 8500 ]; then
    sed -i "s@^pm.max_children.*@pm.max_children = 70@" /usr/local/php/etc/php-fpm.conf
    sed -i "s@^pm.start_servers.*@pm.start_servers = 50@" /usr/local/php/etc/php-fpm.conf
    sed -i "s@^pm.min_spare_servers.*@pm.min_spare_servers = 40@" /usr/local/php/etc/php-fpm.conf
    sed -i "s@^pm.max_spare_servers.*@pm.max_spare_servers = 70@" /usr/local/php/etc/php-fpm.conf
elif [ $Mem -gt 8500 ]; then
    sed -i "s@^pm.max_children.*@pm.max_children = 80@" /usr/local/php/etc/php-fpm.conf
    sed -i "s@^pm.start_servers.*@pm.start_servers = 60@" /usr/local/php/etc/php-fpm.conf
    sed -i "s@^pm.min_spare_servers.*@pm.min_spare_servers = 50@" /usr/local/php/etc/php-fpm.conf
    sed -i "s@^pm.max_spare_servers.*@pm.max_spare_servers = 80@" /usr/local/php/etc/php-fpm.conf
fi
```

### 安装nginx

#### 准备工作

    # 创建运行用户
    run_user=www
    id -u ${run_user} >/dev/null 2>&1
    [ $? -ne 0 ] && useradd -M -s /sbin/nologin ${run_user}
    
    # 创建数据目录,权限
    mkdir -p /data/wwwlogs/
    chmod 640 /data/wwwlogs/
    mkdir -p /data/wwwroot/default/
    chmod 640 /data/wwwroot/default/ -R
    chown www.www /data/wwwroot/default/ -R

#### 安装

```sh
cd /soft
wget -c http://mirrors.sohu.com/nginx/nginx-1.14.0.tar.gz
tar -zxvf nginx-1.14.0.tar.gz -C /usr/local/src/
cd /usr/local/src/nginx-1.14.0
# close debug
sed -i 's@CFLAGS="$CFLAGS -g"@#CFLAGS="$CFLAGS -g"@' auto/cc/gcc
export LIBRARY_PATH=/usr/local/jemalloc/lib
./configure --prefix=/usr/local/nginx --user=${run_user} --group=${run_user} --with-pcre=/usr/local/src/pcre-8.42 \
    --with-pcre-jit --with-zlib=/usr/local/src/zlib-1.2.11/ --with-openssl=/usr/local/src/openssl-1.1.0h \
    --with-perl=/bin/perl --with-http_flv_module --with-http_mp4_module --with-http_gzip_static_module \
    --with-http_gunzip_module --with-http_auth_request_module --with-http_ssl_module --with-http_v2_module\
    --with-http_stub_status_module --with-http_realip_module --with-ld-opt='-ljemalloc'
make && make install

sed -i "s@^export PATH=\(.*\)@export PATH=/usr/local/nginx/sbin:\1@" /etc/profile
. /etc/profile
```

#### 配置服务

```sh
cat <<'EOF' >/etc/init.d/nginx
#!/bin/bash
#
# nginx - this script starts and stops the nginx daemon
#
# chkconfig:   - 85 15
# description:  NGINX is an HTTP(S) server, HTTP(S) reverse \
#               proxy and IMAP/POP3 proxy server
# processname: nginx
# config:      /etc/nginx/nginx.conf
# config:      /etc/sysconfig/nginx
# pidfile:     /var/run/nginx.pid

# Source function library.
. /etc/rc.d/init.d/functions

# Source networking configuration.
. /etc/sysconfig/network

# Check that networking is up.
[ "$NETWORKING" = "no" ] && exit 0

nginx="/usr/local/nginx/sbin/nginx"
prog=$(basename $nginx)

NGINX_CONF_FILE="/usr/local/nginx/conf/nginx.conf"

[ -f /etc/sysconfig/nginx ] && . /etc/sysconfig/nginx

lockfile=/var/lock/subsys/nginx

make_dirs() {
  # make required directories
  user=`$nginx -V 2>&1 | grep "configure arguments:.*--user=" | sed 's/[^*]*--user=\([^ ]*\).*/\1/g' -`
  if [ -n "$user" ]; then
    if [ -z "`grep $user /etc/passwd`" ]; then
      useradd -M -s /bin/nologin $user
    fi
    options=`$nginx -V 2>&1 | grep 'configure arguments:'`
    for opt in $options; do
        if [ `echo $opt | grep '.*-temp-path'` ]; then
          value=`echo $opt | cut -d "=" -f 2`
          if [ ! -d "$value" ]; then
            # echo "creating" $value
            mkdir -p $value && chown -R $user $value
          fi
        fi
    done
  fi
}

start() {
  [ -x $nginx ] || exit 5
  [ -f $NGINX_CONF_FILE ] || exit 6
  make_dirs
  echo -n $"Starting $prog: "
  daemon $nginx -c $NGINX_CONF_FILE
  retval=$?
  echo
  [ $retval -eq 0 ] && touch $lockfile
  return $retval
}

stop() {
  echo -n $"Stopping $prog: "
  killproc $prog -QUIT
  retval=$?
  echo
  [ $retval -eq 0 ] && rm -f $lockfile
  return $retval
}

restart() {
  configtest || return $?
  stop
  sleep 1
  start
}

reload() {
  configtest || return $?
  echo -n $"Reloading $prog: "
  killproc $nginx -HUP
  RETVAL=$?
  echo
}

force_reload() {
  restart
}

configtest() {
  $nginx -t -c $NGINX_CONF_FILE
}

rh_status() {
  status $prog
}

rh_status_q() {
  rh_status >/dev/null 2>&1
}

case "$1" in
  start)
    rh_status_q && exit 0
    $1
    ;;
  stop)
    rh_status_q || exit 0
    $1
    ;;
  restart|configtest)
    $1
    ;;
  reload)
    rh_status_q || exit 7
    $1
    ;;
  force-reload)
    force_reload
    ;;
  status)
    rh_status
    ;;
  condrestart|try-restart)
    rh_status_q || exit 0
    ;;
  *)
    echo $"Usage: $0 {start|stop|status|restart|reload|configtest}"
    exit 2
esac
EOF

chmod +x /etc/init.d/nginx
chkconfig --add nginx
chkconfig nginx on
sed -i "s@/usr/local/nginx@/usr/local/nginx@g" /etc/init.d/nginx
```

#### nginx配置文件

```sh
cat <<'EOF' >/usr/local/nginx/conf/nginx.conf
user www www;
worker_processes auto;

error_log /data/wwwlogs/error_nginx.log crit;
pid /var/run/nginx.pid;
worker_rlimit_nofile 51200;

events {
  use epoll;
  worker_connections 51200;
  multi_accept on;
}

http {
  include mime.types;
  default_type application/octet-stream;
  server_names_hash_bucket_size 128;
  client_header_buffer_size 32k;
  large_client_header_buffers 4 32k;
  client_max_body_size 1024m;
  client_body_buffer_size 10m;
  sendfile on;
  tcp_nopush on;
  keepalive_timeout 120;
  server_tokens off;
  tcp_nodelay on;

  fastcgi_connect_timeout 300;
  fastcgi_send_timeout 300;
  fastcgi_read_timeout 300;
  fastcgi_buffer_size 64k;
  fastcgi_buffers 4 64k;
  fastcgi_busy_buffers_size 128k;
  fastcgi_temp_file_write_size 128k;
  fastcgi_intercept_errors on;

  #Gzip Compression
  gzip on;
  gzip_buffers 16 8k;
  gzip_comp_level 6;
  gzip_http_version 1.1;
  gzip_min_length 256;
  gzip_proxied any;
  gzip_vary on;
  gzip_types
    text/xml application/xml application/atom+xml application/rss+xml application/xhtml+xml image/svg+xml
    text/javascript application/javascript application/x-javascript
    text/x-json application/json application/x-web-app-manifest+json
    text/css text/plain text/x-component
    font/opentype application/x-font-ttf application/vnd.ms-fontobject
    image/x-icon;
  gzip_disable "MSIE [1-6]\.(?!.*SV1)";

  #If you have a lot of static files to serve through Nginx then caching of the files' metadata (not the actual files' contents) can save some latency.
  open_file_cache max=1000 inactive=20s;
  open_file_cache_valid 30s;
  open_file_cache_min_uses 2;
  open_file_cache_errors on;

######################## default ############################
  server {
    listen 80;
    server_name _;
    access_log /data/wwwlogs/access_nginx.log combined;
    root /data/wwwroot/default;
    index index.html index.htm index.php;
    #error_page 404 /404.html;
    #error_page 502 /502.html;
    location /nginx_status {
      stub_status on;
      access_log off;
      allow 127.0.0.1;
      deny all;
    }
    location ~ [^/]\.php(/|$) {
      #fastcgi_pass remote_php_ip:9000;
      fastcgi_pass unix:/dev/shm/php-cgi.sock;
      fastcgi_index index.php;
      include fastcgi.conf;
    }
    location ~ .*\.(gif|jpg|jpeg|png|bmp|swf|flv|mp4|ico)$ {
      expires 30d;
      access_log off;
    }
    location ~ .*\.(js|css)?$ {
      expires 7d;
      access_log off;
    }
    location ~ /\.ht {
      deny all;
    }
  }
########################## vhost #############################
  include vhost/*.conf;
}
EOF

sed -i "s@/data/wwwroot/default@/data/wwwroot/default@" /usr/local/nginx/conf/nginx.conf
sed -i "s@/data/wwwlogs@/data/wwwlogs@g" /usr/local/nginx/conf/nginx.conf
sed -i "s@^user www www@user ${run_user} ${run_user}@" /usr/local/nginx/conf/nginx.conf
```

#### nginx-proxy

```sh
cat > /usr/local/nginx/conf/proxy.conf << EOF
proxy_connect_timeout 300s;
proxy_send_timeout 900;
proxy_read_timeout 900;
proxy_buffer_size 32k;
proxy_buffers 4 64k;
proxy_busy_buffers_size 128k;
proxy_redirect off;
proxy_hide_header Vary;
proxy_set_header Accept-Encoding '';
proxy_set_header Referer \$http_referer;
proxy_set_header Cookie \$http_cookie;
proxy_set_header Host \$host;
proxy_set_header X-Real-IP \$remote_addr;
proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
proxy_set_header X-Forwarded-Proto \$scheme;
EOF
```

#### nginx-log

```sh
cat > /etc/logrotate.d/nginx << EOF
/data/wwwlogs/*nginx.log {
    daily
    rotate 5
    missingok
    dateext
    compress
    notifempty
    sharedscripts
    postrotate
        [ -e /var/run/nginx.pid ] && kill -USR1 \`cat /var/run/nginx.pid\`
    endscript
}
EOF
```

### 安装探针

```sh
cd /soft
wget -c http://mirrors.linuxeye.com/oneinstack/src/tz.zip
unzip tz.zip -d /data/wwwroot/default
wget -O /data/wwwroot/default/index.php https://api.inn-studio.com/download?id=xprober
```



### 服务管理

- 注意：如果使用`service`方式启动服务则只能以`service`方式查看,关闭或重启服务
- 如果使用`systemctl`方式启动服务则只能以`systemctl`方式查看,关闭或重启服务

> service
    
    service php-fpm stop
    service php-fpm start
    service php-fpm status
    service php-fpm restart

    service nginx stop
    service nginx start
    service nginx status
    service nginx restart

> systemctl

    systemctl stop php-fpm
    systemctl start php-fpm
    systemctl status php-fpm
    systemctl restart php-fpm
    
    systemctl stop nginx
    systemctl start nginx
    systemctl status nginx
    systemctl restart nginx


### 一键安装脚本

```sh
#!/bin/sh

#################################################
# 初始化安装环境
#################################################

for i in gcc gcc-c++ vim wget ntpdate lrzsz python-devel patch unzip autoconf bzip2-devel; do yum -y install $i; done

#yum -y install gcc gcc-c++ vim wget ntpdate lrzsz python-devel patch unzip autoconf bzip2 bzip2-devel
mkdir /soft

setenforce 0
sed -i 's/SELINUX=.*/SELINUX=disabled/' /etc/selinux/config

rm -f /etc/localtime
ln -sf /usr/share/zoneinfo/Asia/Shanghai /etc/localtime

ntpdate ntp.aliyun.com && hwclock -w
echo "*/20 * * * * $(which ntpdate) pool.ntp.org > /dev/null 2>&1 && $(which hwclock) -w" >> /var/spool/cron/root
chmod 600 /var/spool/cron/root

startime=$(date +'%Y-%m-%d %H:%M:%S')


#################################################
# 系统 优化
#################################################
yum -y update bash glibc

cat <<EOF >/etc/profile.d/oneinstack.sh 
HISTSIZE=10000
PS1="\[\e[37;40m\][\[\e[32;40m\]\u\[\e[37;40m\]@\h \[\e[35;40m\]\W\[\e[0m\]]\\\\$ "
HISTTIMEFORMAT="%F %T \$(whoami) "

alias l='ls -AFhlt'
alias lh='l | head'
alias vi=vim

GREP_OPTIONS="--color=auto"
alias grep='grep --color'
alias egrep='egrep --color'
alias fgrep='fgrep --color'
EOF

[ -z "$(grep ^'PROMPT_COMMAND=' /etc/bashrc)" ] && cat <<EOF  >>/etc/bashrc 
PROMPT_COMMAND='{ msg=\$(history 1 | { read x y; echo \$y; });logger "[euid=\$(whoami)]":\$(who am i):[\`pwd\`]"\$msg"; }'
EOF

# /etc/security/limits.conf
[ -e /etc/security/limits.d/*nproc.conf ] && rename nproc.conf nproc.conf_bk /etc/security/limits.d/*nproc.conf
sed -i '/^# End of file/,$d' /etc/security/limits.conf
cat >> /etc/security/limits.conf <<EOF
# End of file
* soft nproc 1000000
* hard nproc 1000000
* soft nofile 1000000
* hard nofile 1000000
EOF

# /etc/hosts
[ "$(hostname -i | awk '{print $1}')" != "127.0.0.1" ] && sed -i "s@127.0.0.1.*localhost@&\n127.0.0.1 $(hostname)@g" /etc/hosts

# ip_conntrack table full dropping packets
[ ! -e "/etc/sysconfig/modules/iptables.modules" ] && { echo -e "modprobe nf_conntrack\nmodprobe nf_conntrack_ipv4" > /etc/sysconfig/modules/iptables.modules; chmod +x /etc/sysconfig/modules/iptables.modules; }
modprobe nf_conntrack
modprobe nf_conntrack_ipv4
echo options nf_conntrack hashsize=131072 > /etc/modprobe.d/nf_conntrack.conf

# /etc/sysctl.conf
[ ! -e "/etc/sysctl.conf_bk" ] && /bin/mv /etc/sysctl.conf{,_bk}
cat > /etc/sysctl.conf << EOF
fs.file-max=1000000
net.ipv4.tcp_max_tw_buckets = 6000
net.ipv4.tcp_sack = 1
net.ipv4.tcp_window_scaling = 1
net.ipv4.tcp_rmem = 4096 87380 4194304
net.ipv4.tcp_wmem = 4096 16384 4194304
net.ipv4.tcp_max_syn_backlog = 16384
net.core.netdev_max_backlog = 32768
net.core.somaxconn = 32768
net.core.wmem_default = 8388608
net.core.rmem_default = 8388608
net.core.rmem_max = 16777216
net.core.wmem_max = 16777216
net.ipv4.tcp_timestamps = 1
net.ipv4.tcp_fin_timeout = 20
net.ipv4.tcp_synack_retries = 2
net.ipv4.tcp_syn_retries = 2
net.ipv4.tcp_syncookies = 1
#net.ipv4.tcp_tw_len = 1
net.ipv4.tcp_tw_reuse = 1
net.ipv4.tcp_mem = 94500000 915000000 927000000
net.ipv4.tcp_max_orphans = 3276800
net.ipv4.ip_local_port_range = 1024 65000
net.nf_conntrack_max = 6553500
net.netfilter.nf_conntrack_max = 6553500
net.netfilter.nf_conntrack_tcp_timeout_close_wait = 60
net.netfilter.nf_conntrack_tcp_timeout_fin_wait = 120
net.netfilter.nf_conntrack_tcp_timeout_time_wait = 120
net.netfilter.nf_conntrack_tcp_timeout_established = 3600
EOF
sysctl -p

sed -i 's@LANG=.*$@LANG="en_US.UTF-8"@g' /etc/locale.conf

#################################################
# Zlib Install
#################################################
cd /soft
wget -c http://www.zlib.net/zlib-1.2.11.tar.gz
tar xvf zlib-1.2.11.tar.gz -C /usr/local/src/
cd /usr/local/src/zlib-1.2.11/
CFLAGS="-O3 -fPIC" ./configure --prefix=/usr/local/zlib  --shared
make && make install
export C_INCLUDE_PATH=/usr/local/zlib/include
echo '/usr/local/zlib/lib' >> /etc/ld.so.conf
ldconfig -v | grep libz


#################################################
# Openssl Install
#################################################
cd /soft
wget -c https://www.openssl.org/source/openssl-1.1.0h.tar.gz
tar xvf openssl-1.1.0h.tar.gz -C /usr/local/src/
cd /usr/local/src/openssl-1.1.0h/
./config -fPIC --prefix=/usr/local/openssl zlib-dynamic shared \
--openssldir=/usr/local/openssl
make && make install
wget -O /usr/local/openssl/cacert.pem http://curl.haxx.se/ca/cacert.pem
echo '/usr/local/openssl/lib' >> /etc/ld.so.conf
ldconfig -v | grep libssl.so
ldconfig -v | grep libcrypto.so


#################################################
# C-ares Install
#################################################
cd /soft
wget https://c-ares.haxx.se/download/c-ares-1.14.0.tar.gz
tar xvf c-ares-1.14.0.tar.gz -C /usr/local/src/
cd /usr/local/src/c-ares-1.14.0/
./configure --prefix=/usr/local/c-ares --disable-static
make && make install
echo '/usr/local/c-ares/lib' >> /etc/ld.so.conf
ldconfig -v | grep libcares.so


#################################################
# Curl Install
#################################################
cd /soft
wget -c https://curl.haxx.se/download/curl-7.59.0.tar.gz
tar xvf curl-7.59.0.tar.gz -C /usr/local/src/
cd /usr/local/src/curl-7.59.0
./configure --prefix=/usr/local/curl --disable-static \
    --enable-threaded-resolver --enable-ares=/usr/local/c-ares \
    --without-nss --with-ssl=/usr/local/openssl --with-zlib=/usr/local/zlib
make && make install
echo '/usr/local/curl/lib' >> /etc/ld.so.conf
ldconfig -v | grep libcurl


#################################################
# Pycurl Install (修复yum命令异常)
#################################################
cd /soft
wget -c https://dl.bintray.com/pycurl/pycurl/pycurl-7.43.0.1.tar.gz
tar xvf pycurl-7.43.0.1.tar.gz -C /usr/local/src/
cd /usr/local/src/pycurl-7.43.0.1/
python setup.py install --curl-config=/usr/local/curl/bin/curl-config \
--with-openssl --openssl-dir=/usr/local/openssl
ln -sf /usr/local/curl/bin/curl-config /usr/local/bin/curl-config
make && make install


#################################################
# Libiconv Install
#################################################
cd /soft
wget -c ftp.gnu.org/pub/gnu/libiconv/libiconv-1.15.tar.gz
tar xvf libiconv-1.15.tar.gz -C /usr/local/src/
cd /usr/local/src/libiconv-1.15/
./configure --prefix=/usr/local/libiconv
make && make install
echo '/usr/local/libiconv/lib' >> /etc/ld.so.conf
ldconfig -v | grep libiconv
ldconfig -v | grep libcharset


#################################################
# Libmcrypt Install
#################################################
cd /soft
wget -c https://jaist.dl.sourceforge.net/project/mcrypt/Libmcrypt/2.5.8/libmcrypt-2.5.8.tar.gz
tar xvf libmcrypt-2.5.8.tar.gz -C /usr/local/src/
cd /usr/local/src/libmcrypt-2.5.8
./configure --prefix=/usr/local/libmcrypt
make && make install
cd libltdl
./configure --prefix=/usr/local/libltdl --enable-ltdl-install
make && make install
echo '/usr/local/libmcrypt/lib' >> /etc/ld.so.conf
echo '/usr/local/libltdl/lib' >> /etc/ld.so.conf
ldconfig -v | grep libmcrypt
ldconfig -v | grep libltdl


#################################################
# Mhash Install
#################################################
cd /soft
wget -c https://jaist.dl.sourceforge.net/project/mhash/mhash/0.9.9.9/mhash-0.9.9.9.tar.gz
tar xvf mhash-0.9.9.9.tar.gz -C /usr/local/src/
cd /usr/local/src/mhash-0.9.9.9/
./configure --prefix=/usr/local/mhash
make && make install
echo '/usr/local/mhash/lib' >> /etc/ld.so.conf
ldconfig -v | grep libmhash


#################################################
# Mcrypt Install
#################################################
cd /soft
wget -c https://sourceforge.net/projects/mcrypt/files/MCrypt/2.6.8/mcrypt-2.6.8.tar.gz
tar xvf mcrypt-2.6.8.tar.gz -C /usr/local/src/
cd /usr/local/src/mcrypt-2.6.8
export LIBRARY_PATH=/usr/local/mhash/lib
export C_INCLUDE_PATH=/usr/local/mhash/include
ln -sf /usr/local/libmcrypt/bin/libmcrypt-config /usr/local/bin/libmcrypt-config
./configure --prefix=/usr/local/mcrypt --with-libmcrypt-prefix=/usr/local/libmcrypt \
    --with-libiconv-prefix=/usr/local/libiconv
make && make install


#################################################
# ICU Install
#################################################
cd /soft
wget -c http://download.icu-project.org/files/icu4c/60rc/icu4c-60rc-src.tgz
tar xvf icu4c-60rc-src.tgz -C /usr/local/src/
cd /usr/local/src/icu/source/
./configure --prefix=/usr/local/icu4c
make && make install
echo '/usr/local/icu4c/lib' >> /etc/ld.so.conf
ldconfig -v | grep libicu


#################################################
# Lbxml2 Install
#################################################
cd /soft
wget -c https://distfiles.macports.org/libxml2/libxml2-2.9.7.tar.gz
tar xvf libxml2-2.9.7.tar.gz -C /usr/local/src/
cd /usr/local/src/libxml2-2.9.7
./configure --prefix=/usr/local/libxml2 --disable-static --with-history --with-sax1 \
    --with-zlib=/usr/local/zlib --with-http --with-python=/usr/bin/python --with-iconv=/usr/local/libiconv
make && make install
ln -sf /usr/local/libxml2/bin/xml2-config /usr/local/bin/xml2-config
echo '/usr/local/libxml2/lib' >> /etc/ld.so.conf
ldconfig -v | grep libxml2


#################################################
# Libxslt Install
#################################################
cd /soft
wget -c http://distfiles.macports.org/libxslt/libxslt-1.1.32.tar.gz
tar xvf libxslt-1.1.32.tar.gz -C /usr/local/src/
cd /usr/local/src/libxslt-1.1.32
./configure --prefix=/usr/local/libxslt --disable-static --with-python=/usr/bin/python \
    --with-libxml-prefix=/usr/local/libxml2
make && make install
ln -sf /usr/local/libxslt/bin/xslt-config /usr/local/bin/xslt-config
echo '/usr/local/libxslt/lib' >> /etc/ld.so.conf
ldconfig -v | grep libxslt


#################################################
# Libjpeg Install
#################################################
cd /soft
wget -c https://distfiles.macports.org/jpeg/jpegsrc.v9c.tar.gz
tar xvf jpegsrc.v9c.tar.gz -C /usr/local/src/
cd /usr/local/src/jpeg-9c
./configure --prefix=/usr/local/libjpeg --enable-shared --enable-static
make && make install
echo '/usr/local/libjpeg/lib' >> /etc/ld.so.conf
ldconfig -v | grep libjpeg


#################################################
# Libpng Install
#################################################
cd /soft
wget -c http://distfiles.macports.org/libpng/libpng-1.6.34.tar.xz
tar xvf libpng-1.6.34.tar.xz -C /usr/local/src/
cd /usr/local/src/libpng-1.6.34
wget -c https://downloads.sourceforge.net/sourceforge/libpng-apng/libpng-1.6.34-apng.patch.gz
gzip -d libpng-1.6.34-apng.patch.gz
patch -d . -p1 < libpng-1.6.34-apng.patch
export LIBRARY_PATH=/usr/local/zlib/lib
export C_INCLUDE_PATH=/usr/local/zlib/include
LIBS=-lpthread ./configure --prefix=/usr/local/libpng --disable-static --with-zlib-prefix=/usr/local/zlib
make && make install
ln -sf /usr/local/libpng/bin/libpng-config /usr/local/bin/libpng-config
echo '/usr/local/libpng/lib' >> /etc/ld.so.conf
ldconfig -v | grep libpng


#################################################
# Libwebp Install
#################################################
cd /soft
wget -c http://downloads.webmproject.org/releases/webp/libwebp-1.0.0.tar.gz
tar xvf libwebp-1.0.0.tar.gz -C /usr/local/src/
cd /usr/local/src/libwebp-1.0.0/
./configure --prefix=/usr/local/libwebp --enable-libwebpmux --enable-libwebpdemux \
    --enable-libwebpdecoder --enable-libwebpextras --enable-swap-16bit-csp --disable-static \
    --with-jpeglibdir=/usr/local/libjpeg/lib --with-jpegincludedir=/usr/local/libjpeg/include \
    --with-pnglibdir=/usr/local/libpng/lib --with-pngincludedir=/usr/local/libpng/include
make && make install
echo '/usr/local/libwebp/lib' >> /etc/ld.so.conf
ldconfig -v | grep libwebp


#################################################
# Freetype Install
#################################################
cd /soft
wget -c https://download.savannah.gnu.org/releases/freetype/freetype-2.9.tar.gz
tar xvf freetype-2.9.tar.gz -C /usr/local/src/
cd /usr/local/src/freetype-2.9/
export LIBRARY_PATH=/usr/local/zlib/lib
export C_INCLUDE_PATH=/usr/local/zlib/include
./configure --prefix=/usr/local/freetype2  --disable-static --with-zlib=yes --with-png=yes --with-bzip2=yes
make && make install
echo '/usr/local/freetype2/lib' >> /etc/ld.so.conf
ldconfig -v | grep libfreetype
ln -sf /usr/local/freetype2/include/freetype2/freetype/ /usr/local/freetype2/include/freetype
ln -sf /usr/local/freetype2/include/freetype2/ft2build.h /usr/local/freetype2/include/ft2build.h


#################################################
# LibXpm Install
#################################################
yum -y install libX11-devel
cd /soft
wget -c https://www.x.org/archive//individual/lib/libXpm-3.5.12.tar.gz
tar xvf libXpm-3.5.12.tar.gz -C /usr/local/src/
cd /usr/local/src/libXpm-3.5.12/
./configure --prefix=/usr/local/libXpm
make && make install
echo '/usr/local/libXpm/lib' >> /etc/ld.so.conf
ldconfig -v | grep libXpm


#################################################
# Libgd Install
#################################################
cd /soft
wget -c https://github.com/libgd/libgd/releases/download/gd-2.2.5/libgd-2.2.5.tar.gz
tar xvf libgd-2.2.5.tar.gz -C /usr/local/src/
cd /usr/local/src/libgd-2.2.5/
export LIBS="-L/usr/local/freetype2/lib -lfreetype"
./configure --prefix=/usr/local/libgd --with-png=/usr/local/libpng \
    --with-webp=/usr/local/libwebp --with-freetype=/usr/local/freetype2 \
    --with-png=/usr/local/libpng --with-xpm=/usr/local/libXpm  \
    --with-jpeg=/usr/local/libjpeg --with-webp=/usr/local/libwebp \
    --with-libiconv-prefix=/usr/local/libiconv --with-zlib=/usr/local/zlib
make && make install
echo '/usr/local/libgd/lib' >> /etc/ld.so.conf
ldconfig -v | grep libgd


#################################################
# Pcre Install
#################################################
cd /soft
wget -c https://ftp.pcre.org/pub/pcre/pcre-8.42.tar.gz
tar -xvf pcre-8.42.tar.gz -C /usr/local/src/
cd /usr/local/src/pcre-8.42
./configure --prefix=/usr/local/pcre --enable-utf --enable-pcregrep-libz=/usr/local/zlib --enable-pcregrep-libbz2 --enable-jit --enable-pcre16 --enable-pcre32 --enable-unicode-properties
make && make install
echo '/usr/local/pcre/lib' >> /etc/ld.so.conf
ldconfig


#################################################
# Jemalloc Install
#################################################
cd /soft
wget -c https://github.com/jemalloc/jemalloc/releases/download/5.0.1/jemalloc-5.0.1.tar.bz2
tar xvf jemalloc-5.0.1.tar.bz2 -C /usr/local/src/
cd /usr/local/src/jemalloc-5.0.1/
./configure --prefix=/usr/local/jemalloc
make && make install
ln -s /usr/local/jemalloc/lib/libjemalloc.so.2 /usr/lib64/libjemalloc.so.1
echo '/usr/local/jemalloc/lib' >> /etc/ld.so.conf
ldconfig


#################################################
# PHP Install
#################################################
run_user=www
useradd -M -s /sbin/nologin ${run_user}
cd /soft
wget -c http://mirrors.sohu.com/php/php-7.1.17.tar.gz
tar xvf php-7.1.17.tar.gz -C /usr/local/src/
cd /usr/local/src/php-7.1.17/
./buildconf --force
export C_INCLUDE_PATH=/usr/local/libXpm/include:/usr/local/freetype2/include
./configure --prefix=/usr/local/php --with-config-file-path=/usr/local/php/etc \
    --with-config-file-scan-dir=/usr/local/php/etc/php.d --with-fpm-user=${run_user} \
    --with-fpm-group=${run_user} --enable-fpm --enable-opcache --enable-fileinfo \
    --enable-mysqlnd --with-mysqli=mysqlnd --with-pdo-mysql=mysqlnd \
    --with-iconv-dir=/usr/local/libiconv --with-freetype-dir=/usr/local/freetype2 \
    --with-jpeg-dir=/usr/local/libjpeg --with-png-dir=/usr/local/libpng --with-zlib=/usr/local/zlib \
    --with-webp-dir=/usr/local/libwebp --with-icu-dir=/usr/local/icu4c \
    --with-libxml-dir=/usr/local/libxml2 --enable-xml --disable-rpath --enable-bcmath \
    --enable-shmop --enable-exif --enable-sysvsem --enable-inline-optimization \
    --with-curl=/usr/local/curl --enable-mbregex --with-xpm-dir=/usr/local/libXpm \
    --enable-mbstring --with-mcrypt=/usr/local/libmcrypt --with-gd=/usr/local/libgd \
    --enable-gd-native-ttf --with-openssl=/usr/local/openssl --with-mhash=/usr/local/mhash \
    --enable-pcntl --enable-sockets --with-xmlrpc --enable-ftp --enable-intl --with-xsl=/usr/local/libxslt \
    --with-gettext --enable-zip --enable-soap --disable-debug --with-pcre-regex --with-pcre-jit --with-pcre-dir=/usr/local/pcre
make ZEND_EXTRA_LIBS='-liconv' && make install

echo '/usr/local/php/lib' >> /etc/ld.so.conf
ldconfig -v | grep php
echo "export PATH=/usr/local/php/bin:/usr/local/php/sbin:\$PATH" >> /etc/profile


#################################################
# PHP Config
#################################################
mkdir /usr/local/php/etc/php.d
/bin/cp php.ini-production /usr/local/php/etc/php.ini

Mem=`free -m | awk '/Mem:/{print $2}'`

if [ $Mem -le 640 ]; then
    Mem_level=512M
    Memory_limit=64
    THREAD=1
elif [ $Mem -gt 640 -a $Mem -le 1280 ]; then
    Mem_level=1G
    Memory_limit=128
elif [ $Mem -gt 1280 -a $Mem -le 2500 ]; then
    Mem_level=2G
    Memory_limit=192
elif [ $Mem -gt 2500 -a $Mem -le 3500 ]; then
    Mem_level=3G
    Memory_limit=256
elif [ $Mem -gt 3500 -a $Mem -le 4500 ]; then
    Mem_level=4G
    Memory_limit=320
elif [ $Mem -gt 4500 -a $Mem -le 8000 ]; then
    Mem_level=6G
    Memory_limit=384
elif [ $Mem -gt 8000 ]; then
    Mem_level=8G
    Memory_limit=448
fi

sed -i "s@^memory_limit.*@memory_limit = ${Memory_limit}M@" /usr/local/php/etc/php.ini
sed -i 's@^output_buffering =@output_buffering = On\noutput_buffering =@' /usr/local/php/etc/php.ini
sed -i 's@^;cgi.fix_pathinfo.*@cgi.fix_pathinfo=0@' /usr/local/php/etc/php.ini
sed -i 's@^short_open_tag = Off@short_open_tag = On@' /usr/local/php/etc/php.ini
sed -i 's@^expose_php = On@expose_php = Off@' /usr/local/php/etc/php.ini
sed -i 's@^request_order.*@request_order = "CGP"@' /usr/local/php/etc/php.ini
sed -i 's@^;date.timezone.*@date.timezone = Asia/Shanghai@' /usr/local/php/etc/php.ini
sed -i 's@^post_max_size.*@post_max_size = 100M@' /usr/local/php/etc/php.ini
sed -i 's@^upload_max_filesize.*@upload_max_filesize = 50M@' /usr/local/php/etc/php.ini
sed -i 's@^max_execution_time.*@max_execution_time = 600@' /usr/local/php/etc/php.ini
sed -i 's@^;realpath_cache_size.*@realpath_cache_size = 2M@' /usr/local/php/etc/php.ini
sed -i 's@^disable_functions.*@disable_functions = passthru,exec,system,chroot,chgrp,chown,shell_exec,proc_open,proc_get_status,ini_alter,ini_restore,dl,openlog,syslog,readlink,symlink,popepassthru,stream_socket_server,fsocket,popen@' /usr/local/php/etc/php.ini
[ -e /usr/sbin/sendmail ] && sed -i 's@^;sendmail_path.*@sendmail_path = /usr/sbin/sendmail -t -i@' /usr/local/php/etc/php.ini


#################################################
# PHP Opcache
#################################################
cat <<EOF > /usr/local/php/etc/php.d/02-opcache.ini
[opcache]
zend_extension=opcache.so
opcache.enable=1
opcache.enable_cli=1
opcache.memory_consumption=${Memory_limit}
opcache.interned_strings_buffer=8
opcache.max_accelerated_files=100000
opcache.max_wasted_percentage=5
opcache.use_cwd=1
opcache.validate_timestamps=1
opcache.revalidate_freq=60
opcache.save_comments=0
opcache.fast_shutdown=1
opcache.consistency_checks=0
;opcache.optimization_level=0
EOF


#################################################
# PHP Fpm Config
#################################################
/bin/cp sapi/fpm/init.d.php-fpm /etc/init.d/php-fpm
chmod +x /etc/init.d/php-fpm
chkconfig --add php-fpm
chkconfig php-fpm on

cat <<EOF > /usr/local/php/etc/php-fpm.conf 
;;;;;;;;;;;;;;;;;;;;;
; FPM Configuration ;
;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;
; Global Options ;
;;;;;;;;;;;;;;;;;;

[global]
pid = run/php-fpm.pid
error_log = log/php-fpm.log
log_level = warning

emergency_restart_threshold = 30
emergency_restart_interval = 60s
process_control_timeout = 5s
daemonize = yes

;;;;;;;;;;;;;;;;;;;;
; Pool Definitions ;
;;;;;;;;;;;;;;;;;;;;

[${run_user}]
listen = /dev/shm/php-cgi.sock
listen.backlog = -1
listen.allowed_clients = 127.0.0.1
listen.owner = ${run_user}
listen.group = ${run_user}
listen.mode = 0666
user = ${run_user}
group = ${run_user}

pm = dynamic
pm.max_children = 12
pm.start_servers = 8
pm.min_spare_servers = 6
pm.max_spare_servers = 12
pm.max_requests = 2048
pm.process_idle_timeout = 10s
request_terminate_timeout = 120
request_slowlog_timeout = 0

pm.status_path = /php-fpm_status
slowlog = log/slow.log
rlimit_files = 51200
rlimit_core = 0

catch_workers_output = yes
;env[HOSTNAME] = $HOSTNAME
env[PATH] = /usr/local/bin:/usr/bin:/bin
env[TMP] = /tmp
env[TMPDIR] = /tmp
env[TEMP] = /tmp
EOF

if [ $Mem -le 3000 ]; then
    sed -i "s@^pm.max_children.*@pm.max_children = $(($Mem/3/20))@" /usr/local/php/etc/php-fpm.conf
    sed -i "s@^pm.start_servers.*@pm.start_servers = $(($Mem/3/30))@" /usr/local/php/etc/php-fpm.conf
    sed -i "s@^pm.min_spare_servers.*@pm.min_spare_servers = $(($Mem/3/40))@" /usr/local/php/etc/php-fpm.conf
    sed -i "s@^pm.max_spare_servers.*@pm.max_spare_servers = $(($Mem/3/20))@" /usr/local/php/etc/php-fpm.conf
elif [ $Mem -gt 3000 -a $Mem -le 4500 ]; then
    sed -i "s@^pm.max_children.*@pm.max_children = 50@" /usr/local/php/etc/php-fpm.conf
    sed -i "s@^pm.start_servers.*@pm.start_servers = 30@" /usr/local/php/etc/php-fpm.conf
    sed -i "s@^pm.min_spare_servers.*@pm.min_spare_servers = 20@" /usr/local/php/etc/php-fpm.conf
    sed -i "s@^pm.max_spare_servers.*@pm.max_spare_servers = 50@" /usr/local/php/etc/php-fpm.conf
elif [ $Mem -gt 4500 -a $Mem -le 6500 ]; then
    sed -i "s@^pm.max_children.*@pm.max_children = 60@" /usr/local/php/etc/php-fpm.conf
    sed -i "s@^pm.start_servers.*@pm.start_servers = 40@" /usr/local/php/etc/php-fpm.conf
    sed -i "s@^pm.min_spare_servers.*@pm.min_spare_servers = 30@" /usr/local/php/etc/php-fpm.conf
    sed -i "s@^pm.max_spare_servers.*@pm.max_spare_servers = 60@" /usr/local/php/etc/php-fpm.conf
elif [ $Mem -gt 6500 -a $Mem -le 8500 ]; then
    sed -i "s@^pm.max_children.*@pm.max_children = 70@" /usr/local/php/etc/php-fpm.conf
    sed -i "s@^pm.start_servers.*@pm.start_servers = 50@" /usr/local/php/etc/php-fpm.conf
    sed -i "s@^pm.min_spare_servers.*@pm.min_spare_servers = 40@" /usr/local/php/etc/php-fpm.conf
    sed -i "s@^pm.max_spare_servers.*@pm.max_spare_servers = 70@" /usr/local/php/etc/php-fpm.conf
elif [ $Mem -gt 8500 ]; then
    sed -i "s@^pm.max_children.*@pm.max_children = 80@" /usr/local/php/etc/php-fpm.conf
    sed -i "s@^pm.start_servers.*@pm.start_servers = 60@" /usr/local/php/etc/php-fpm.conf
    sed -i "s@^pm.min_spare_servers.*@pm.min_spare_servers = 50@" /usr/local/php/etc/php-fpm.conf
    sed -i "s@^pm.max_spare_servers.*@pm.max_spare_servers = 80@" /usr/local/php/etc/php-fpm.conf
fi


#################################################
# Nginx Install
#################################################
run_user=www
id -u ${run_user} >/dev/null 2>&1
[ $? -ne 0 ] && useradd -M -s /sbin/nologin ${run_user}

mkdir -p /data/wwwlogs/
chmod 640 /data/wwwlogs/
mkdir -p /data/wwwroot/default/
chmod 640 /data/wwwroot/default/ -R
chown www.www /data/wwwroot/default/ -R

cd /soft
wget -c http://mirrors.sohu.com/nginx/nginx-1.14.0.tar.gz
tar -zxvf nginx-1.14.0.tar.gz -C /usr/local/src/
cd /usr/local/src/nginx-1.14.0
# close debug
sed -i 's@CFLAGS="$CFLAGS -g"@#CFLAGS="$CFLAGS -g"@' auto/cc/gcc
export LIBRARY_PATH=/usr/local/jemalloc/lib
./configure --prefix=/usr/local/nginx --user=${run_user} --group=${run_user} --with-pcre=/usr/local/src/pcre-8.42 \
    --with-pcre-jit --with-zlib=/usr/local/src/zlib-1.2.11/ --with-openssl=/usr/local/src/openssl-1.1.0h \
    --with-perl=/bin/perl --with-http_flv_module --with-http_mp4_module --with-http_gzip_static_module \
    --with-http_gunzip_module --with-http_auth_request_module --with-http_ssl_module --with-http_v2_module\
    --with-http_stub_status_module --with-http_realip_module --with-ld-opt='-ljemalloc'
make && make install

sed -i "s@^export PATH=\(.*\)@export PATH=/usr/local/nginx/sbin:\1@" /etc/profile
. /etc/profile

#sed -i "s@index index.html index.php;@index index.html index.php;\n    location ~ /php-fpm_status {\n        #fastcgi_pass remote_php_ip:9000;\n        fastcgi_pass unix:/dev/shm/php-cgi.sock;\n        fastcgi_index index.php;\n        include fastcgi.conf;\n        allow 127.0.0.1;\n        deny all;\n        }@" /usr/local/nginx/conf/nginx.conf


#################################################
# Nginx Init
#################################################
cat <<'EOF' >/etc/init.d/nginx
#!/bin/bash
#
# nginx - this script starts and stops the nginx daemon
#
# chkconfig:   - 85 15
# description:  NGINX is an HTTP(S) server, HTTP(S) reverse \
#               proxy and IMAP/POP3 proxy server
# processname: nginx
# config:      /etc/nginx/nginx.conf
# config:      /etc/sysconfig/nginx
# pidfile:     /var/run/nginx.pid

# Source function library.
. /etc/rc.d/init.d/functions

# Source networking configuration.
. /etc/sysconfig/network

# Check that networking is up.
[ "$NETWORKING" = "no" ] && exit 0

nginx="/usr/local/nginx/sbin/nginx"
prog=$(basename $nginx)

NGINX_CONF_FILE="/usr/local/nginx/conf/nginx.conf"

[ -f /etc/sysconfig/nginx ] && . /etc/sysconfig/nginx

lockfile=/var/lock/subsys/nginx

make_dirs() {
  # make required directories
  user=`$nginx -V 2>&1 | grep "configure arguments:.*--user=" | sed 's/[^*]*--user=\([^ ]*\).*/\1/g' -`
  if [ -n "$user" ]; then
    if [ -z "`grep $user /etc/passwd`" ]; then
      useradd -M -s /bin/nologin $user
    fi
    options=`$nginx -V 2>&1 | grep 'configure arguments:'`
    for opt in $options; do
        if [ `echo $opt | grep '.*-temp-path'` ]; then
          value=`echo $opt | cut -d "=" -f 2`
          if [ ! -d "$value" ]; then
            # echo "creating" $value
            mkdir -p $value && chown -R $user $value
          fi
        fi
    done
  fi
}

start() {
  [ -x $nginx ] || exit 5
  [ -f $NGINX_CONF_FILE ] || exit 6
  make_dirs
  echo -n $"Starting $prog: "
  daemon $nginx -c $NGINX_CONF_FILE
  retval=$?
  echo
  [ $retval -eq 0 ] && touch $lockfile
  return $retval
}

stop() {
  echo -n $"Stopping $prog: "
  killproc $prog -QUIT
  retval=$?
  echo
  [ $retval -eq 0 ] && rm -f $lockfile
  return $retval
}

restart() {
  configtest || return $?
  stop
  sleep 1
  start
}

reload() {
  configtest || return $?
  echo -n $"Reloading $prog: "
  killproc $nginx -HUP
  RETVAL=$?
  echo
}

force_reload() {
  restart
}

configtest() {
  $nginx -t -c $NGINX_CONF_FILE
}

rh_status() {
  status $prog
}

rh_status_q() {
  rh_status >/dev/null 2>&1
}

case "$1" in
  start)
    rh_status_q && exit 0
    $1
    ;;
  stop)
    rh_status_q || exit 0
    $1
    ;;
  restart|configtest)
    $1
    ;;
  reload)
    rh_status_q || exit 7
    $1
    ;;
  force-reload)
    force_reload
    ;;
  status)
    rh_status
    ;;
  condrestart|try-restart)
    rh_status_q || exit 0
    ;;
  *)
    echo $"Usage: $0 {start|stop|status|restart|reload|configtest}"
    exit 2
esac
EOF

chmod +x /etc/init.d/nginx
chkconfig --add nginx
chkconfig nginx on
sed -i "s@/usr/local/nginx@/usr/local/nginx@g" /etc/init.d/nginx


#################################################
# Nginx Config
#################################################
cat <<'EOF' >/usr/local/nginx/conf/nginx.conf
user www www;
worker_processes auto;

error_log /data/wwwlogs/error_nginx.log crit;
pid /var/run/nginx.pid;
worker_rlimit_nofile 51200;

events {
  use epoll;
  worker_connections 51200;
  multi_accept on;
}

http {
  include mime.types;
  default_type application/octet-stream;
  server_names_hash_bucket_size 128;
  client_header_buffer_size 32k;
  large_client_header_buffers 4 32k;
  client_max_body_size 1024m;
  client_body_buffer_size 10m;
  sendfile on;
  tcp_nopush on;
  keepalive_timeout 120;
  server_tokens off;
  tcp_nodelay on;

  fastcgi_connect_timeout 300;
  fastcgi_send_timeout 300;
  fastcgi_read_timeout 300;
  fastcgi_buffer_size 64k;
  fastcgi_buffers 4 64k;
  fastcgi_busy_buffers_size 128k;
  fastcgi_temp_file_write_size 128k;
  fastcgi_intercept_errors on;

  #Gzip Compression
  gzip on;
  gzip_buffers 16 8k;
  gzip_comp_level 6;
  gzip_http_version 1.1;
  gzip_min_length 256;
  gzip_proxied any;
  gzip_vary on;
  gzip_types
    text/xml application/xml application/atom+xml application/rss+xml application/xhtml+xml image/svg+xml
    text/javascript application/javascript application/x-javascript
    text/x-json application/json application/x-web-app-manifest+json
    text/css text/plain text/x-component
    font/opentype application/x-font-ttf application/vnd.ms-fontobject
    image/x-icon;
  gzip_disable "MSIE [1-6]\.(?!.*SV1)";

  #If you have a lot of static files to serve through Nginx then caching of the files' metadata (not the actual files' contents) can save some latency.
  open_file_cache max=1000 inactive=20s;
  open_file_cache_valid 30s;
  open_file_cache_min_uses 2;
  open_file_cache_errors on;

######################## default ############################
  server {
    listen 80;
    server_name _;
    access_log /data/wwwlogs/access_nginx.log combined;
    root /data/wwwroot/default;
    index index.html index.htm index.php;
    #error_page 404 /404.html;
    #error_page 502 /502.html;
    location /nginx_status {
      stub_status on;
      access_log off;
      allow 127.0.0.1;
      deny all;
    }
    location ~ [^/]\.php(/|$) {
      #fastcgi_pass remote_php_ip:9000;
      fastcgi_pass unix:/dev/shm/php-cgi.sock;
      fastcgi_index index.php;
      include fastcgi.conf;
    }
    location ~ .*\.(gif|jpg|jpeg|png|bmp|swf|flv|mp4|ico)$ {
      expires 30d;
      access_log off;
    }
    location ~ .*\.(js|css)?$ {
      expires 7d;
      access_log off;
    }
    location ~ /\.ht {
      deny all;
    }
  }
########################## vhost #############################
  include vhost/*.conf;
}
EOF

sed -i "s@/data/wwwroot/default@/data/wwwroot/default@" /usr/local/nginx/conf/nginx.conf
sed -i "s@/data/wwwlogs@/data/wwwlogs@g" /usr/local/nginx/conf/nginx.conf
sed -i "s@^user www www@user ${run_user} ${run_user}@" /usr/local/nginx/conf/nginx.conf


#################################################
# Nginx Proxy
#################################################
cat > /usr/local/nginx/conf/proxy.conf << EOF
proxy_connect_timeout 300s;
proxy_send_timeout 900;
proxy_read_timeout 900;
proxy_buffer_size 32k;
proxy_buffers 4 64k;
proxy_busy_buffers_size 128k;
proxy_redirect off;
proxy_hide_header Vary;
proxy_set_header Accept-Encoding '';
proxy_set_header Referer \$http_referer;
proxy_set_header Cookie \$http_cookie;
proxy_set_header Host \$host;
proxy_set_header X-Real-IP \$remote_addr;
proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
proxy_set_header X-Forwarded-Proto \$scheme;
EOF


#################################################
# Nginx Log
#################################################
cat > /etc/logrotate.d/nginx << EOF
/data/wwwlogs/*nginx.log {
    daily
    rotate 5
    missingok
    dateext
    compress
    notifempty
    sharedscripts
    postrotate
        [ -e /var/run/nginx.pid ] && kill -USR1 \`cat /var/run/nginx.pid\`
    endscript
}
EOF

ldconfig
service nginx start


#################################################
# 探针
#################################################
cd /soft
wget -c http://mirrors.linuxeye.com/oneinstack/src/tz.zip
unzip tz.zip -d /data/wwwroot/default
wget -O /data/wwwroot/default/index.php https://api.inn-studio.com/download?id=xprober


#################################################
# Service Manage
#################################################
echo '############# service php-fpm #############'
echo -e "service:\n"
echo -e "\tphp-fpm:"
echo -e "\t\tservice php-fpm stop"
echo -e "\t\tservice php-fpm start"
echo -e "\t\tservice php-fpm status"
echo -e "\t\tservice php-fpm restart"

echo -e "\n"

echo -e "\tnginx:"
echo -e "\t\tservice nginx stop"
echo -e "\t\tservice nginx start"
echo -e "\t\tservice nginx status"
echo -e "\t\tservice nginx restart"



echo -e "\n\n"
echo '############ systemctl php-fpm ############'
echo -e "systemctl:\n"
echo -e "\tphp-fpm:"
echo -e "\t\tsystemctl stop php-fpm"
echo -e "\t\tsystemctl start php-fpm"
echo -e "\t\tsystemctl status php-fpm"
echo -e "\t\tsystemctl restart php-fpm"

echo -e "\n"

echo -e "\tnginx:"
echo -e "\t\tsystemctl stop nginx"
echo -e "\t\tsystemctl start nginx"
echo -e "\t\tsystemctl status nginx"
echo -e "\t\tsystemctl restart nginx"

echo -e "\n\n"
echo '############# Time #############'
echo 'Start Time: '$startime
echo 'End Time: '$(date +'%Y-%m-%d %H:%M:%S')
```
