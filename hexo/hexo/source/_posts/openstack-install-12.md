title: OpenStack集群安装-13_创建虚拟机
tags:
  - openstack
  - null
copyright: true
categories:
  - openstack
abbrlink: openstack-13
date: 2018-08-01 00:34:01
---
## 上传公钥

    source ~/admin-openstack.sh
    nova keypair-add --pub-key ~/.ssh/id_dsa.pub mykey_controller1

## 创建可以用区域

    nova aggregate-create Dell01 Dell01
    nova aggregate-list
---

> 添加主机

    nova aggregate-add-host Dell01 computer01

## 创建安全规则

    openstack security group rule create --proto icmp default
    openstack security group rule create --proto tcp --dst-port 22 'default'

## 创建网络

### 获取本机网段

    IP=$(ip add|grep global|awk -F'[ /]+' '{ print $3 }'|head -n 1)
    IPS=$(echo $IP|awk -F\. '{ print $1"."$2"."$3 }')

### 创建网络

    openstack network create --share --external --provider-physical-network provider \
      --provider-network-type flat lan_$IPS

### 创建子网

    openstack subnet create --network lan_$IPS --allocation-pool start=$IPS.70,end=$IPS.100 \
      --dns-nameserver 223.5.5.5 --gateway $IPS.1 --subnet-range $IPS.0/24 net_$IPS

### 创建实例类型

    openstack flavor create --id 1 --vcpus 1 --ram 512 --disk 5  m1.nano
    openstack flavor create --id 2 --vcpus 1 --ram 512 --disk 12  m2.nano
    openstack flavor create --id 3 --vcpus 2 --ram 1024 --disk 12  m3.nano

## 上传镜像

### 下载测试镜像

    wget http://download.cirros-cloud.net/0.3.5/cirros-0.3.5-x86_64-disk.img
    wget http://download.cirros-cloud.net/0.4.0/cirros-0.4.0-x86_64-disk.img

### 上传镜像

> 使用qcow2磁盘格式, bare容器格式, 上传镜像到镜像服务并设置公共可见

    openstack image create "cirros-0.3.5" --file cirros-0.3.5-x86_64-disk.img \
      --disk-format qcow2 --container-format bare --public

    openstack image create "cirros-0.4.0" --file cirros-0.4.0-x86_64-disk.img \
      --disk-format qcow2 --container-format bare --public

### 创建虚拟机
    nova boot --flavor m1.nano --image cirros-0.3.5 --nic net-name=lan_$IPS \
      --security-group default --key-name mykey_controller1 VM01

    nova boot --flavor m1.nano --image cirros-0.4.0 --nic net-name=lan_$IPS \
      --security-group default --key-name mykey_controller1 VM02

### 验证

    openstack server list
    openstack console url show VM01