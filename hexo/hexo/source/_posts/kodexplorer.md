---
title: KodExplorer
tags:
  - php
  - session
categories:
  - php
abbrlink: e268
date: 2018-03-27 21:06:22
---

## 概观

> KodExplorer是web的文件管理器。它也是一个web代码编辑器，它允许您直接在web浏览器中开发网站。您可以在Linux、Windows或Mac平台上运行KodExplorer，无论是在线的还是本地的。唯一的要求是让PHP 5可用。

![](https://raw.githubusercontent.com/kalcaddle/static/master/images/kod/common2.png)
![](https://raw.githubusercontent.com/kalcaddle/static/master/images/kod/common3.png)


### [演示](http://demo.kodcloud.com/) [账号/密码: demo/demo]

- [更新日志](./ChangeLog.md)
- [英文文档](http://kodcloud.com#lang=en)
- [中文文档](http://kodcloud.com/#lang=zh_CN)
- [Donate](https://www.paypal.me/kalcaddle)

### 源码

- [github](https://github.com/kalcaddle/KodExplorer)
- [gitee](https://gitee.com/kalcaddle/KODExplorer)

----

## 特征

- 使用操作系统，Rich上下文菜单和工具栏，拖放，快捷键......
- 提供40多种语言。
- 文件管理
    - 所有对远程服务器上的文件和文件夹的操作（复制，可爱，粘贴，移动，删除，上传，创建文件夹/文件，重命名等）
    - 多用户支持，自定义角色组。
    - 灵活配置访问权限，文件类型限制，用户界面和其他
    - 剪贴板：复制，剪切，粘贴，清除
    - 可选的文件和文件夹支持（鼠标点击＆Ctrl＆Shift＆字＆键盘快捷键）
    - 键盘快捷键：删除删除，ctrl + A选择，ctrl + C复制，ctrl + X拼接，上/下/左/右/首页/结束等。
    - 多个操作支持选定的文件和文件夹：移动，复制，可爱，删除，重命名，打开，存档，删除，下载等。
    - 双击或单击即可打开文件和文件夹
    - Filetree：允许一次打开并显示多个子文件夹
    - 在客户端实现自然排序
    - 列表，图标和分割视图;
    - 使用拖放移动/复制/克隆/删除文件
    - 将文件或文件夹共享给其他人。
    - 将文件夹添加到收藏夹
    - 计算目录大小
    - 图像文件的缩略图
    - 规范器：文件名和文件路径的UTF-8规范器等。
    - Muti Charset支持，在各种情况下出现乱码的解决方案;文件名和文件路径等的清理工具
    - 支持多个分块上传，
    - 通过拖放HTML5支持进行背景文件上传;使用Chrome，Firefox和Edge进行文件夹上传
    - 上传表单网址（或列表）
    - 直接提取到当前工作目录（你不想 - 创建一个文件夹）
    - 搜索：按文件名和文件内容搜索
    - 基于名称的文件排除
    - 复制直接文件的URL
    - 档案创建/提取/预览（zip，rar，7z，tar，gzip，tgz）
    - Quicklook，预览常见文件类型; 图像文件，文本文件，pdf，swf，文档文件等
    - 视频和音频播放器依靠网络浏览器功能

- 编辑
    - 语法高亮超过120种语言
    - 多个标签，拖放标签。
    - 超过15个主题，选择您最喜爱的编程风格
    - Web开发：集成了Emmet的HTML / JS / CSS编辑器
    - 自动缩进和缩进;换行;代码折叠
    - 多个游标和选择;（中键选择; Ctrl + Command + G）
    - 自动完成。
    - 完全可定制的键绑定，包括vim和Emacs模式
    - 用正则表达式搜索并替换;突出显示匹配的括号
    - 在软标签和真正的标签之间切换
    - 显示隐藏的字符
    - 使用鼠标拖放文本
    - 现场语法检查器（JavaScript / CoffeeScript / CSS / XQuery / HTML / PHP等）
    - 剪切，复制和粘贴功能
    - Markdown支持（实时预览;转换为html等）
    - 格式：JavaScript / CSS / HTML / JSON / PHP等
    - 跨平台，即使在移动设备上
    - 易于与其他系统集成
    - 由科德本身开发，这是一个不错的尝试。
    - Syntax highlighting for over 120 languages

----

## 安装

**1. 源码安装**

```
git clone https://github.com/kalcaddle/KODExplorer.git
chmod -Rf 777 ./KODExplorer/*
```

**2. 下载安装**

```
wget https://github.com/kalcaddle/KODExplorer/archive/master.zip
unzip master.zip
chmod -Rf 777 ./*
```

----

## 常见问题解答

* 忘记密码
    > 登录页面：请参阅“忘记密码”。

* 通过拖放上传
    > 浏览器兼容性：Chrome，Firefox和Edge

* 如何使系统更安全？
    > 确保管理员密码更复杂。
    > 打开登录验证码。
    > 设置http服务器不允许列出目录;
    >PHP安全性：设置open_basedir的路径。

----

## 截图

### 文件管理:
- 概观
![概观](https://raw.githubusercontent.com/kalcaddle/static/master/images/kod/file.png)

- 文件分类 (图标,列表,分割)
![文件分类](https://raw.githubusercontent.com/kalcaddle/static/master/images/kod/file-resize.png)

- 档案创建/提取/预览(zip, rar, 7z, tar, gzip, tgz)
![档案创建/提取/预览](https://raw.githubusercontent.com/kalcaddle/static/master/images/kod/file-unzip.png)

- 拖动上传
![拖动上传](https://raw.githubusercontent.com/kalcaddle/static/master/images/kod/file-upload-drag.png)

- 播放器
![播放器](https://raw.githubusercontent.com/kalcaddle/static/master/images/kod/file-player.png)

- 在线办公室视图和编辑器
![Online Office](https://raw.githubusercontent.com/kalcaddle/static/master/images/kod/file-open-pptx.png)


### 编辑器:
- 概观
![Overview](https://raw.githubusercontent.com/kalcaddle/static/master/images/kod/editor.png)

- 实时预览
![Live preview](https://raw.githubusercontent.com/kalcaddle/static/master/images/kod/editor-preview.png)

- 搜索文件夹
![Search folder](https://raw.githubusercontent.com/kalcaddle/static/master/images/kod/editor-search.png)

- Markdown
![Markdown](https://raw.githubusercontent.com/kalcaddle/static/master/images/kod/file-markdown.png)

- 代码样式
![Code style](https://raw.githubusercontent.com/kalcaddle/static/master/images/kod/editor-theme.png)


### 其他:
- 用户管理
![System role](https://raw.githubusercontent.com/kalcaddle/static/master/images/kod/system-role.png)

- 多彩的主题
![Colorful Theme](https://raw.githubusercontent.com/kalcaddle/static/master/images/kod/system-theme.png)

- 自定义主题
![Custom Theme](https://raw.githubusercontent.com/kalcaddle/static/master/images/kod/common-alpha.png)

- 语言
![Language](https://raw.githubusercontent.com/kalcaddle/static/master/images/kod/language.png)

----

## 软件需求

- 服务器：
    - Windows，Linux，Mac ...
    - PHP 5.0+
    - 数据库：文件系统驱动程序; sqlite; mysql; ...

- 浏览器兼容性：
    - Chrome
    - Firefox
    - Opera
    - IE8 +

>提示：它也可以在路由器或家庭NAS上运行

----

## Credits
kod 是由以下开源项目成为可能

* [seajs](https://github.com/seajs/seajs) 
* [jQuery](https://github.com/jquery/jquery)
* [ace](https://github.com/ajaxorg/ace)
* [zTree](https://github.com/zTree/zTree_v3) 
* [webuploader](https://github.com/fex-team/webuploader) 
* [artTemplate](http://aui.github.com/artTemplate/)
* [artDialog](https://github.com/aui/artDialog)
* [jQuery-contextMenu](http://medialize.github.com/jQuery-contextMenu/) 
* ...

----

## 许可信息

kodcloud is issued under GPLv3.   license.[License](http://kodcloud.com/tools/licenses/license.txt)  
Contact: warlee#kodcloud.com
Copyright (C) 2013 kodcloud.com

----

## 版权声明

kodexplorer 使用 GPL v3 协议.

