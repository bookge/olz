---
title: RHEL7 重置root密码
tags:
  - linux
  - 密码
categories:
  - linux
abbrlink: 86a0
date: 2017-12-13 17:46:34
---
## 进入单用户模式

重启Linux系统主机并出现引导界面时，按下键盘上的e键进入内核编辑界面

![](/images/chpasswd-01.png)

在linux16参数这行的最后面追加`rd.break`，然后按下`Ctrl + X`组合键来运行修改过的内核程序

![](/images/chpasswd-02.png)


## 重新挂载/sysroot

    mount -o remount,rw /sysroot

## chroot,修改密码
>chroot 把根目录换成指定的目的目录

    chroot /sysroot
    passwd

## 创建安全标签

    touch /.autorelabel

## 退出,重启

    exit
    reboot
