title: 搭建mirror镜像站
tags:
  - mirror
  - yum
  - linux
copyright: true
categories:
  - linux
abbrlink: eafe
date: 2018-04-27 18:03:05
---
### 环境

>[下载镜像 792MB](http://mirrors.163.com/centos/7.4.1708/isos/x86_64/CentOS-7-x86_64-Minimal-1708.iso)

    系统：CentOS7.4-Mini-1708
    IP：192.168.3.4

### 安装rsync

    yum -y install rsync

### 创建存储目录

    mkdir /mirror /scripts

### 创建同步源脚本

> [rsync 同步方式](https://mirrors.ustc.edu.cn/help/rsync-guide.html)

```sh
cat <<'EOF' >/scripts/ustc_rsync.sh
#!/bin/bash
if ! which rsync >/dev/null 2>&1;then exit 1;fi
#rsync://mirrors.163.com/centos/7/

rsyncUrl=(

    rsync://rsync.mirrors.ustc.edu.cn/repo/centos/7/
    rsync://rsync.mirrors.ustc.edu.cn/repo/epel/7/
)


localDir=(
    /data/mirror/centos/7/
    /data/mirror/epel/7/
)


rsync_Mirrors() {
    #rsync -vai4CH --safe-links --numeric-ids --delete --delete-delay --delay-updates $1 $2
    rsync -avrt --progress $1 $2
}


#https://mariadb.com/kb/en/mariadb/mirroring-mariadb/
#https://www.percona.com/blog/2014/02/14/using-percona-rsync-repositories-set-local-centos-mirror/
#http://dev.mysql.com/downloads/how-to-mirror.html

for i in `seq 0 ${#localDir[@]}`;do
    [ ! -d ${localDir[$i]} ] && mkdir -p ${localDir[$i]}
    rsync_Mirrors ${rsyncUrl[$i]} ${localDir[$i]}
    [ $? -eq 0 ] && createrepo ${localDir[$i]}
done
EOF
```

### 配置脚本执行权限

    chmod +x /scripts/ustc_rsync.sh

### 执行同步
>同步时间比较长CentOS7，epel源加起来大概 207G

    /scripts/ustc_rsync.sh

### 定时运行同步

    # 每6小时执行一次
    echo '1 6 * * *  /bin/sh  /scripts/ustc_rsync.sh >/mirror/rsync.log 2>&1' >>/var/spool/cron/root

### Web提供YUN

#### Python方式(不推荐)

> 运行以下命令后浏览器打开：http://Server IP:8000 可看到列出的目录

    cd /mirror
    /usr/bin/nohup /usr/bin/python -m SimpleHTTPServer 8000 >/mirror/python-http.log 2>&1 &

#### Nginx方式

> 编译安装Nginx此编译方法可能导致YUM命令故障[参考](http://192.168.2.3/posts/7ae3.html) (可用YUM方式安装)
编译Nginx时可以直接指定 pcre, zlib, openssl 源码路径而无需编译安装这三个软件(查看nginx编译参数)

##### 准备工作

    # 安装GCC
    yum -y install gcc gcc-c++ wget vim

    # 创建软件下载目录
    mkdir /root/software

##### 安装pcre

>轻量级的Perl函数库

    cd /root/software
    wget https://ftp.pcre.org/pub/pcre/pcre-8.42.tar.gz
    tar -zxvf pcre-8.42.tar.gz -C /usr/src/
    cd /usr/src/pcre-8.42
    ./configure --prefix=/usr/local/pcre
    make
    make install

##### 安装zlib

>提供数据压缩用的函式库

    cd /root/software
    wget http://zlib.net/zlib-1.2.11.tar.gz
    tar -zxvf zlib-1.2.11.tar.gz -C /usr/src/
    cd /usr/src/zlib-1.2.11
    ./configure --prefix=/usr/local/zlib
    make
    make install
    ln -sf /usr/local/zlib/include/* /usr/local/include/

##### 安装openssl

    yum -y remove openssl openssl-devel
    yum -y install perl
    cd /root/software
    wget https://www.openssl.org/source/openssl-1.1.1-pre5.tar.gz
    tar -zxvf openssl-1.1.1-pre5.tar.gz -C /usr/src/
    cd /usr/src/openssl-1.1.1-pre5
    ./config --prefix=/usr/local/openssl
    make
    make install

    # 链接openssl 库文件
    echo "/usr/local/openssl/lib" >/etc/ld.so.conf.d/openssl.conf
    ldconfig

    # 命令路径添加到环境变量
    echo 'export PATH=/usr/local/openssl/bin/:$PATH' >>/etc/profile
    source /etc/profile

##### 安装nginx

    cd /root/software
    wget http://mirrors.sohu.com/nginx/nginx-1.14.0.tar.gz
    tar -zxvf nginx-1.14.0.tar.gz -C /usr/src/
    cd /usr/src/nginx-1.14.0
    ./configure --prefix=/usr/local/nginx/ --with-pcre=/usr/src/pcre-8.42/ --with-zlib=/usr/src/zlib-1.2.11/ --with-openssl=/usr/local/openssl/
    make
    make install

    # 命令路径添加到环境变量
    echo 'export PATH=/usr/local/nginx/sbin/:$PATH' >>/etc/profile
    source /etc/profile

##### 配置Nginx

    mkdir -p /data/mirror
    cd /usr/local/nginx/conf/
    cp -a nginx.conf nginx.conf.bak
    sed -i '44s/root.*/root   \/data\/mirror;/' nginx.conf
    sed -i '44a\            autoindex on;' nginx.conf
    sed -i '45a\            autoindex_exact_size off;' nginx.conf
    sed -i '46a\            autoindex_localtime on;' nginx.conf
    sed -i '48d' nginx.conf

    # 启动服务
    systemctl start nginx

### 配置repo文件

>此处在服务器上执行,用于提供客户端下载
>baseurl中配置的地址为服务器地址


```sh
cat <<'EOF' >/mirror/local.repo
# CentOS-Base.repo
#
# The mirror system uses the connecting IP address of the client and the
# update status of each mirror to pick mirrors that are updated to and
# geographically close to the client.  You should use this for CentOS updates
# unless you are manually picking other mirrors.
#
# If the mirrorlist= does not work for you, as a fall back you can try the
# remarked out baseurl= line instead.
#
#

[base]
name=CentOS-$releasever - Base
#mirrorlist=http://mirrorlist.centos.org/?release=$releasever&arch=$basearch&repo=os
baseurl=http://192.168.3.4:8000/centos/$releasever/os/$basearch/
gpgcheck=1
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-7

#released updates
[updates]
name=CentOS-$releasever - Updates
# mirrorlist=http://mirrorlist.centos.org/?release=$releasever&arch=$basearch&repo=updates
baseurl=http://192.168.3.4:8000/centos/$releasever/updates/$basearch/
gpgcheck=1
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-7

#additional packages that may be useful
[extras]
name=CentOS-$releasever - Extras
# mirrorlist=http://mirrorlist.centos.org/?release=$releasever&arch=$basearch&repo=extras
baseurl=http://192.168.3.4:8000/centos/$releasever/extras/$basearch/
gpgcheck=1
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-7

#additional packages that extend functionality of existing packages
[centosplus]
name=CentOS-$releasever - Plus
# mirrorlist=http://mirrorlist.centos.org/?release=$releasever&arch=$basearch&repo=centosplus
baseurl=http://192.168.3.4:8000/centos/$releasever/centosplus/$basearch/
gpgcheck=1
enabled=0
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-7

[epel]
name=Extra Packages for Enterprise Linux 7 - $basearch
baseurl=http://192.168.3.4:8000/epel/7/$basearch
#mirrorlist=https://mirrors.fedoraproject.org/metalink?repo=epel-7&arch=$basearch
failovermethod=priority
enabled=1
gpgcheck=0
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-EPEL-7

[epel-debuginfo]
name=Extra Packages for Enterprise Linux 7 - $basearch - Debug
baseurl=http://192.168.3.4:8000/epel/7/$basearch/debug
#mirrorlist=https://mirrors.fedoraproject.org/metalink?repo=epel-debug-7&arch=$basearch
failovermethod=priority
enabled=0
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-EPEL-7
gpgcheck=0

[epel-source]
name=Extra Packages for Enterprise Linux 7 - $basearch - Source
baseurl=http://192.168.3.4:8000/epel/7/SRPMS
#mirrorlist=https://mirrors.fedoraproject.org/metalink?repo=epel-source-7&arch=$basearch
failovermethod=priority
enabled=0
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-EPEL-7
gpgcheck=0
EOF
```

### 客户端配置YUM源

    # 安装epel源
    yum -y install epel-release

    # 备份旧的yum源
    mkdir /etc/yum.repos.d/old/
    mv /etc/yum.repos.d/*.repo /etc/yum.repos.d/old/

    # 配置YUM源
    curl -o /etc/yum.repos.d/local.repo http://192.168.3.4:8000/local.repo

    # 更新缓存
    yum clean all
    yum makecache

本文所同步的源来自[中国科学技术大学](http://mirrors.ustc.edu.cn/)
YUM配置文件基于[CentOS Help](http://mirrors.ustc.edu.cn/help/centos.html) 所修改
