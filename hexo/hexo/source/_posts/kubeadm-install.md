title: kubeadm脚本快速安装
author: 亦 漩
abbrlink: ce34
date: 2018-07-03 16:04:55
tags:
---
## kubeadm-master

```shell
#!/bin/bash

############
#  初始化  #
############

# host
echo '192.168.0.11 node01' >>/etc/hosts
echo '192.168.0.22 node02' >>/etc/hosts
echo '192.168.0.33 node03' >>/etc/hosts
echo '192.168.0.44 node04' >>/etc/hosts

# host name
hostnamectl set-hostname node00
hostnamectl status

# 配置阿里yum源
yum -y install epel-release
rm -f /etc/yum.repos.d/*
curl -o /etc/yum.repos.d/Centos-7.repo http://mirrors.aliyun.com/repo/Centos-7.repo
curl -o /etc/yum.repos.d/epel-7.repo http://mirrors.aliyun.com/repo/epel-7.repo
sed -i '/aliyuncs.com/d' /etc/yum.repos.d/*.repo
yum makecache

# Selinux, Firewalld
systemctl stop firewalld
systemctl disable firewalld
firewall-cmd --state
setenforce 0
sed -i '/^SELINUX=.*/c SELINUX=disabled' /etc/selinux/config
grep --color=auto '^SELINUX' /etc/selinux/config

# 常用工具
yum -y install wget vim ntpdate net-tools tree lrzsz lsof

# 时间同步
ntpdate ntp.aliyun.com && hwclock -w
echo "*/20 * * * * /usr/sbin/ntpdate pool.ntp.org > /dev/null 2>&1 && /usr/sbin/hwclock -w" >/tmp/crontab
crontab /tmp/crontab

# 关闭 Swap
swapoff -a 
sed -i 's/.*swap.*/#&/' /etc/fstab

# 内核参数优化
echo 'net.ipv4.ip_forward = 1' > /etc/sysctl.d/k8s.conf
echo 'net.bridge.bridge-nf-call-ip6tables = 1' >> /etc/sysctl.d/k8s.conf
echo 'net.bridge.bridge-nf-call-iptables = 1' >> /etc/sysctl.d/k8s.conf
echo 'vm.swappiness=0' >> /etc/sysctl.d/k8s.conf
sysctl -p /etc/sysctl.d/k8s.conf
echo "sysctl -p /etc/sysctl.d/k8s.conf" >>/etc/profile

echo '# myset' >> /etc/security/limits.conf
echo '* soft nofile 65536' >> /etc/security/limits.conf
echo '* hard nofile 65536' >> /etc/security/limits.conf
echo '* soft nproc 65536' >> /etc/security/limits.conf
echo '* hard nproc 65536' >> /etc/security/limits.conf
echo '* soft  memlock  unlimited' >> /etc/security/limits.conf
echo '* hard memlock  unlimited' >> /etc/security/limits.conf

#####################
#  Docker 环境安装  #
#####################
# curl https://coding.net/u/yx571304/p/Docker/git/raw/master/install.sh | sh
# 卸载旧版
yum -y remove docker \
    docker-client \
    docker-client-latest \
    docker-common \
    docker-latest \
    docker-latest-logrotate \
    docker-logrotate \
    docker-selinux \
    docker-engine-selinux \
    docker-engine

# 安装依赖
yum -y install yum-utils \
    device-mapper-persistent-data \
    lvm2

# 添加安装源
yum-config-manager \
    --add-repo \
    https://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo

# 安装 Docker CE
yum makecache fast
yum -y install docker-ce-17.06.0.ce

# 配置 Docker镜像加速
mkdir /etc/docker
cat <<EOF >/etc/docker/daemon.json
{
  "registry-mirrors": ["http://3272dd08.m.daocloud.io"]
}
EOF

# 启动服务
systemctl start docker
systemctl enable docker

# 验证安装
docker -v
docker version

# 配置安装源
cat <<'EOF' >/etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://mirrors.aliyun.com/kubernetes/yum/repos/kubernetes-el7-x86_64
enabled=1
gpgcheck=0
EOF

# kubeadm 及相关工具包
yum -y install kubelet-1.10.0 kubeadm-1.10.0 kubectl-1.10.0 kubernetes-cni

# 更换 kubelet 驱动
sed -i 's/driver=systemd/driver=cgroupfs/' /etc/systemd/system/kubelet.service.d/10-kubeadm.conf

# 启动服务配置开机启动
systemctl daemon-reload
systemctl enable kubelet

# 下载镜像
docker pull registry.cn-shanghai.aliyuncs.com/alik8s/kube-proxy-amd64:v1.10.0
docker pull registry.cn-shanghai.aliyuncs.com/alik8s/kube-controller-manager-amd64:v1.10.0
docker pull registry.cn-shanghai.aliyuncs.com/alik8s/kube-scheduler-amd64:v1.10.0
docker pull registry.cn-shanghai.aliyuncs.com/alik8s/kube-apiserver-amd64:v1.10.0
docker pull registry.cn-shanghai.aliyuncs.com/alik8s/etcd-amd64:3.1.12
docker pull registry.cn-shanghai.aliyuncs.com/alik8s/kubernetes-dashboard-amd64:v1.8.3
docker pull registry.cn-shanghai.aliyuncs.com/alik8s/heapster-grafana-amd64:v4.4.3
docker pull registry.cn-shanghai.aliyuncs.com/alik8s/heapster-influxdb-amd64:v1.3.3
docker pull registry.cn-shanghai.aliyuncs.com/alik8s/heapster-amd64:v1.4.2
docker pull registry.cn-shanghai.aliyuncs.com/alik8s/k8s-dns-dnsmasq-nanny-amd64:1.14.8
docker pull registry.cn-shanghai.aliyuncs.com/alik8s/k8s-dns-sidecar-amd64:1.14.8
docker pull registry.cn-shanghai.aliyuncs.com/alik8s/k8s-dns-kube-dns-amd64:1.14.8
docker pull registry.cn-shanghai.aliyuncs.com/alik8s/pause-amd64:3.1
docker pull registry.cn-shanghai.aliyuncs.com/alik8s/flannel:v0.10.0-amd64

# 镜像打标记
docker tag registry.cn-shanghai.aliyuncs.com/alik8s/kube-proxy-amd64:v1.10.0 k8s.gcr.io/kube-proxy-amd64:v1.10.0
docker tag registry.cn-shanghai.aliyuncs.com/alik8s/kube-controller-manager-amd64:v1.10.0 k8s.gcr.io/kube-controller-manager-amd64:v1.10.0
docker tag registry.cn-shanghai.aliyuncs.com/alik8s/kube-scheduler-amd64:v1.10.0 k8s.gcr.io/kube-scheduler-amd64:v1.10.0
docker tag registry.cn-shanghai.aliyuncs.com/alik8s/kube-apiserver-amd64:v1.10.0 k8s.gcr.io/kube-apiserver-amd64:v1.10.0
docker tag registry.cn-shanghai.aliyuncs.com/alik8s/etcd-amd64:3.1.12 k8s.gcr.io/etcd-amd64:3.1.12
docker tag registry.cn-shanghai.aliyuncs.com/alik8s/kubernetes-dashboard-amd64:v1.8.3 k8s.gcr.io/kubernetes-dashboard-amd64:v1.8.3
docker tag registry.cn-shanghai.aliyuncs.com/alik8s/heapster-grafana-amd64:v4.4.3 k8s.gcr.io/heapster-grafana-amd64:v4.4.3
docker tag registry.cn-shanghai.aliyuncs.com/alik8s/heapster-influxdb-amd64:v1.3.3 k8s.gcr.io/heapster-influxdb-amd64:v1.3.3
docker tag registry.cn-shanghai.aliyuncs.com/alik8s/heapster-amd64:v1.4.2 k8s.gcr.io/heapster-amd64:v1.4.2
docker tag registry.cn-shanghai.aliyuncs.com/alik8s/k8s-dns-dnsmasq-nanny-amd64:1.14.8 k8s.gcr.io/k8s-dns-dnsmasq-nanny-amd64:1.14.8
docker tag registry.cn-shanghai.aliyuncs.com/alik8s/k8s-dns-sidecar-amd64:1.14.8 k8s.gcr.io/k8s-dns-sidecar-amd64:1.14.8
docker tag registry.cn-shanghai.aliyuncs.com/alik8s/k8s-dns-kube-dns-amd64:1.14.8 k8s.gcr.io/k8s-dns-kube-dns-amd64:1.14.8
docker tag registry.cn-shanghai.aliyuncs.com/alik8s/pause-amd64:3.1 k8s.gcr.io/pause-amd64:3.1
docker tag registry.cn-shanghai.aliyuncs.com/alik8s/flannel:v0.10.0-amd64  quay.io/coreos/flannel:v0.10.0-amd64


# 删除源镜像
docker rmi registry.cn-shanghai.aliyuncs.com/alik8s/kube-proxy-amd64:v1.10.0
docker rmi registry.cn-shanghai.aliyuncs.com/alik8s/kube-controller-manager-amd64:v1.10.0
docker rmi registry.cn-shanghai.aliyuncs.com/alik8s/kube-scheduler-amd64:v1.10.0
docker rmi registry.cn-shanghai.aliyuncs.com/alik8s/kube-apiserver-amd64:v1.10.0
docker rmi registry.cn-shanghai.aliyuncs.com/alik8s/etcd-amd64:3.1.12
docker rmi registry.cn-shanghai.aliyuncs.com/alik8s/kubernetes-dashboard-amd64:v1.8.3
docker rmi registry.cn-shanghai.aliyuncs.com/alik8s/heapster-grafana-amd64:v4.4.3
docker rmi registry.cn-shanghai.aliyuncs.com/alik8s/heapster-influxdb-amd64:v1.3.3
docker rmi registry.cn-shanghai.aliyuncs.com/alik8s/heapster-amd64:v1.4.2
docker rmi registry.cn-shanghai.aliyuncs.com/alik8s/k8s-dns-dnsmasq-nanny-amd64:1.14.8
docker rmi registry.cn-shanghai.aliyuncs.com/alik8s/k8s-dns-sidecar-amd64:1.14.8
docker rmi registry.cn-shanghai.aliyuncs.com/alik8s/k8s-dns-kube-dns-amd64:1.14.8
docker rmi registry.cn-shanghai.aliyuncs.com/alik8s/pause-amd64:3.1
docker rmi registry.cn-shanghai.aliyuncs.com/alik8s/flannel:v0.10.0-amd64

# 下载模板
mkdir -p $HOME/k8s/heapster
curl -s http://elven.vip/ks/k8s/oneinstall/yml/kube-flannel.yml > $HOME/k8s/kube-flannel.yml
curl -s http://elven.vip/ks/k8s/oneinstall/yml/kubernetes-dashboard.yaml > $HOME/k8s/kubernetes-dashboard.yaml
curl -s http://elven.vip/ks/k8s/oneinstall/yml/heapster-rbac.yaml > $HOME/k8s/heapster-rbac.yaml
curl -s http://elven.vip/ks/k8s/oneinstall/yml/heapster/influxdb.yaml > $HOME/k8s/heapster/influxdb.yaml
curl -s http://elven.vip/ks/k8s/oneinstall/yml/heapster/heapster.yaml > $HOME/k8s/heapster/heapster.yaml
curl -s http://elven.vip/ks/k8s/oneinstall/yml/heapster/grafana.yaml > $HOME/k8s/heapster/grafana.yaml

# 镜像列表
docker images |egrep 'k8s.gcr.io|quay.io'

# 部署master节点[单节点安装]
# 重置
kubeadm reset

# 初始化安装
cd $HOME/k8s
kubeadm init --kubernetes-version=v1.10.0  --pod-network-cidr=10.244.0.0/16 |tee /tmp/install.log

# k8s node节点代码保存到 $HOME/k8s.add.node.txt
grep 'kubeadm join' /tmp/install.log >$HOME/k8s.add.node.txt
rm -f /tmp/install.log 
sleep 2

# 默认token有效期24小时，生成一个永不过期的
Token=`kubeadm token create --ttl 0`
# echo $Token
# kubeadm token list
sed -i -r "s#(.*) --token (.*) --discovery(.*)#\1 --token $Token --discovery\3#" $HOME/k8s.add.node.txt

# kubectl认证
export KUBECONFIG=/etc/kubernetes/admin.conf
echo "export KUBECONFIG=/etc/kubernetes/admin.conf" >> ~/.bash_profile
##
echo "# used for kubectl ,k8s" >/etc/profile
echo "export KUBECONFIG=/etc/kubernetes/admin.conf" >>/etc/profile

sleep 5
echo -e "\033[32m查看K8S状态\033[0m" 
kubectl get cs

# 让master也运行pod
kubectl taint nodes --all node-role.kubernetes.io/master-

cd $HOME/
echo -e "\033[32m部署flannel网络 \033[0m" 
kubectl create -f k8s/kube-flannel.yml
sleep 5
echo  
echo -e "\033[32m部署dashboard\033[0m" 
kubectl create -f k8s/kubernetes-dashboard.yaml
# dashboard监控图形化
sleep 5
kubectl create -f k8s/heapster/
kubectl create -f k8s/heapster-rbac.yaml
sleep 10
echo -e "\033[32m查看pod \033[0m" 
kubectl get pods --all-namespaces

echo  
echo -e "\033[32mdashboard登录令牌,保存到$HOME/k8s.token.dashboard.txt\033[0m" 
kubectl -n kube-system describe secret $(kubectl -n kube-system get secret | grep admin-user | awk '{print $1}') |awk '/token:/{print$2}' >$HOME/k8s.token.dashboard.txt

# kubectl get node 报错
cp /etc/kubernetes/admin.conf $HOME/
chown $(id -u):$(id -g) $HOME/admin.conf
export KUBECONFIG=$HOME/admin.conf
kubectl get nodes

echo 'dashboard登录令牌如下:'
echo  
cat $HOME/k8s.token.dashboard.txt
echo  
echo 'dashboard登录地址 https://本机IP:30000即: '
IP=`sed -r 's#^.*join (.*):6443.*$#\1#' $HOME/k8s.add.node.txt`
echo "  https://$IP:30000"
echo  
echo '登录dashboard，输入令牌token'
echo '推荐 火狐浏览器'
echo '若提示 不安全的连接, 高级->添加例外'
echo  
echo  
echo -e "\033[32m添加k8s node节点代码如下:\033[0m" 
echo  
cat $HOME/k8s.add.node.txt
echo  
echo  "重新登录查看Node    kubectl get nodes"
echo  "添加完node节点后执行 kubectl get nodes 查看节点"
exit

```

---

## kubeadm-node

```sh
#!/bin/bash

trap "exit 0" 2

thead_num=6

tmp_fifo_file="/tmp/$$.fifo"

mkfifo $tmp_fifo_file

exec 4<>$tmp_fifo_file

rm -f $tmp_fifo_file

for ((i=0;i<$thead_num;i++))
do
    echo >&4
done 

while read line
do
    read -u4
    {
    ip=$(echo $line | awk '{print $1}')
    host=$(echo $line | awk '{print $2}')
    echo $ip
    ssh -T root@$host <<EOF
    {
        hostnamectl set-hostname $host
        hostnamectl status
        
        # host
        echo '192.168.2.60 node00' >>/etc/hosts
        echo '192.168.2.61 node01' >>/etc/hosts
        echo '192.168.2.62 node02' >>/etc/hosts
        echo '192.168.2.63 node03' >>/etc/hosts
        echo '192.168.2.64 node04' >>/etc/hosts
        echo '192.168.2.65 node05' >>/etc/hosts

        yum -y install epel-release
        rm -f /etc/yum.repos.d/*
        curl -o /etc/yum.repos.d/Centos-7.repo http://mirrors.aliyun.com/repo/Centos-7.repo
        curl -o /etc/yum.repos.d/epel-7.repo http://mirrors.aliyun.com/repo/epel-7.repo
        sed -i '/aliyuncs.com/d' /etc/yum.repos.d/*.repo

        # Selinux, Firewalld
        systemctl stop firewalld
        systemctl disable firewalld
        firewall-cmd --state
        setenforce 0
        sed -i '/^SELINUX=.*/c SELINUX=disabled' /etc/selinux/config
        grep --color=auto '^SELINUX' /etc/selinux/config

        yum -y install wget vim ntpdate net-tools tree lrzsz lsof

        ntpdate ntp.aliyun.com && hwclock -w
        echo "*/20 * * * * /usr/sbin/ntpdate pool.ntp.org > /dev/null 2>&1 && /usr/sbin/hwclock -w" >/tmp/crontab
        crontab /tmp/crontab

        swapoff -a 
        sed -i 's/.*swap.*/#&/' /etc/fstab
        
        [ "$ip" == "192.168.0.11" ] && continue

        echo 'net.ipv4.ip_forward = 1' > /etc/sysctl.d/k8s.conf
        echo 'net.bridge.bridge-nf-call-ip6tables = 1' >> /etc/sysctl.d/k8s.conf
        echo 'net.bridge.bridge-nf-call-iptables = 1' >> /etc/sysctl.d/k8s.conf
        echo 'vm.swappiness=0' >> /etc/sysctl.d/k8s.conf
        sysctl -p /etc/sysctl.d/k8s.conf
        echo "sysctl -p /etc/sysctl.d/k8s.conf" >>/etc/profile

        echo '# myset' >> /etc/security/limits.conf
        echo '* soft nofile 65536' >> /etc/security/limits.conf
        echo '* hard nofile 65536' >> /etc/security/limits.conf
        echo '* soft nproc 65536' >> /etc/security/limits.conf
        echo '* hard nproc 65536' >> /etc/security/limits.conf
        echo '* soft  memlock  unlimited' >> /etc/security/limits.conf
        echo '* hard memlock  unlimited' >> /etc/security/limits.conf

        yum -y remove docker docker-client docker-client-latest docker-common docker-latest docker-latest-logrotate docker-logrotate docker-selinux docker-engine-selinux docker-engine

        yum -y install yum-utils device-mapper-persistent-data lvm2

        yum-config-manager --add-repo https://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo

        yum makecache fast
        yum -y install docker-ce-17.06.0.ce

        mkdir /etc/docker
        echo '{' >/etc/docker/daemon.json
        echo '  "registry-mirrors": ["http://3272dd08.m.daocloud.io"]' >>/etc/docker/daemon.json
        echo '}' >>/etc/docker/daemon.json


        systemctl start docker
        systemctl enable docker

        docker -v
        docker version


        echo '[kubernetes]' >/etc/yum.repos.d/kubernetes.repo
        echo 'name=Kubernetes' >>/etc/yum.repos.d/kubernetes.repo
        echo 'baseurl=https://mirrors.aliyun.com/kubernetes/yum/repos/kubernetes-el7-x86_64' >>/etc/yum.repos.d/kubernetes.repo
        echo 'enabled=1' >>/etc/yum.repos.d/kubernetes.repo
        echo 'gpgcheck=0' >>/etc/yum.repos.d/kubernetes.repo


        yum -y install kubelet-1.10.0 kubeadm-1.10.0 kubectl-1.10.0 kubernetes-cni

        sed -i 's/driver=systemd/driver=cgroupfs/' /etc/systemd/system/kubelet.service.d/10-kubeadm.conf

        systemctl daemon-reload
        systemctl enable kubelet

        docker pull registry.cn-shanghai.aliyuncs.com/alik8s/kube-proxy-amd64:v1.10.0
        docker pull registry.cn-shanghai.aliyuncs.com/alik8s/pause-amd64:3.1
        docker pull registry.cn-shanghai.aliyuncs.com/alik8s/flannel:v0.10.0-amd64

        docker tag registry.cn-shanghai.aliyuncs.com/alik8s/kube-proxy-amd64:v1.10.0 k8s.gcr.io/kube-proxy-amd64:v1.10.0
        docker tag registry.cn-shanghai.aliyuncs.com/alik8s/pause-amd64:3.1 k8s.gcr.io/pause-amd64:3.1
        docker tag registry.cn-shanghai.aliyuncs.com/alik8s/flannel:v0.10.0-amd64  quay.io/coreos/flannel:v0.10.0-amd64


        docker rmi registry.cn-shanghai.aliyuncs.com/alik8s/kube-proxy-amd64:v1.10.0
        docker rmi registry.cn-shanghai.aliyuncs.com/alik8s/pause-amd64:3.1
        docker rmi registry.cn-shanghai.aliyuncs.com/alik8s/flannel:v0.10.0-amd64

        docker images |egrep 'k8s.gcr.io|quay.io'     

    }  >/tmp/$host.log 2>&1
EOF
    echo "" >&4
} >/dev/null 2>&1 &
done < ./list

wait

exec 4<&-

exec 4>&-

echo "安装完毕后请在在node节点执行 master主机上/root/k8s.add.node.txt 中的命令 "

exit 0

```

---