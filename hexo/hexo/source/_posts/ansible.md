title: ansible
tags:
  - null
  - null
copyright: true
categories:
  - null
abbrlink: ea2c
date: 2018-07-13 23:21:03
---
## 安装

>[官方文档](http://www.ansible.com.cn/docs/intro_installation.html)

### 配置 YUM, EPEL

    yum -y install epel-release
    rm -f /etc/yum.repos.d/*
    curl -o /etc/yum.repos.d/Centos-7.repo http://mirrors.aliyun.com/repo/Centos-7.repo
    curl -o /etc/yum.repos.d/epel-7.repo http://mirrors.aliyun.com/repo/epel-7.repo
    sed -i '/aliyuncs.com/d' /etc/yum.repos.d/*.repo

### 安装Ansible

    yum -y install ansible

---

## 配置

### 1.使用秘钥

#### 生成秘钥

    [ -f ~/.ssh/id_dsa ] || ssh-keygen -t dsa -P '' -f ~/.ssh/id_dsa;

#### 手动发送公钥

    ssh-copy-id -i ~/.ssh/id_dsa.pub 192.168.0.11
    ssh-copy-id -i ~/.ssh/id_dsa.pub 192.168.0.12
    ssh-copy-id -i ~/.ssh/id_dsa.pub 192.168.0.13
    ssh-copy-id -i ~/.ssh/id_dsa.pub 192.168.0.14
    ssh-copy-id -i ~/.ssh/id_dsa.pub 192.168.0.15
    ssh-copy-id -i ~/.ssh/id_dsa.pub 192.168.0.16

#### 自动发送公钥

    # 安装自动交互工具
    [ -f /usr/bin/expect ] || yum -y install expect;

    # 自答应函数
    ssh_remote(){
    key=$1
    host=$2
    user=$3
    pass=$4
    /usr/bin/expect <<EOF
        set timeout 30
        spawn ssh-copy-id -i $key $user@$host
        expect {
            "(yes/no)?" { send "yes\r"; exp_continue }
            "password:" { send "$pass\r" }
        }
        expect eof
    EOF
    }

    # 发送公钥到被管理端 [函数, 公钥, 被管主机, 用户名, 密码]
    ssh_remote ~/.ssh/id_dsa.pub 192.168.0.11 root redhat
    ssh_remote ~/.ssh/id_dsa.pub 192.168.0.22 root redhat
    ssh_remote ~/.ssh/id_dsa.pub 192.168.0.33 root redhat
    ssh_remote ~/.ssh/id_dsa.pub 192.168.0.44 root redhat
    ssh_remote ~/.ssh/id_dsa.pub 192.168.0.55 root redhat
    ssh_remote ~/.ssh/id_dsa.pub 192.168.0.66 root redhat

#### 添加远程主机

    cat <<EOF >>/etc/ansible/hosts
    # $(date +'%F %T')
    [master]
    192.168.0.11
    192.168.0.22
    192.168.0.33

    [node]
    192.168.0.44
    192.168.0.55
    192.168.0.66
    EOF

---

### 2.使用密码

> 此方法存在安全隐患不建议使用

    cat <<EOF >>/etc/ansible/hosts
    192.168.0.11 ansible_ssh_port=22 ansible_ssh_user=root ansible_ssh_pass=redhat
    192.168.0.12 ansible_ssh_port=22 ansible_ssh_user=root ansible_ssh_pass=redhat
    192.168.0.13 ansible_ssh_port=22 ansible_ssh_user=root ansible_ssh_pass=redhat
    192.168.0.14 ansible_ssh_port=22 ansible_ssh_user=root ansible_ssh_pass=redhat
    192.168.0.15 ansible_ssh_port=22 ansible_ssh_user=root ansible_ssh_pass=redhat
    192.168.0.16 ansible_ssh_port=22 ansible_ssh_user=root ansible_ssh_pass=redhat
    EOF

---

## Ansible模块


### ping模块

    # ping所有主机
    ansible all -m ping

    # ping master 组的所有主机
    ansible master -m ping

    # ping node 组的所有主机
    ansible node -m ping

    # 指定被管主机用户执行命令
    # 条件：远程主机必须有用户 feng 本地当前用户有权限ssh 被管主机feng 的权限
    ansible node -m ping -u feng

---

>远程命令模块(command/script/shell)

---

### command模块

> command 作为 Ansible 的默认模块，可以运行远程权限范围所有的 shell 命令，不支持管道符

    ansible all -m command -a "/bin/echo hello"            # ansible all -a "/bin/echo hello"
    ansible all -m command -a "date '+%F %T'"              # ansible all -a "date '+%F %T'"
    ansible all -m command -a "free -m"                    # ansible all -a "free -m"
    ansible all -a "echo redhat | passwd --stdin feng"     # 更改所有主机上用户 feng 的密码为 redhat

### script模块

> script 的功能是在远程主机执行主控端存储的 shell 脚本文件，相当于 scp + shell 组合

    echo -e '#!/bin/bash\n\ndate +"%F %T"' >/tmp/date.sh   # 打印当前日期时间脚本
    ansible all -m script -a "/tmp/date.sh"                # 所有被管理端执行脚本

### shell模块

> shell 的功能是执行远程主机上的 shell 脚本文件，支持管道符

    ansible master -m shell -a "/tmp/date.sh"

### copy模块

> copy 模块(实现主控端向目标主机拷贝文件，类似于 scp 功能)

    # 向master 组中主机拷贝 test.sh 到 /tmp 下
    ansible master -m copy -a 'src=/tmp/date.sh dest=/tmp/'

    # 向master 组中主机拷贝 test.sh 到 /tmp 下，属主、组为 root ，权限为 0755
    ansible master -m copy -a "src=/tmp/date.sh dest=/tmp/ owner=root group=root mode=0755"

### stat 模块

> stat 模块(获取远程文件状态信息，atime/ctime/mtime/md5/uid/gid 等信息)

    ansible all -m stat -a "path=/etc/rc.local"

### get_url模块

> get_url 模块(实现在远程主机下载指定 URL 到本地，支持 sha256sum 文件校验)

    ansible all -m get_url -a "url=http://www.baidu.com dest=/tmp/index.html mode=0740 force=yes"

### yum 模块

> yum 模块 软件包管理

    ansible node -m yum -a "name=vim state=latest"

### cron 模块

> 注意 name 不要相同否则会被覆盖掉

    ansible node -m cron -a "name='check dirs' minute='*/1' job='ls -alh > /tmp/ansible'"    # 每分钟执行一次
    ansible node -m cron -a "name='check dirs' hour='*/1' job='ls -alh > /tmp/ansible'"      # 每小时执行一次
    ansible node -m cron -a "name='check date' minute='*/1' job='/usr/bin/date +\'\%F \%T\' > /tmp/ansible'"

### mount 模块

> mount 模块(远程主机分区挂载 此挂载是直接写入 /etc/fstab)

    ansible node -m mount -a "name=/mnt src=/dev/sd0 fstype=ext4 opts=ro state=present"
    ansible node -m mount -a "name=/mnt src=/dev/cdrom fstype=iso9660 opts=ro state=present"    # 挂载光盘

### service 模块

> service 模块(远程主机系统服务管理)

    ansible node -m service -a "name=firewalld state=started"      # 启用 node 组防火墙
    ansible node -m service -a "name=firewalld state=stopped"      # 关闭 node 组防火墙
    ansible node -m service -a "name=firewalld state=restarted"    # 重启 node 组防火墙
    ansible node -m service -a "name=firewalld state=reloaded"     # 重载 node 组防火墙

### user 模块
 
    ansible node -m user -a "name=wang comment='user wang'"        # 在 node 组服务器创建用户 wang
    ansible node -m user -a "name=wang state=absent remove=yes"    # 在 node 组服务器删除用户 wang

### 示例

> 新增用户并更改密码

    ansible node -m user -a "name=feng comment='user feng'"   # OR ansible node -a 'useradd feng2'
    echo 'echo redhat | passwd --stdin feng' > pass.sh
    ansible node -m script -a 'pass.sh'