---
title: 构建自己的 Docker镜像
tags:
  - docker
  - build
copyright: true
categories:
  - docker
abbrlink: 871a
date: 2018-05-17 18:02:44
---

### Docker 指令

---

#### COPY 复制文件

- COPY <源路径>... <目标路径>
- COPY ["<源路径1>",... "<目标路径>"]


    COPY package.json /usr/src/app/
    COPY hom* /mydir/
    COPY hom?.txt /mydir/
    
> <目标路径> 可以是容器内的绝对路径，也可以是相对于工作目录的相对路径（工作目录可以用 WORKDIR 指令来指定）。目标路径不需要事先创建，如果目录不存在会在复制文件前先行创建缺失目录。使用 COPY 指令，源文件的各种元数据都会保留。比如读、写、执行权限、文件变更时间等。   

---

#### ADD 更高级的复制

- ADD 指令和 COPY 的格式和性质基本一致。但是在 COPY 基础上增加了一些功能。如果<源路径>是 url Docker引擎将自动下载，并放到<目标路径>文件权限 600 如果 <源路径> 为一个 tar 压缩文件的话，压缩格式为 gzip, bzip2 以及 xz 的情况下，ADD 指令将会自动解压缩这个压缩文件到 <目标路径> 去


    ADD ubuntu-xenial-core-cloudimg-amd64-root.tar.gz /

---

#### CMD 容器启动命令

- shell 格式：


    CMD <命令>

- exec 格式：


    CMD ["可执行文件", "参数1", "参数2"...]

- 参数列表格式：CMD ["参数1", "参数2"...]。在指定了 ENTRYPOINT 指令后，用 CMD 指定具体的参数。

- Docker 不是虚拟机，容器中的应用都应该以前台执行，而不是像虚拟机、物理机里面那样，用 upstart/systemd 去启动后台服务，容器内没有后台服务的概念。例如容器启动后开启 nginx，直接执行 nginx 可执行文件，并且要求以前台形式运行。比如：


    CMD ["nginx", "-g", "daemon off;"]

---

#### ENTRYPOINT 入口点

- ENTRYPOINT 的目的和 CMD 一样，都是在指定容器启动程序及参数。ENTRYPOINT 在运行时也可以替代，不过比 CMD 要略显繁琐，需要通过 docker run 的参数 --entrypoint 来指定。当指定了 ENTRYPOINT 后，CMD 的含义就发生了改变，不再是直接的运行其命令，而是将 CMD 的内容作为参数传给 ENTRYPOINT 指令，换句话说实际执行时，将变为：


    <ENTRYPOINT> "<CMD>"

##### 场景一：让镜像变成像命令一样使用

- 假设我们需要一个得知自己当前公网 IP 的镜像，那么可以先用 CMD 来实现：


    FROM centos
    RUN yum -y update \
        && yum install -y curl \
        && yum clean all \
        && rm -rf /var/cache/yum
    CMD [ "curl", "-s", "http://ip.cn" ]

- 假如我们使用 docker build -t myip . 来构建镜像的话，如果我们需要查询当前公网 IP，只需要执行：


    docker run myip
    当前 IP：221.234.131.117 来自：湖北省武汉市 电信

- 如果我们希望加参数呢？比如从上面的 CMD 中可以看到实质的命令是 curl，那么如果我们希望显示 HTTP 头信息，就需要加上 -i 参数。那么我们在 Dockerfile 文件中用 ENTRYPOINT 来实现这个镜像：


    FROM centos
    RUN yum -y update \
        && yum install -y curl \
        && yum clean all \
        && rm -rf /var/cache/yum
    ENTRYPOINT [ "curl", "-s", "http://ip.cn" ]

- 这次我们再来尝试直接使用 docker run myip -i：


    docker run myip -i
    HTTP/1.1 200 OK
    Date: Wed, 16 May 2018 09:37:17 GMT
    Content-Type: text/html; charset=UTF-8
    Transfer-Encoding: chunked
    Connection: keep-alive
    Set-Cookie: __cfduid=d496a06be8fc34f0b942bd80c653a18081526463437; expires=Thu, 16-May-19 09:37:17 GMT; path=/; domain=.ip.cn; HttpOnly
    Server: cloudflare
    CF-RAY: 41bcdd62e5a36d6c-SJC

    当前 IP：221.234.131.117 来自：湖北省武汉市 电信


##### 场景二：应用运行前的准备工作

- 启动容器就是启动主进程，但有些时候，启动主进程前，需要一些准备工作。

- 比如 mysql 类的数据库，可能需要一些数据库配置、初始化的工作，这些工作要在最终的 mysql 服务器运行之前解决。

- 此外，可能希望避免使用 root 用户去启动服务，从而提高安全性，而在启动服务前还需要以 root 身份执行一些必要的准备工作，最后切换到服务用户身份启动服务。或者除了服务外，其它命令依旧可以使用 root 身份执行，方便调试等。

- 这些准备工作是和容器 CMD 无关的，无论 CMD 为什么，都需要事先进行一个预处理的工作。这种情况下，可以写一个脚本，然后放入 ENTRYPOINT 中去执行，而这个脚本会将接到的参数（也就是 <CMD>）作为命令，在脚本最后执行。比如官方镜像 redis 中就是这么做的：


    FROM alpine:3.4
    ...
    RUN addgroup -S redis && adduser -S -G redis redis
    ...
    ENTRYPOINT ["docker-entrypoint.sh"]

    EXPOSE 6379
    CMD [ "redis-server" ]

- 可以看到其中为了 redis 服务创建了 redis 用户，并在最后指定了 ENTRYPOINT 为 docker-entrypoint.sh 脚本。


    #!/bin/sh
    ...
    # allow the container to be started with `--user`
    if [ "$1" = 'redis-server' -a "$(id -u)" = '0' ]; then
        chown -R redis .
        exec su-exec redis "$0" "$@"
    fi

    exec "$@"

- 该脚本的内容就是根据 CMD 的内容来判断，如果是 redis-server 的话，则切换到 redis 用户身份启动服务器，否则依旧使用 root 身份执行。比如：


    docker run -it redis id
    uid=0(root) gid=0(root) groups=0(root)

---

#### ENV 设置环境变量

- 格式有两种：


    ENV <key> <value>
    ENV <key1>=<value1> <key2>=<value2>...
    ENV <key1>=<value1> \
        <key2>=<value2> \
        ...
    
- 这个例子中演示了如何换行，以及对含有空格的值用双引号括起来的办法，这和 Shell 下的行为是一致的。

- 定义了环境变量，那么在后续的指令中，就可以使用这个环境变量。比如在官方 node 镜像 Dockerfile 中，就有类似这样的代码：


    ENV NODE_VERSION 7.2.0

    RUN curl -SLO "https://nodejs.org/dist/v$NODE_VERSION/node-v$NODE_VERSION-linux-x64.tar.xz" \
       && curl -SLO "https://nodejs.org/dist/v$NODE_VERSION/SHASUMS256.txt.asc" \
       && gpg --batch --decrypt --output SHASUMS256.txt SHASUMS256.txt.asc \
       && grep " node-v$NODE_VERSION-linux-x64.tar.xz\$" SHASUMS256.txt | sha256sum -c - \
       && tar -xJf "node-v$NODE_VERSION-linux-x64.tar.xz" -C /usr/local --strip-components=1 \
       && rm "node-v$NODE_VERSION-linux-x64.tar.xz" SHASUMS256.txt.asc SHASUMS256.txt \
       && ln -s /usr/local/bin/node /usr/local/bin/nodejs

- 在这里先定义了环境变量 NODE_VERSION，其后的 RUN 这层里，多次使用 $NODE_VERSION 来进行操作定制。可以看到，将来升级镜像构建版本的时候，只需要更新 7.2.0 即可，Dockerfile 构建维护变得更轻松了。

- 下列指令可以支持环境变量展开： 


    ADD、COPY、ENV、EXPOSE、LABEL、USER、WORKDIR、VOLUME、STOPSIGNAL、ONBUILD。

> 通过环境变量，我们可以让一份 Dockerfile 制作更多的镜像，只需使用不同的环境变量即可。

---

#### ARG 构建参数

- 格式：


    ARG <参数名>[=<默认值>]

- 构建参数和 ENV 的效果一样，都是设置环境变量。所不同的是，ARG 所设置的构建环境的环境变量，在将来容器运行时是不会存在这些环境变量的。但是不要因此就使用 ARG 保存密码之类的信息，因为 docker history 还是可以看到所有值的。

- Dockerfile 中的 ARG 指令是定义参数名称，以及定义其默认值。该默认值可以在构建命令 docker build 中用 --build-arg <参数名>=<值> 来覆盖。

- 这对于使用 CI 系统，用同样的构建流程构建不同的 Dockerfile 的时候比较有帮助，避免构建命令必须根据每个 Dockerfile 的内容修改。

---

#### VOLUME 定义匿名卷

- 格式为：


    VOLUME ["<路径1>", "<路径2>"...]
    VOLUME <路径>


- 容器运行时应该尽量保持容器存储层不发生写操作，对于数据库类需要保存动态数据的应用，其数据库文件应该保存于卷(volume)中，为了防止运行时用户忘记将动态文件所保存目录挂载为卷，在 Dockerfile 中，我们可以事先指定某些目录挂载为匿名卷，这样在运行时如果用户不指定挂载，其应用也可以正常运行，不会向容器存储层写入大量数据。


    VOLUME /data
    
    
- 这里的 /data 目录就会在运行时自动挂载为匿名卷，任何向 /data 中写入的信息都不会记录进容器存储层，从而保证了容器存储层的无状态化。当然，运行时可以覆盖这个挂载设置。比如：


    docker run -d -v mydata:/data xxxx

- 在这行命令中，就使用了 mydata 这个命名卷挂载到了 /data 这个位置，替代了 Dockerfile 中定义的匿名卷的挂载配置。

---

#### EXPOSE 声明端口

- 格式为 :


    EXPOSE <端口1> [<端口2>...]。

- EXPOSE 指令是声明运行时容器提供服务端口，这只是一个声明，在运行时并不会因为这个声明应用就会开启这个端口的服务。在 Dockerfile 中写入这样的声明有两个好处，一个是帮助镜像使用者理解这个镜像服务的守护端口，以方便配置映射；另一个用处则是在运行时使用随机端口映射时，也就是 docker run -P 时，会自动随机映射 EXPOSE 的端口。

- 此外，在早期 Docker 版本中还有一个特殊的用处。以前所有容器都运行于默认桥接网络中，因此所有容器互相之间都可以直接访问，这样存在一定的安全性问题。于是有了一个 Docker 引擎参数 --icc=false，当指定该参数后，容器间将默认无法互访，除非互相间使用了 --links 参数的容器才可以互通，并且只有镜像中 EXPOSE 所声明的端口才可以被访问。这个 --icc=false 的用法，在引入了 docker network 后已经基本不用了，通过自定义网络可以很轻松的实现容器间的互联与隔离。

- 要将 EXPOSE 和在运行时使用 -p <宿主端口>:<容器端口> 区分开来。-p，是映射宿主端口和容器端口，换句话说，就是将容器的对应端口服务公开给外界访问，而 EXPOSE 仅仅是声明容器打算使用什么端口而已，并不会自动在宿主进行端口映射。

---

#### WORKDIR 指定工作目录

- 格式为 

    WORKDIR <工作目录路径>。

- 使用 WORKDIR 指令可以来指定工作目录（或者称为当前目录），以后各层的当前目录就被改为指定的目录，如该目录不存在，WORKDIR 会帮你建立目录。

- 之前提到一些初学者常犯的错误是把 Dockerfile 等同于 Shell 脚本来书写，这种错误的理解还可能会导致出现下面这样的错误：


    RUN cd /app
    RUN echo "hello" > world.txt

- 如果将这个 Dockerfile 进行构建镜像运行后，会发现找不到 /app/world.txt 文件，或者其内容不是 hello。原因其实很简单，在 Shell 中，连续两行是同一个进程执行环境，因此前一个命令修改的内存状态，会直接影响后一个命令；而在 Dockerfile 中，这两行 RUN 命令的执行环境根本不同，是两个完全不同的容器。这就是对 Dockerfile 构建分层存储的概念不了解所导致的错误。

- 之前说过每一个 RUN 都是启动一个容器、执行命令、然后提交存储层文件变更。第一层 RUN cd /app 的执行仅仅是当前进程的工作目录变更，一个内存上的变化而已，其结果不会造成任何文件变更。而到第二层的时候，启动的是一个全新的容器，跟第一层的容器更完全没关系，自然不可能继承前一层构建过程中的内存变化。

- 因此如果需要改变以后各层的工作目录的位置，那么应该使用 WORKDIR 指令。

---

#### USER 指定当前用户

- 格式：


    USER <用户名>

- USER 指令和 WORKDIR 相似，都是改变环境状态并影响以后的层。WORKDIR 是改变工作目录，USER 则是改变之后层的执行 RUN, CMD 以及 ENTRYPOINT 这类命令的身份。

- 当然，和 WORKDIR 一样，USER 只是帮助你切换到指定用户而已，这个用户必须是事先建立好的，否则无法切换。


    RUN groupadd -r redis && useradd -r -g redis redis
    USER redis
    RUN [ "redis-server" ]

- 如果以 root 执行的脚本，在执行期间希望改变身份，比如希望以某个已经建立好的用户来运行某个服务进程，不要使用 su 或者 sudo，这些都需要比较麻烦的配置，而且在 TTY 缺失的环境下经常出错。建议使用 gosu。


    # 建立 redis 用户，并使用 gosu 换另一个用户执行命令
    RUN groupadd -r redis && useradd -r -g redis redis
    # 下载 gosu
    RUN wget -O /usr/local/bin/gosu "https://github.com/tianon/gosu/releases/download/1.7/gosu-amd64" \
        && chmod +x /usr/local/bin/gosu \
        && gosu nobody true
    # 设置 CMD，并以另外的用户执行
    CMD [ "exec", "gosu", "redis", "redis-server" ]
 
---

### Nginx

#### 创建 Dockerfile

---

##### 创建目录

```sh
mkdir -p ~/nginx/www ~/nginx/logs ~/nginx/conf

# www目录将映射为nginx容器配置的虚拟目录
# logs目录将映射为nginx容器的日志目录
# conf目录里的配置文件将映射为nginx容器的配置文件
```

---

##### 创建 Dockerfile

```sh
cd ~/nginx
cat <<! >~/nginx/Dockerfile
FROM centos

MAINTAINER Jackie "www.jackieathome.net"
ENV NGINX_VERSION 1.10.3
ENV OPENSSL_VERSION 1.0.2k
ENV PCRE_VERSION 8.40
ENV ZLIB_VERSION 1.2.11
ENV BUILD_ROOT /usr/local/src/nginx

# 为了减小最终生成的镜像占用的空间，这里没有执行yum update命令，可能不是好的实践
# 为了加快构建速度，这里使用了163的安装源
#RUN yum -y update \

RUN yum -y install curl \
        && mv /etc/yum.repos.d/CentOS-Base.repo /etc/yum.repos.d/CentOS-Base.repo.backup \
        && curl http://mirrors.163.com/.help/CentOS7-Base-163.repo -o /etc/yum.repos.d/CentOS7-Base-163.repo \ 
        && yum clean all \
        && yum makecache \
        && yum -y install gcc gcc-c++ make perl zip unzip \
        && mkdir -p $BUILD_ROOT \
        && cd $BUILD_ROOT \
        && curl https://ftp.pcre.org/pub/pcre/pcre-$PCRE_VERSION.zip -o $BUILD_ROOT/pcre-$PCRE_VERSION.zip \
        && curl https://www.openssl.org/source/openssl-$OPENSSL_VERSION.tar.gz -o $BUILD_ROOT/openssl-$OPENSSL_VERSION.tar.gz \
        && curl http://www.zlib.net/zlib-$ZLIB_VERSION.tar.gz -o $BUILD_ROOT/zlib-$ZLIB_VERSION.tar.gz \
        && curl https://nginx.org/download/nginx-$NGINX_VERSION.tar.gz -o $BUILD_ROOT/nginx-$NGINX_VERSION.tar.gz \
        && tar vxzf nginx-$NGINX_VERSION.tar.gz \
        && unzip pcre-$PCRE_VERSION.zip \
        && tar vxfz zlib-$ZLIB_VERSION.tar.gz \
        && tar vxfz openssl-$OPENSSL_VERSION.tar.gz \
        && cd nginx-$NGINX_VERSION \
        && BUILD_CONFIG="\
            --prefix=/etc/nginx \
            --sbin-path=/usr/sbin/nginx \
            --conf-path=/etc/nginx/nginx.conf \
            --error-log-path=/var/log/nginx/error.log \
            --http-log-path=/var/log/nginx/access.log \
            --pid-path=/var/run/nginx.pid \
            --lock-path=/var/run/nginx.lock \
            --http-client-body-temp-path=/var/cache/nginx/client_temp \
            --http-proxy-temp-path=/var/cache/nginx/proxy_temp \
            --http-fastcgi-temp-path=/var/cache/nginx/fastcgi_temp \
            --http-uwsgi-temp-path=/var/cache/nginx/uwsgi_temp \
            --http-scgi-temp-path=/var/cache/nginx/scgi_temp \
            --with-openssl=$BUILD_ROOT/openssl-$OPENSSL_VERSION \
            --with-pcre=$BUILD_ROOT/pcre-$PCRE_VERSION \
            --with-zlib=$BUILD_ROOT/zlib-$ZLIB_VERSION \
            --with-http_ssl_module \
            --with-http_v2_module \ 
            --with-threads \
            " \
        && mkdir -p /var/cache/nginx \
        && ./configure $BUILD_CONFIG \
        && make && make install \
        && rm -rf $BUILD_ROOT \
        && yum -y remove gcc gcc-c++ make perl zip unzip \
        && yum clean all

EXPOSE 80 443

VOLUME ["/etc/nginx/nginx.conf", "/etc/nginx/html/"]

CMD ["nginx", "-g", "daemon off;"]
!
```


##### 创建镜像

```sh
docker build -t my-nginx .
```
