---
title: Nginx负载均衡与反向代理配置
tags:
  - nginx负载均衡
  - nginx反向代理
  - proxy
copyright: true
categories:
  - nginx
abbrlink: nginx-proxy
date: 2018-09-24 12:30:38
---

[TOC]

## 环境准备

    系统：centos7
    软件版本：nginx-1.12.2
    配置文件: /etc/nginx/nginx.conf

    说明：服务器3台分别安装nginx, 第三台配置负载均衡
    lab-01    IP: 10.0.0.11     nginx_port 8080
    lab-02    IP: 10.0.0.12     nginx_port 8080
    lab-03    IP: 10.0.0.13     nginx_port 8080 80


## 负载均衡

### lab-01

    # 主机名
    hostnamectl set-hostname lab-01

    # 关闭防火墙
    systemctl stop firewalld
    systemctl dieable firewalld
    systemctl status firewalld

    # 关闭selinux
    getenforce 0
    sed -i 's/^SELINUX=enforcing/SELINUX=disabled/' /etc/selinux/config

    # 配置YUM源
    rm -f /etc/yum.repos.d/*
    curl -so /etc/yum.repos.d/epel-7.repo http://mirrors.aliyun.com/repo/epel-7.repo
    curl -so /etc/yum.repos.d/Centos-7.repo http://mirrors.aliyun.com/repo/Centos-7.repo
    sed -i '/aliyuncs.com/d' /etc/yum.repos.d/Centos-7.repo /etc/yum.repos.d/epel-7.repo

    # 安装 nginx
    yum install -y nginx

    # 修改监听端口为8080
     sed -i '30,40 s/listen       80/listen       8080/' /etc/nginx/nginx.conf

    # 修改日志配置,在日志文件中写入访问者真实IP,而不是负载均衡服务器IP
    sed -i 's/remote_addr/http_x_real_ip/' /etc/nginx/nginx.conf

    # 创建主页内容为当前主机名,启动服务
    echo `hostname` >/usr/share/nginx/html/index.html
    systemctl start nginx

### lab-02

    # 主机名
    hostnamectl set-hostname lab-02

    # 关闭防火墙
    systemctl stop firewalld
    systemctl dieable firewalld
    systemctl status firewalld

    # 关闭selinux
    getenforce 0
    sed -i 's/^SELINUX=enforcing/SELINUX=disabled/' /etc/selinux/config

    # 配置YUM源
    rm -f /etc/yum.repos.d/*
    curl -so /etc/yum.repos.d/epel-7.repo http://mirrors.aliyun.com/repo/epel-7.repo
    curl -so /etc/yum.repos.d/Centos-7.repo http://mirrors.aliyun.com/repo/Centos-7.repo
    sed -i '/aliyuncs.com/d' /etc/yum.repos.d/Centos-7.repo /etc/yum.repos.d/epel-7.repo

    # 安装 nginx
    yum install -y nginx

    # 修改监听端口为8080
     sed -i '30,40 s/listen       80/listen       8080/' /etc/nginx/nginx.conf

    # 修改日志配置,在日志文件中写入访问者真实IP,而不是负载均衡服务器IP
    sed -i 's/remote_addr/http_x_real_ip/' /etc/nginx/nginx.conf

    # 创建主页内容为当前主机名,启动服务
    echo `hostname` >/usr/share/nginx/html/index.html
    systemctl start nginx

### lab-03

    # 主机名
    hostnamectl set-hostname lab-03

    # 关闭防火墙
    systemctl stop firewalld
    systemctl dieable firewalld
    systemctl status firewalld

    # 关闭selinux
    getenforce 0
    sed -i 's/^SELINUX=enforcing/SELINUX=disabled/' /etc/selinux/config

    # 配置YUM源
    rm -f /etc/yum.repos.d/*
    curl -so /etc/yum.repos.d/epel-7.repo http://mirrors.aliyun.com/repo/epel-7.repo
    curl -so /etc/yum.repos.d/Centos-7.repo http://mirrors.aliyun.com/repo/Centos-7.repo
    sed -i '/aliyuncs.com/d' /etc/yum.repos.d/Centos-7.repo /etc/yum.repos.d/epel-7.repo

    # 安装 nginx
    yum install -y nginx

    # 修改监听端口为8080
     sed -i '30,40 s/listen       80/listen       8080/' /etc/nginx/nginx.conf

    # 修改日志配置,在日志文件中写入访问者真实IP,而不是负载均衡服务器IP
    sed -i 's/remote_addr/http_x_real_ip/' /etc/nginx/nginx.conf

    # 创建主页内容为当前主机名,启动服务
    echo `hostname` >/usr/share/nginx/html/index.html
    systemctl start nginx

    # 创建负载均衡配置
    cat <<'EOF'  >/etc/nginx/conf.d/ha.conf
    upstream my-ha {
    #    ip_hash;    # 对请求的ip进行hash运算并将结果映射到其中一个app，它能确保一个确定的请求ip会被映射到一个确定的服务对session 有要求的可以开启此选项
            server 10.0.0.11:8080;
            server 10.0.0.12:8080;
            server 10.0.0.13:8080;
    #        server 10.0.0.11:8080 down;       # 表示单前的server暂时不参与负载
    #        server 10.0.0.12:8080 weight=2;   # 默认为1.weight越大，负载的权重就越大
    #        server 10.0.0.13:8080 backup;     # 其它所有的非backup机器down或者忙的时候，请求backup机器。所以这台机器压力会最轻
        }

    server {
        listen    80;
        # server_name www.test.com  # 配置成实际的域名，每个虚拟主机的配置文件域名都相同
        # Load configuration files for the default server block.
        include /etc/nginx/default.d/*.conf;

        location / {
            proxy_redirect off;
            proxy_set_header Host $host;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_pass http://my-ha;
        }

        error_page 404 /404.html;
            location = /40x.html {
        }

        error_page 500 502 503 504 /50x.html;
            location = /50x.html {
        }
    }
    EOF

    # 重启 nginx 服务
    systemctl restart nginx

    # 打开浏览器访问
    http://10.0.0.13

## 反向代理

### 安装 Nginx

    # 主机名
    hostnamectl set-hostname lab-01

    # 关闭防火墙
    systemctl stop firewalld
    systemctl dieable firewalld
    systemctl status firewalld

    # 关闭selinux
    getenforce 0
    sed -i 's/^SELINUX=enforcing/SELINUX=disabled/' /etc/selinux/config

    # 配置YUM源
    rm -f /etc/yum.repos.d/*
    curl -so /etc/yum.repos.d/epel-7.repo http://mirrors.aliyun.com/repo/epel-7.repo
    curl -so /etc/yum.repos.d/Centos-7.repo http://mirrors.aliyun.com/repo/Centos-7.repo
    sed -i '/aliyuncs.com/d' /etc/yum.repos.d/Centos-7.repo /etc/yum.repos.d/epel-7.repo

    # 安装 nginx
    yum install -y nginx

    # 修改监听端口为8080
     sed -i '30,40 s/listen       80/listen       8080/' /etc/nginx/nginx.conf

### 配置代理[1]

```
cat <<'EOF'   >/etc/nginx/conf.d/proxy.conf
server {
    listen 8083;
    server_name doc.onlycloud.xin;
    location / {
        proxy_redirect off;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_pass http://192.168.2.3;
    }
    access_log /var/log/nginx/doc.onlycloud.xin.log;
}

server {
    listen 8087;
    server_name filerun.onlycloud.xin;
    location / {
        proxy_redirect off;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_pass http://192.168.2.7;
    }
    access_log /var/log/nginx/filerun.onlycloud.xin.log;
}
EOF
```

### 创建日志文件

    touch /var/log/nginx/doc.onlycloud.xin.log
    touch /var/log/nginx/filerun.onlycloud.xin.log

### 重启服务

    systemctl restart nginx

### 验证

    # hosts 添加域名对应的解析访问
    http://doc.onlycloud.xin:8083  或者 http://服务器IP:8083
    http://filerun.onlycloud.xin:8087  或者 http://服务器IP:8087

### 配置代理[2]

```
cat <<EOF  >/etc/nginx/conf.d/mirrors.conf
server {
    listen 8090;
    #listen [::]:80;
    autoindex on;
    autoindex_exact_size off;
    autoindex_localtime on;
    server_name mirrors.test.com;
    index index.html index.htm index.php default.html default.htm default.php;
    root  /home/wwwroot/html;

    location /ubuntu/ {
        proxy_pass http://mirrors.aliyun.com/ubuntu/;
    }

    location /centos/ {
        proxy_pass http://mirrors.aliyun.com/centos/;
    }

    location /epel/ {
        proxy_pass http://mirrors.aliyun.com/epel/;
    }
}
EOF
```

### 创建目录

    mkdir -p /home/wwwroot/html/{centos,ubuntu,epel}
    chown nginx:nginx /home/wwwroot/html/ -R

### 重启服务

    systemctl restart nginx

### 验证

    http://服务器IP:8090
