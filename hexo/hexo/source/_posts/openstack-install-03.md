title: OpenStack集群安装-03_MariaDB-Galera-Cluster集群
tags:
  - openstack
  - Galera
  - mariadb
copyright: true
categories:
  - openstack
abbrlink: openstack-03
date: 2018-07-20 05:42:00
---
### 安装 OpenStack 客户端,工具

    for HOST in controller{1..3}; do
        echo "--------------- $HOST ---------------"
        ssh -T $HOST <<'EOF'
        # 安装 OpenStack 客户端,工具
        yum -y install python-openstackclient openstack-selinux openstack-utils
    EOF
    done

### 安装配置 MariaDB Galera

> controller1,2,3 安装软件,创建配置文件,启动服务，初始化数据库,授权

    # 设置 Mariadb root 密码(密码为 redhat 写入家目录供后面读取)
    echo "DBPass='redhat'" >>~/PASS
    source ~/PASS
    
    # 安装配置数据库
    conf='/etc/my.cnf.d/openstack.cnf'
    for HOST in controller{1..3}; do
        echo "--------------- $HOST ---------------"
        ssh -T $HOST <<EOF
        # 安装Mariadb Galera 集群, 自动交互工具expect
        yum -y install mariadb mariadb-server mariadb-galera-server expect

        # 创建 openstack 数据库配置文件
        echo "[mysqld]" >$conf
        echo "bind-address = $HOST" >>$conf
        echo "default-storage-engine = innodb" >>$conf
        echo "innodb_file_per_table" >>$conf
        echo "max_connections = 4096" >>$conf
        echo "collation-server = utf8_general_ci" >>$conf
        echo "character-set-server = utf8" >>$conf

        # 启动数据库服务,禁止跟随系统启动
        systemctl start mariadb
        systemctl disable mariadb

        # 初始化数据库
        /usr/bin/expect << EOF
        set timeout 30
        spawn mysql_secure_installation
        expect {
            "enter for none" { send "\r"; exp_continue}
            "Y/n" { send "Y\r" ; exp_continue}
            "password:" { send "$DBPass\r"; exp_continue}
            "new password:" { send "$DBPass\r"; exp_continue}
            "Y/n" { send "Y\r" ; exp_continue}
            eof { exit }
        }
        
        # 验证
        mysql -u root -p$DBPass -e "show databases;"
        if [ $? = 0 ]; then
            echo -e "\033[32m \n HOST: $HOST  MariaDB  初始化成功 \033[0m"
        else
            echo -e "\033[31m \n HOST: $HOST  MariaDB  初始化失败 \033[0m"
            exit
        fi
        
        # 配置数据库root 远程访问权限(Galera 集群管理用)
        mysql -u root -p$DBPass -e "
        grant all privileges on *.* to 'root'@'%' identified by '$DBPass' with grant option; 
        flush privileges;
        select user,host,password from mysql.user;"
    EOF
    done
    
#### Galera配置文件

    conf='/etc/my.cnf.d/galera.cnf'
    for HOST in controller{1..3}; do
        ssh -T $HOST <<EOF
        # 备份默认配置文件
        [ -f $conf.bak ] || cp $conf{,.bak}

        # 过滤配置文件中的无效行
        egrep -v "#|^$" $conf.bak >$conf
        sed -i 's/wsrep_on=1/wsrep_on=ON/' $conf
        
        # 配置数据库凭证
        sed -i 's/wsrep_sst_auth=.*/wsrep_sst_auth=root:'$DBPass'/' $conf
        echo 'wsrep_cluster_address="gcomm://controller1,controller2,controller3"' >>$conf
        echo "wsrep_node_name= $HOST" >>$conf
        echo "wsrep_node_address= $HOST" >>$conf
    EOF
    done

#### 集群配置

> 初始化集群

    # 关闭 controller1 MariaDB 服务
    systemctl stop mariadb

    # 在 controller1 初始化集群
    galera_new_cluster
    sleep 2

    # 重启 controller2 controller3 MariaDB 服务
    for HOST in controller2 controller3; do
    ssh -T root@$HOST <<EOF
        systemctl daemon-reload
        systemctl restart mariadb
    EOF
    done
    sleep 3

    # 启动 controller1 MariaDB 服务
    systemctl start mariadb
---

> 查看集群节点

    mysql -u root -p$DBPass -e "show status like 'wsrep_cluster_size';"
    mysql -u root -p$DBPass -e "show status like 'wsrep_incoming_addresses';"
---

> 集群设置

    # 创建用于监控的mariadb 用户haproxy (haproxy代理，监控使用)
    mysql -u root -p$DBPass -e "create user 'haproxy'@'%';flush privileges;"

    # controller1 作为服务控制节点
    cat <<'EOF' >>/etc/rc.local
    
    # 作为第一个节点启动SQL
    /usr/bin/galera_new_cluster
    sleep 5
    ssh controller2 "systemctl start mariadb"
    ssh controller3 "systemctl start mariadb"
    sleep 3
    systemctl restart mariadb
    EOF

    # 添加执行权限(系统启动后执行)
    chmod +x /etc/rc.d/rc.local

### Haproxy配置

    cat <<'EOF' >>/etc/haproxy/haproxy.cfg
    ########mariadb_cluster############
    listen mariadb_cluster
      mode tcp     
      bind controller:3306     
      balance leastconn     
      option mysql-check user haproxy     
      server controller1 controller1:3306 weight 1 check inter 2000 rise 2 fall 5     
      server controller2 controller2:3306 weight 1 check inter 2000 rise 2 fall 5
      server controller3 controller3:3306 weight 1 check inter 2000 rise 2 fall 5
    EOF

    # 同步 haproxy 配置文件
    scp /etc/haproxy/haproxy.cfg controller2:/etc/haproxy/haproxy.cfg
    scp /etc/haproxy/haproxy.cfg controller3:/etc/haproxy/haproxy.cfg

    # 重启服务
    systemctl restart haproxy
    ssh controller2 "systemctl restart haproxy"
    ssh controller3 "systemctl restart haproxy"

### 验证

> 命令行执行查看数据库

    mysql -h controller -u root -p$DBPass -e "show databases;"
    mysql -h controller1 -u root -p$DBPass -e "show databases;"
    mysql -h controller2 -u root -p$DBPass -e "show databases;"
    mysql -h controller3 -u root -p$DBPass -e "show databases;"

    # 浏览器打开 http://192.168.0.11:1080/admin 查看 mariadb_cluster 状态

### 使用脚本

    site='http://home.onlycloud.xin'
    wget $site/code/openstack-03_mariadb-galera-cluster.sh -O openstack-mariadb-galera.sh
    sh openstack-mariadb-galera.sh
