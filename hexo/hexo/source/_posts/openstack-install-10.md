title: OpenStack集群安装-10_cinder存储安装
tags:
  - openstack
  - cinder
copyright: true
categories:
  - openstack
abbrlink: openstack-10
date: 2018-07-29 17:36:00
---
    # Cinder 块存储
    # 控制节点安装配置 cinder-api, cinder-scheduler 服务
    # 存储节点安装配置 cinder-volume 服务

## 控制节点安装配置

### 环境准备

#### 创建数据库

> 创建 cinder 数据库,用户,授权

    source ~/PASS               # 读取数据库密码
    mysql -u root -p$DBPass -e "
    create database cinder;
    grant all privileges on cinder.* to 'cinder'@'localhost' identified by 'cinder';
    grant all privileges on cinder.* to 'cinder'@'%' identified by 'cinder';
    flush privileges;
    select user,host from mysql.user;
    show databases;"


#### 创建,角色,服务,API

> 创建 cinder 角色,授权

    source ~/admin-openstack.sh              # 加载凭证
    openstack user create --domain default --password=cinder cinder
    openstack role add --project service --user cinder admin
---

> 创建 glance 服务

    openstack service create --name cinderv2   --description "OpenStack Block Storage" volumev2
    openstack service create --name cinderv3   --description "OpenStack Block Storage" volumev3
    openstack service list        # 查看服务
---

> 注册 API

    openstack endpoint create --region RegionOne volumev2 public http://controller:8776/v2/%\(project_id\)s
    openstack endpoint create --region RegionOne volumev2 internal http://controller:8776/v2/%\(project_id\)s
    openstack endpoint create --region RegionOne volumev2 admin http://controller:8776/v2/%\(project_id\)s
    openstack endpoint create --region RegionOne volumev3 public http://controller:8776/v3/%\(project_id\)s
    openstack endpoint create --region RegionOne volumev3 internal http://controller:8776/v3/%\(project_id\)s
    openstack endpoint create --region RegionOne volumev3 admin http://controller:8776/v3/%\(project_id\)s
    openstack endpoint list        # 查看注册的 API
---

### 安装 Cinder

    for HOST in controller{1..3}; do
        echo "------------ $HOST ------------"
        ssh -T $HOST <<'EOF'
        # 安装 Cinder NFS
        yum install -y openstack-cinder nfs-utils
        
        # 备份默认配置文件
        [ -f /etc/cinder/cinder.conf.bak ] || cp /etc/cinder/cinder.conf{,.bak}
        
        # Nova 添加 cinder 配置
        if [ -z "$(grep 'Cinder' /etc/nova/nova.conf)" ];then
            echo -e '\n# Cinder' >>/etc/nova/nova.conf
            echo '[cinder]' >>/etc/nova/nova.conf
            echo 'os_region_name = RegionOne' >>/etc/nova/nova.conf
        fi
    EOF
    done

### 配置 Cinder

> Cinder 配置文件

    # 备份默认配置文件
    [ -f /etc/cinder/cinder.conf.bak ] || cp /etc/cinder/cinder.conf{,.bak}
    
    # 创建 Cinder 配置文件
    cat <<'EOF' >/etc/cinder/cinder.conf
    [DEFAULT]
    osapi_volume_listen = controller1
    osapi_volume_listen_port = 8776
    auth_strategy = keystone
    log_dir = /var/log/cinder
    state_path = /var/lib/cinder
    glance_api_servers = http://controller:9292
    transport_url = rabbit://openstack:openstack@controller

    [database]
    connection = mysql+pymysql://cinder:cinder@controller/cinder

    [keystone_authtoken]
    auth_uri = http://controller:5000
    auth_url = http://controller:35357
    memcached_servers = controller1:11211,controller2:11211,controller3:11211
    auth_type = password
    project_domain_name = default
    user_domain_name = default
    project_name = service
    username = cinder
    password = cinder

    [oslo_concurrency]
    lock_path = /var/lib/cinder/tmp
    EOF
---

> 同步配置到其他节点

    # 同步 cinder 配置文件, 修改其他节点监听主机
    rsync -avzP -e 'ssh -p 22' /etc/cinder/cinder.conf controller2:/etc/cinder/
    rsync -avzP -e 'ssh -p 22' /etc/cinder/cinder.conf controller3:/etc/cinder/
    ssh controller2 "sed -i '1,8s/controller1/controller2/' /etc/cinder/cinder.conf"
    ssh controller3 "sed -i '1,8s/controller1/controller3/' /etc/cinder/cinder.conf"
---

> 初始化数据

    su -s /bin/sh -c "cinder-manage db sync" cinder
---

> 检验 cinder 数据库

    mysql -h controller -u cinder -pcinder -e "use cinder;show tables;"

### 配置Haproxy

> Haproxy 代理 Cinder 配置

    cat <<'EOF' >>/etc/haproxy/haproxy.cfg

    ##########Cinder_API_cluster##########
    listen Cinder_API_cluster
      bind controller:8776
      #balance source
      option tcpka
      option httpchk
      option tcplog
      server controller1 controller1:8776 check inter 2000 rise 2 fall 5
      server controller2 controller2:8776 check inter 2000 rise 2 fall 5
      server controller3 controller3:8776 check inter 2000 rise 2 fall 5
    EOF
---

> 同步配置到其他节点

    rsync -avzP -e 'ssh -p 22' /etc/haproxy/haproxy.cfg controller2:/etc/haproxy/
    rsync -avzP -e 'ssh -p 22' /etc/haproxy/haproxy.cfg controller3:/etc/haproxy/
---

### 服务管理

    for HOST in controller{1..3}; do
        echo "------------ $HOST ------------"
        ssh -T $HOST <<'EOF'
        # 重启 nova-api, haproxy
        systemctl restart openstack-nova-api haproxy
        
        # cinder服务跟随系统启动
        systemctl enable openstack-cinder-api openstack-cinder-scheduler
        
        # 启动 cinder 服务
        systemctl start openstack-cinder-api openstack-cinder-scheduler
    EOF
    done

### 添加 cinder1 主机免密码登录

    source ~/SSH_KEY
    SSH_KEY cinder1                        # controller1 免密码登录 cinder1
    scp /etc/hosts cinder1:/etc/           # 同步hosts
    scp ~/admin-openstack.sh cinder1:~/    # 同步 opentstack 管理员凭证

## 存储节点安装配置

> cinder 主机执行

> cinder 后端可以使用LVM, NFS,分布式存储等 (本次安装以LVM, NFS为例 /dev/sdb1: nfs  /dev/sdb2: lvm)

### 初始化环境

    curl http://home.onlycloud.xin/code/openstack-01_init.sh | sh

### 创建分区

> 快速分区,新建2个30G分区

    echo -e 'n\np\n1\n\n+30G\nw' | fdisk /dev/sdb
    echo -e 'n\np\n2\n\n+30G\nw' | fdisk /dev/sdb
---

> 格式化分区

    mkfs.ext4 /dev/sdb1; mkfs.ext4 /dev/sdb2

### 挂载

    # 创建数据目录,挂载
    mkdir -p /data

    # 开机挂载磁盘
    echo '/dev/sdb1       /data   ext4    defaults        0 0' >>/etc/fstab
    mount -a

### 安装配置 Cinder

> 安装 openstack 客户端, 工具, cinder, lvm2

    yum install -y python-openstackclient openstack-selinux openstack-utils
    yum install -y openstack-cinder targetcli python-keystone lvm2 nfs-utils rpcbind
---

> cinder 配置文件

    # 备份默认配置文件
    [ -f /etc/cinder/cinder.conf.bak ] || cp /etc/cinder/cinder.conf{,.bak}
     
    # 获取第一块网卡 ip地址
    MyIP=$(ip add|grep global|awk -F'[ /]+' '{ print $3 }'|head -n 1)
    
    # 创建 cinder 配置文件
    cat <<'EOF' >/etc/cinder/cinder.conf
    [DEFAULT]
    auth_strategy = keystone
    log_dir = /var/log/cinder
    state_path = /var/lib/cinder
    glance_api_servers = http://controller:9292
    transport_url = rabbit://openstack:openstack@controller
    enabled_backends = lvm,nfs

    [database]
    connection = mysql+pymysql://cinder:cinder@controller/cinder

    [keystone_authtoken]
    auth_uri = http://controller:5000
    auth_url = http://controller:35357
    memcached_servers = controller1:11211,controller2:11211,controller3:11211
    auth_type = password
    project_domain_name = default
    user_domain_name = default
    project_name = service
    username = cinder
    password = cinder

    [oslo_concurrency]
    lock_path = /var/lib/cinder/tmp

    [lvm]
    volume_driver = cinder.volume.drivers.lvm.LVMVolumeDriver
    iscsi_helper = lioadm
    iscsi_protocol = iscsi
    volume_group = cinder_lvm01
    iscsi_ip_address = cinder_ip
    volumes_dir = $state_path/volumes
    volume_backend_name = lvm01

    [nfs]
    volume_driver = cinder.volume.drivers.nfs.NfsDriver
    nfs_shares_config = /etc/cinder/nfs_shares
    nfs_mount_point_base = $state_path/mnt
    volume_backend_name = nfs01
    EOF
    
    sed -i "s/cinder_ip/$MyIP/" /etc/cinder/cinder.conf
---

> 设置 cinder 配置文件权限

    chmod 640 /etc/cinder/cinder.conf 
    chgrp cinder /etc/cinder/cinder.conf 
---

### 配置 lvm 后端存储

>创建 LVM 物理卷 pv 与卷组 vg

    echo -e 'y\n' | pvcreate /dev/sdb2
    vgcreate cinder_lvm01 /dev/sdb2
    vgdisplay
---

> 配置LVM过滤，只接收上面配置的lvm设备/dev/sdb2( sdb2 作为lvm 存储)

    # 备份lvm默认配置文件
    [ -f /etc/lvm/lvm.conf.bak ] || cp /etc/lvm/lvm.conf{,.bak}
    sed -i '141a\        filter = [ "a/sdb2/", "r/.*/"]' /etc/lvm/lvm.conf
---

### 配置 NFS 后端存储

    # 获取第一块网卡 ip地址
    MyIP=$(ip add|grep global|awk -F'[ /]+' '{ print $3 }'|head -n 1)
    
    # 创建数据目录,设置目录权限
    mkdir -p /data/{cinder_nfs1,cinder_nfs2}
    chown cinder:cinder /data/cinder_nfs1
    chmod 777 /data/cinder_nfs1
    echo "/data/cinder_nfs1 *(rw,root_squash,sync,anonuid=165,anongid=165)">/etc/exports
    exportfs -r
    
    # 配置 NFS 权限
    echo "$MyIP:/data/cinder_nfs1" >/etc/cinder/nfs_shares
    chmod 640 /etc/cinder/nfs_shares
    chown root:cinder /etc/cinder/nfs_shares
    
    # 启动服务,设置开机启动, 验证 NFS
    systemctl restart rpcbind nfs-server
    systemctl enable rpcbind nfs-server
    showmount -e localhost
---

### 服务管理

    systemctl enable openstack-cinder-api openstack-cinder-scheduler
    systemctl start openstack-cinder-api openstack-cinder-scheduler
    systemctl enable openstack-cinder-volume target
    systemctl start openstack-cinder-volume target

### 验证

    source ~/admin-openstack.sh
    cinder service-list
    openstack volume service list     # 能看到存储节点@lvm, @nfs 且up状态说明配置成功
    +------------------+-------------+------+---------+-------+----------------------------+
    | Binary           | Host        | Zone | Status  | State | Updated At                 |
    +------------------+-------------+------+---------+-------+----------------------------+
    | cinder-scheduler | controller1 | nova | enabled | up    | 2018-07-29T13:22:04.000000 |
    | cinder-scheduler | controller2 | nova | enabled | up    | 2018-07-29T13:22:10.000000 |
    | cinder-scheduler | controller3 | nova | enabled | up    | 2018-07-29T13:22:05.000000 |
    | cinder-scheduler | cinder1     | nova | enabled | up    | 2018-07-29T13:22:07.000000 |
    | cinder-volume    | cinder1@lvm | nova | enabled | up    | 2018-07-29T13:22:08.000000 |
    | cinder-volume    | cinder1@nfs | nova | enabled | up    | 2018-07-29T13:22:04.000000 |
    +------------------+-------------+------+---------+-------+----------------------------+

### 创建云硬盘

> 创建云硬盘类型,关联volum

    # LVM 
    # (backend_name与配置文件名对应)
    cinder type-create lvm
    cinder type-key lvm set volume_backend_name=lvm01
    
    # NFS
    cinder type-create nfs
    cinder type-key nfs set volume_backend_name=nfs01

    # check
    cinder extra-specs-list
    cinder type-list
    
    # delete
    # cinder type-delete nfs
    # cinder type-delete lvm
---

> 创建云盘(容量单位G)

    openstack volume create --size 10 --type lvm disk01        # lvm类型
    openstack volume create --size 10 --type nfs disk02        # nfs类型
    openstack volume list
---

## 使用脚本

> 存储控制端(controller1 执行)

    site='http://home.onlycloud.xin'
    wget $site/code/openstack-10_cinder-cluster.sh -O openstack-cinder-cluster.sh
    sh openstack-cinder-cluster.sh
---

> 存储节点(controller1 执行)

    ssh cinder1 <<EOF
    site='http://home.onlycloud.xin'
    curl $site/code/openstack-10_cinder.sh -o openstack-cinder.sh
    sh openstack-cinder.sh
    EOF
