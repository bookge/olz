---
title: Heartbeat-DRBD-NFS-Cluster
tags:
  - Heartbeat
  - DRBD
  - NFS
copyright: true
categories:
  - Cluster
abbrlink: 6b7
date: 2018-08-13 00:07:10
---

## 环境说明

```conf
MASTER: data-1-1  

    Heartbeat:

        ens33: 10.0.0.11    管理IP, Lan数据转发    VIP：10.0.0.101
        ens37: 10.0.10.11   NFS服务器心跳链接
        ens38: 10.0.11.11   NFS服务器DRDB同步 

    DRBD:

        管理IP        : ens33:10.0.0.11
        DRBD管理名称  : data
        DRBD挂载目录  : /data
        DRBD逻辑设备  : /dev/drbd0
        DRBD对接IP    : ens37:10.0.11.11
        DRBD存储设备  : /dev/sdb1
        DRBDMeta设备  : /dev/sdb2[0]

BACKUP: data-1-2

    Heartbeat:

        ens33: 10.0.0.12    管理IP, Lan数据转发    VIP：10.0.0.102
        ens37: 10.0.10.12   NFS服务器心跳链接
        ens38: 10.0.11.12   NFS服务器DRDB同步

    DRBD:

        管理IP        : ens33:10.0.0.12
        DRBD管理名称  : data
        DRBD挂载目录  : /data
        DRBD逻辑设备  : /dev/drbd0
        DRBD对接IP    : ens37:10.0.11.12
        DRBD存储设备  : /dev/sdb1
        DRBDMeta设备  : /dev/sdb2[0]

SLAVE: data-1-3 

        ens33: 10.0.0.13    管理IP
```

## 配置网络参数

> `data-1-1`网络参数(设置主机名, 删除网卡配置文件重新生成网络配置参数,添加hosts解析,配置心跳网络路由)

    hostnamectl set-hostname data-1-1
    NetName=ens33
    rm -f /etc/sysconfig/network-scripts/ifcfg-$NetName
    nmcli con add con-name $NetName ifname $NetName autoconnect yes type ethernet ip4 10.0.0.11/24 ipv4.dns "202.103.24.68" ipv4.gateway "10.0.0.2"

    NetName=ens37
    rm -f /etc/sysconfig/network-scripts/ifcfg-$NetName
    nmcli con add con-name $NetName ifname $NetName autoconnect yes type ethernet ip4 10.0.10.11/24

    NetName=ens38
    rm -f /etc/sysconfig/network-scripts/ifcfg-$NetName
    nmcli con add con-name $NetName ifname $NetName autoconnect yes type ethernet ip4 10.0.11.11/24
    nmcli connection reload

    # Hosts (主机名解析到心跳网卡真实IP)
    cat <<EOF   >>/etc/hosts
    10.0.10.11    data-1-1
    10.0.10.12    data-1-2
    EOF

    # data-1-1 到达 data-1-2 心跳网络走 ens37 网卡
    route add -host 10.0.10.12 dev ens37
    echo '/sbin/route add -host 10.0.10.12 dev ens37' >>/etc/rc.local
    chmod +x /etc/rc.local
---

> `data-1-2`网络参数(设置主机名, 删除网卡配置文件重新生成网络配置参数,添加hosts解析,配置心跳网络路由)

    hostnamectl set-hostname data-1-2
    NetName=ens33
    rm -f /etc/sysconfig/network-scripts/ifcfg-$NetName
    nmcli con add con-name $NetName ifname $NetName autoconnect yes type ethernet ip4 10.0.0.12/24 ipv4.dns "202.103.24.68" ipv4.gateway "10.0.0.2"

    NetName=ens37
    rm -f /etc/sysconfig/network-scripts/ifcfg-$NetName
    nmcli con add con-name $NetName ifname $NetName autoconnect yes type ethernet ip4 10.0.10.12/24

    NetName=ens38
    rm -f /etc/sysconfig/network-scripts/ifcfg-$NetName
    nmcli con add con-name $NetName ifname $NetName autoconnect yes type ethernet ip4 10.0.11.12/24
    nmcli connection reload

    # Hosts (主机名解析到心跳网卡真实IP)
    cat <<EOF   >>/etc/hosts
    10.0.10.11    data-1-1
    10.0.10.12    data-1-2
    EOF

    # data-1-2 到达 data-1-1 心跳网络走 ens37 网卡
    oute add -host 10.0.10.11 dev ens37
    echo '/sbin/route add -host 10.0.10.11 dev ens37' >>/etc/rc.local
    chmod +x /etc/rc.local
---

> `data-1-3`网络参数

    hostnamectl set-hostname data-1-3
    NetName=ens33
    rm -f /etc/sysconfig/network-scripts/ifcfg-$NetName
    nmcli con add con-name $NetName ifname $NetName autoconnect yes type ethernet ip4 10.0.0.13/24 ipv4.dns "202.103.24.68" ipv4.gateway "10.0.0.2"

    NetName=ens37
    rm -f /etc/sysconfig/network-scripts/ifcfg-$NetName
    nmcli con add con-name $NetName ifname $NetName autoconnect yes type ethernet ip4 10.0.10.13/24

    NetName=ens38
    rm -f /etc/sysconfig/network-scripts/ifcfg-$NetName
    nmcli con add con-name $NetName ifname $NetName autoconnect yes type ethernet ip4 10.0.11.13/24
    nmcli connection reload
---

## 添加软件依赖安装源

```conf
# 配置软件依赖安装源 (cluster-glue-libs-devel 安装源)
cat <<EOF   >/etc/yum.repos.d/gf-epel-7.repo
[epel-testing]
name=Extra Packages for Enterprise Linux 7 - $basearch - Testing
baseurl=http://mirror.ghettoforge.org/distributions/gf/el/7/testing/x86_64
enabled=1
gpgcheck=0
EOF
```

## 安装Heartbeat

    # 下载软件包并安装 heartbeat 
    # RPM包生成方法及编译安装方法  http://home.onlycloud.xin/posts/dae5.html
    heartbeat='heartbeat-3.0.6-1.el7.centos.x86_64.rpm'
    heartbeat_libs='heartbeat-libs-3.0.6-1.el7.centos.x86_64.rpm' 
    curl -o $heartbeat http://home.onlycloud.xin/soft/heartbeat/$heartbeat --progress
    curl -o $heartbeat_libs http://home.onlycloud.xin/soft/heartbeat/$heartbeat_libs --progress
    yum localinstall -y heartbeat-*

## 配置Heartbeat

```conf
# 复制配置文件模板
cp /usr/share/doc/heartbeat-3.0.6/{ha.cf,authkeys,haresources} /etc/ha.d/

# ha.cf 配置文件
cat <<EOF   >/etc/ha.d/ha.cf
# 调试日志
debugfile /var/log/ha-debug

# heartbeat 运行日志
logfile /var/log/ha-log

# 在 syslog 服务中配置通过 local1 设备接收日志
logfacility local0

# 心跳间隔时间 default:2
keepalive 1

# 备用节点在 default:30 秒内没有收到主节点心跳信号则立即接管主节点服务资源
deadtime 5

# 心跳延迟时间为 default:10 秒,当 10 秒内备份节点未收到主节点心跳信号时,将写入警告日志,此时不会切换服务
warntime 3

# heartbeat 服务首次启动,需要等待 default:60 秒后才启动服务器的资源,该值至少为 deadtime 两倍(单机启动时vip绑定比较慢)
initdead 10

# 指定心跳信号网卡(直连网卡)
#bcast ens37

# 配置多播通信路径(直连网卡)
mcast ens37 239.0.0.1 694 1 0

# 主节点恢复后,是否将服务自动切回
auto_failback on

# 节点主机名(使用IP地址也可以)
node data-1-1
node data-1-2

# 是否开启 CRM 集群管理
crm no
EOF

# 两端认证方式 authkeys
cat <<EOF  >>/etc/ha.d/authkeys

auth 1
1 sha1 cxix9lkzq2aevnxv43yn68yoh2y7zp2cfwufgs7w
EOF
chmod 600 /etc/ha.d/authkeys

# haresources添加资源
# 相当于执行脚本 /etc/ha.d/resource.d/IPaddr 10.0.0.101/24/ens33 stop/start
cat <<EOF   >/etc/ha.d/haresources
# 虚拟IP配置到 ens33 网卡(虚拟IP流量走 ens33网卡)
data-1-1 IPaddr::10.0.0.101/24/ens33
data-1-2 IPaddr::10.0.0.102/24/ens33
EOF
```

## 验证Heartbeat

```conf
# 启动服务,验证(两个节点各自执行 ip add 能看到两个 10.0.0网段ip)
systemctl start heartbeat
netstat -ntaulp
ip add | grep 10.0.0

# 关闭一端服务,然后在另一端验证(在正常的一端已经接管了所有虚拟IP ip add 能看到三个 10.0.0网段ip)
systemctl stop heartbeat
ip add | grep 10.0.0

# 启动关闭的服务后验证 虚拟IP已经恢复
systemctl start heartbeat
ip add | grep 10.0.0
```

## 硬盘分区

>此处分区比较小,是因为在同步数据时会将整个硬盘同步,需要的时间较长且占用宿主机存储

```conf
# 硬盘分区, 格式化
parted /dev/sdb mklabel gpt      # 更改分区表格式为 GPT
echo -e 'n\np\n1\n\n+3G\nw' | fdisk /dev/sdb
echo -e 'n\np\n2\n\n+1G\nw' | fdisk /dev/sdb
partprobe

# 格式化(sdb2分区不用格式化) 可以启动DRBD后格式化 drbd分区
# mkfs.ext4 /dev/sdb1
parted /dev/sdb p
```

## 安装DRBD

```conf
# YUM 安装 DRDB84
rpm -Uvh http://www.elrepo.org/elrepo-release-7.0-2.el7.elrepo.noarch.rpm
yum install -y kmod-drbd84 drbd84-utils

# 加载DRBD驱动内核模块, 写入随系统加载
modprobe drbd
lsmod | grep drbd
echo 'modprobe drbd' >/etc/sysconfig/modules/drbd.modules
```

## 配置DRBD, 添加资源

```conf
# DRBD 配置文件
cp /etc/drbd.conf{,.bak}
cat <<EOF   >/etc/drbd.conf
global {
    # minor-count 64;
    # dialog-refresh 5;  # 5 seconds
    # disable-ip-verification;
    usage-count no;
}

common {
    protocol C;
    
    disk {
        on-io-error detach;
        no-disk-flushes;
        no-md-flushes;
    }
    
    net {
        sndbuf-size 512k;
        # timeout             60;    #  6 seconds (unit = 0.1 seconds)
        # connect-int         10;    # 10 seconds (unit = 1 seconds)
        # ping-timeout         5;
        max-buffers         8000;
        unplug-watermark    1024;
        max-epoch-size      8000;
        # ko-count             4;
        # allow-two-primaries;
        cram-hmac-alg "sha1";
        shared-secret "q1366zxxlzzv5rnndx4o";
        after-sb-0pri disconnect;
        after-sb-1pri disconnect;
        after-sb-2pri disconnect;
        rr-conflict   disconnect;
        # data-integrity-alg "md5";
        # no-tcp-cork;
    }
    
    syncer {
        rate                330M;   # 同步限速(不要给太大影响正常服务)
        al-extents           517;
    }
}


# RESOURCE data
resource data {
    protocol C;
    
    disk {
        on-io-error detach;
    }
    
    on data-1-1 {
        device     /dev/drbd0;
        disk       /dev/sdb1;
        address    10.0.11.11:7788;   # 数据同步网络 
        meta-disk  /dev/sdb2[0];      # meta-disk internal;  # 内部模式只需要一个分区即可
    }
    
    on data-1-2 {
        device     /dev/drbd0;
        disk       /dev/sdb1;
        address    10.0.11.12:7788;   # 数据同步网络
        meta-disk  /dev/sdb2[0];      # meta-disk internal;  # 内部模式只需要一个分区即可
    }
}
EOF
```
## 初始化DRBD

```conf
# 两端执行初始化(两端执行完毕后状态应该为 0: 'cs:Connected ro:Secondary/Secondary ds:Inconsistent/Inconsistent')
drbdadm create-md data
drbdadm up data
cat /proc/drbd

# 查看角色 服务启动/重启后默认为都为从 (Secondary/Secondary 从/从 本主机/对端主机)
drbdadm role data

# 设置主并同步数据(哪台执行则哪台为主) 同步完成后状态 0: cs:Connected ro:Secondary/Primary ds:UpToDate/UpToDate
drbdadm -- --overwrite-data-of-peer primary data

# 查看角色 (Primary/Secondary  主/从 本主机/对端主机)
drbdadm role data

# 主端格式化 DRBD分区(从端会自动同步不用管)
mkfs.ext4 -b 4096 /dev/drbd0
tune2fs -c -1 /dev/drbd0
```

## 测试验证DRBD

```conf
# 挂载drbd设备(请不要用物理设备挂载/dev/sdb1 否则执行文件操作重启服务后会出现故障)
# 数据通过DRBD裸设备读取数据 如果挂载/dev/sdb1进行删除文件操作会导致数据不同步
mkdir /md1
mount /dev/drbd0 /md1

# 写文件测试
cd /md1
for i in {1..100};do  echo "$(df -hT)"  >$i.txt; done
ls /md1
cat /md1/50.txt

# 从端验证(请不要手动操作分区里边的数据 会导致同步异常)
drbdadm down data
mkdir /md1
mount /dev/sdb1 /md1
ls /md1
cat /md1/50.txt

# 恢复同步
umount /dev/sdb1
drbdadm up data
```

## 配置Heartbate管理DRBD

```conf
# 两端都执行
# 停止Heartbeat服务
systemctl stop heartbeat

# 配置 Heartbeat 资源
# IPaddr::10.0.0.101/24/ens33  相当于执行脚本(配置VIP 10.0.0.101) /etc/ha.d/resource.d/IPaddr 10.0.0.101/24/ens33 stop/start
# drbddisk::data (DRBD 资源名) 相当于执行(将当前drbd data 资源设置为主/从) /etc/ha.d/resource.d/drbddisk data stop/start
# Filesystem::/dev/drbd0::/data::ext4 相当于执行(挂载数据) /etc/ha.d/resource.d/Filesystem /dev/drbd0/ /md1 ext4 stop/start
cat <<EOF   >/etc/ha.d/haresources
# 虚拟IP
data-1-1 IPaddr::10.0.0.101/24/ens33 drbddisk::data Filesystem::/dev/drbd0::/md1::ext4
data-1-2 IPaddr::10.0.0.102/24/ens33 drbddisk::data Filesystem::/dev/drbd0::/md1::ext4
EOF

# 启动Heartbeat服务
systemctl start heartbeat

## 模拟脑裂恢复

```conf
# 将当前主节点虚拟机挂起模拟脑裂状态(通过命令查看Primary 在前面则当前主机为主节点 cat /proc/drbd  ro:Primary/Secondary)
1. 验证从节点状态(此时从节点已经切换为主节点)
ip add | grep 10.0.0      # 此时虚拟IP都漂移到了从节点
cat /proc/drbd            # 查看DRBD状态 当前节点已经变为主节点  cs:WFConnection ro:Primary/Unknown ds:UpToDate/DUnknown
df -hT                    # 此时/md1 已经被挂载 /dev/drbd0              ext4      2.9G  9.2M  2.8G   1% /md1
cd /md1                   # 进入挂载目录写入新数据
touch {1..50}.txt


2. 恢复挂起的虚拟机 恢复脑裂状态(两端执行 cat /proc/drbd 查看本端状态正常对方为 Unknown 则为脑裂状态)
# 从节点执行(数据滞后的一端 数据需要放弃的一端 也就是之前的主节点 被挂起的虚拟机)
modprobe drbd
drbdadm secondary data
drbdadm up data
drbdadm disconnect data
drbdadm -- --discard-my-data connect data
cat /proc/drbd

# 主节点执行(当前的主节点)
drbdadm connect data
cat /proc/drbd        # 查看状态两端同步状态为 ds:UpToDate/UpToDate  0: cs:Connected ro:Secondary/Primary ds:UpToDate/UpToDate

# 从节点重启heartbeat服务,自动切换为主节点
systemctl stop heartbeat
systemctl start heartbeat
ls /md1              # 此时查看在脑裂期间创建的数据已经同步到主节点
```

## 手动管理

```conf
# 手动执行切换
/etc/ha.d/resource.d/IPaddr 10.0.0.101/24/ens33 status       # 虚拟IP 10.0.0.101 在当前节点状态
/etc/ha.d/resource.d/IPaddr 10.0.0.101/24/ens33 stop         # 在执行的主机释放虚拟IP 10.0.0.101
/etc/ha.d/resource.d/IPaddr 10.0.0.102/24/ens33 start        # 在执行的主机添加虚拟IP 10.0.0.102
/etc/ha.d/resource.d/drbddisk data status                    # 状态为 running (Primary) 为主节点
/etc/ha.d/resource.d/drbddisk data start                     # 在两端都重启或状态都为从节点 可以通过此命令设置主节点哪台执行则哪台为主节点
/etc/ha.d/resource.d/Filesystem /dev/drbd0 /md1 ext4 status  # 查看挂载状态
/etc/ha.d/resource.d/Filesystem /dev/drbd0 /md1 ext4 start   # 挂载DRBD设备只有主节点才能挂载(从节点执行会报错)
```

## 安装配置NFS

### NFS Server端(data-1-1 data-1-2)

> 安装配置NFS

    yum install -y nfs-utils rpcbind
---

> 启动服务, 查看服务状态(nfs服务由heartbeat管理)

    systemctl restart rpcbind nfs
    systemctl status rpcbind nfs
    systemctl enable rpcbind
    systemctl is-enabled rpcbind
    rpcinfo -p localhost
---

> 配置NFS资源, 权限

    chmod 777 /md1/
    cat <<EOF  >/etc/exports
    /md1  10.0.0.*(rw,sync)
    EOF
---

> 重读配置, 查看nfs资源
    exportfs -r
    showmount -e localhost
---

### NFS Clietn端(web-01 web-02)

> 安装NFS ,启动rpcbind

    yum install -y nfs-utils rpcbind
    systemctl start rpcbind
    systemctl enable rpcbind
---

> 查看服务资源

    showmount -e 10.0.0.101
---

> 创建挂载目录授权

    mkdir /md1
    chown -R 777 /md1
---

> 挂载数据目录

    mount -t nfs 10.0.0.101:/md1 /md1
    echo -e "\n# Mount NFS\nmount -t nfs 10.0.0.101:/md1 /md1" >>/etc/rc.local
    chmod +x /etc/rc.d/rc.local
---

## 配置Heartbate管理NFS

### 创建NFS控制脚

```conf
cat <<'!'   >/etc/ha.d/resource.d/killnfsd
#!/bin/bash
nfs_usage(){
    cat <<EOF
    USAGE: $0 {start|stop}
EOF
}

nfs_start(){
    systemctl start nfs

    rc=$?
    if [ $rc -ne 0 ];then
        echo "NFS Server Start Failed"
        exit $rc
    else
        echo "NFS Server Start Success"
    fi     
}

nfs_stop(){
    killall -9 nfsd
    systemctl stop nfs
    
    rc=$?
    if [ $rc -ne 0 ];then
        echo "NFS Server Stop Failed"
        exit $rc
    else
        echo "NFS Server Stop Success"
    fi        
}

case $1 in
    start)
        nfs_start
        ;;
    stop)
        nfs_stop
        ;;
    *)
        nfs_usage
        exit 1
        ;;
esac
!
```
---
> 添加可执行权限

    chmod +x /etc/ha.d/resource.d/killnfsd 
---

### 配置Heartbeat资源

    # 停止heartbeat服务
    systemctl stop heartbeat

    # 配置资源
    cat <<EOF   >/etc/ha.d/haresources
    # 虚拟IP(提供资源访问)配置到ens33接口 对外资源流量也走ens33接口
    data-1-1 IPaddr::10.0.0.101/24/ens33 drbddisk::data Filesystem::/dev/drbd0::/md1::ext4 killnfsd
    data-1-2 IPaddr::10.0.0.102/24/ens33 drbddisk::data Filesystem::/dev/drbd0::/md1::ext4 killnfsd
    EOF

    # 启动heartbeat服务
    systemctl start heartbeat

## 验证

    # ssh登录 web-01 web-02
    df -hT
    ls /md1

    # 关闭主节点(data-1-1)heartbeat 服务
    systemctl stop heartbeat

    # ssh登录 web-01 web-02
    df -hT
    ls /md1

    # 启动(data-1-1)heartbeat 服务
    systemctl start heartbeat

    # ssh登录 web-01 web-02
    df -hT
    ls /md1

    # web-01 web-02 应该能自动挂载其中切换主备时会出现几秒超时现象

## 服务重启后启动顺序(两端断电或重启)

    # 1.两端都启动DRBD资源,查看状态(两端都为从 ro:Secondary/Secondary)
    drbdadm up data
    cat /proc/drbd

    # 2.在主端(data-1-1)启动heartbeat服务(查看DRBD状态 ro:Primary/Secondary)
    systemctl start heartbeat
    sleep 3
    cat /proc/drbd
    ip add | grep 10.0.0
    df -hT

    # 3.启动备端(data-1-2)(执行完成命令后当前节点已经变为主节点)
    systemctl start heartbeat
    sleep 3
    cat /proc/drbd
    ip add | grep 10.0.0
    df -hT

    # 4.1 自动切换主节点为(data-1-1) 如果未能切换请重复执行或查看 heartbeat 日志 tail -f /var/log/ha-log
    systemctl stop heartbeat
    systemctl start heartbeat
    sleep 3
    cat /proc/drbd
    ip add | grep 10.0.0
    df -hT

    # 4.2 手动切换主节点
    # data-1-2(执行后变备节点 释放VIP →停止nfs → 取消drbd设备挂载 → drbd状态为备)
    /etc/ha.d/resource.d/IPaddr 10.0.0.101/24/ens33 stop
    /etc/ha.d/resource.d/killnfsd  stop
    /etc/ha.d/resource.d/Filesystem /dev/drbd0 /md1 ext4 stop
    /etc/ha.d/resource.d/drbddisk data stop

    # data-1-1(执行后变主节点 接管VIP → drbd状态为主 → 挂载drbd设备 → 启动nfs)
    /etc/ha.d/resource.d/IPaddr 10.0.0.101/24/ens33 start
    /etc/ha.d/resource.d/drbddisk data start
    /etc/ha.d/resource.d/Filesystem /dev/drbd0 /md1 ext4 start
    /etc/ha.d/resource.d/killnfsd start
---

## 配置inotyfy实现双主多从

    待续。。。。

