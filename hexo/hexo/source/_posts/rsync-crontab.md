---
title: rsync+crontab实现自动同步
tags:
  - 文件同步
  - rsync
  - linux
categories:
  - linux
abbrlink: 10f6
date: 2017-12-20 15:03:54
---
## 摘要

rsync命令是一个远程数据同步工具，可通过LAN/WAN快速同步多台主机间的文件。rsync使用所谓的“rsync算法”来使本地和远程两个主机之间的文件达到同步，这个算法只传送两个文件的不同部分，而不是每次都整份传送，因此速度相当快。

|Server:            |Client                        |
| ----------------- | ---------------------------- |
|  IP: 192.168.0.8  |  IP: 192.168.3.3             |
|  Directory: /tmp  |  Directory: /root/rsync_web  |

## Servr端配置

### 安装

    yum -y install rsync

### 配置rsync主配置文件

|     全局配置      |                        |
|--------------     | ---------------------- |
|`uid`              |`运行RSYNC守护进程的组` |
|`gid`              |`运行RSYNC守护进程的组` |
|`use chroot`       |`安全防护`              |
|`time out`         |`客户端超时时间`        |
|`max connections`  |`最大连接数`            |
|`pid file`         |`pid文件的存放位置`     |
|`lock file`        |`锁文件的存放位置`      |
|`log file`         |`日志文件的存放位置`    |

>如果`use chroot`指定为`true`，那么`rsync`在传输文件以前首先`chroot`到`path`参数所指定的目录下。这样做的原因是实现额外的安全防护，但是缺点是需要以`root`权限，并且不能备份指向外部的符号连接所指向的目录文件。默认情况下`chroot`值为`true`.

|        数据与认证        |                                |
| -------------------- | ----------------------------------------------------------------------- |
|`[rsync_tmp_backup]`  |`认证模块名，对外公布的名字 路径：user@192.168.0.8::rsync_master_backup` |
|`auth users`          |`认证用户名`                                                             |
|`secrets file`        |`认证用户密码文件，配置auth users的密码`                                 |
|`path`                |`同步的目录`                                                             |
|`ignore errors`       |`忽略无关的IO错误`                                                       |
|`red only`            |`读写权限 [false:可读可写]，[ true:只读]`                                |
|`list`                |`查看服务器文件列表权限[false:禁止]，[ true:允许] `                      |
|`hosts allow`         |`允许访问服务的IP[IP,地址段，*]`                                         |
|`hosts deny`         |`禁止访问服务的IP[IP,地址段，*]`                                          |

>认证用户密码文件需要手动建立，创建后需要修改文件的权限为600,list所有用户都可查询不受`hosts deny` 限制

``` bash
cat <<EOF >/etc/rsyncd.conf
uid = nobody
gid = nobody
use chroot = no
timeout = 600
max connections = 200
pid file = /var/run/rsyncd.pid
lock file = /var/run/rsyncd.lock
log file = /var/log/rsyncd.log

[rsync_tmp_backup]
auth users = backup
secrets file = /etc/rsync_tmp.pass
path = /tmp/
ignore errors
read only = true
list = true
hosts allow = 192.168.3.3/255.255.255.255
hosts deny = *
EOF
```

### 创建rsync虚拟用户
>每个用户一行，前面是用户名,后面是密码,该文件的所有者必须是root，权限是600

    echo "backup:123456" >/etc/rsync_s.pass
    chmod 600 /etc/rsync_s.pass

### 启动/测试rsync服务
>注意防火墙,selinux是否阻止程序正常运行

    rsync --daemon
    ps -ef | grep rsync
    rsync --list-only backup@192.168.0.8::

### rsync开机启动

    echo "/usr/bin/rsync --daemon" >> /etc/rc.local
    chmod +x /etc/rc.d/rc.local


## Client端配置

### 安装

    yum -y install rsync

### 创建密码文件
>客户端密码文件只需要指定密码即可，注意密码文件权限

    echo "123456" >/etc/rsync_c.pass
    chmod 600 /etc/rsync_c.pass

### 测试
>此处的backup用户为服务器主配置文件中指定的虚拟用户，不需要是本地用户或服务器本地用户

#### 列出允许列出清单的rsync目录
    rsync --list-only backup@192.168.0.8::

#### 下载文件
|     常用参数          |                        |
| --------------------- | --------------------------------------------- |
|`-v`                   |`--verbose：详细模式输出`                      |
|`-P`                   |`显示传输进度`                                 |
|`-r`                   |`--recursive：对子目录以返回模式处理`          |
|`-p`                   |`--perms：保持文件许可权`                      |
|`-o`                   |`--owner：保持文件属主信息`                    |
|`-g`                   |`--group：保持文件组信息`                      |
|`-t`                   |`--times：保持文件时间信息`                    |
|`--delete`             |`删除哪些DST中存在而SRC中不存在的文件或目录`   |
|`--delete-excluded`    |`同样删除接收端哪些该选项制定排出的文件`       |
|`-z`                   |`--compress：对备份的文件在传输时进行压缩处理` |
|`--exclude=PATTERN`    |`制定排除不需要传输的文件`                     |
|`--include=PATTERN`    |`制定不排除需要传输的文件`                     |
|`--exclude-from=FILE`  |`排除FILE中制定模式的文件`                     |
|`--include-from=FILE`  |`不排除FILE中制定模式匹配的文件`               |

    rsync -azvP  --delete --password-file=/etc/rsync_c.pass backup@192.168.0.8::rsync_tmp_backup /tmp/

#### 上传文件
>配置文件中已设置禁止上传 `read only`

    rsync -avz --password-file=/etc/rsync_c.password /root backup@192.168.0.8::rsync_tmp_backup