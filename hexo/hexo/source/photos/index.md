---
title: photos
date: 2019-10-13 16:47:32
type: photos
---

{% raw %}
<style>
.photo img{
    border: 1px solid #999;
    height:150px;
    width: 150px;
}
.photo li{
    margin: 10px;
    float: left;
    list-style: none;
    display: block;
}
</style>
<div class="photo">
{% endraw %}

* ![](http://oevo99fcp.bkt.clouddn.com/1fd85d5f74fe5b5ebaf6189f1cb71561.jpg)
* ![](http://oevo99fcp.bkt.clouddn.com/1fd85d5f74fe5b5ebaf6189f1cb71561.jpg)
* ![](/images/avatar.png)
* ![](http://192.168.0.72/images/avatar.png)
* ![](http://192.168.0.72/images/avatar.png)
* ![](http://192.168.0.72/images/avatar.png)
* ![](http://192.168.0.72/images/avatar.png)
* ![](http://192.168.0.72/images/avatar.png)

{% raw %}
<div style="clear:both"></div>
</div>
{% endraw %}
