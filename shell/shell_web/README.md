
### shell 简单 web server

Fork: [benrady/shinatra](https://github.com/benrady/shinatra)

#### 用法:

    curl -sSL https://dwz.cn/y3gV4i3L -o service.sh
    chmod +x service.sh

> Example

    # port 监听端口 response 输出给客户端的信息
    ./service.sh [port] [response]

#### 测试:

> 1.服务端

    ./service.sh 8080 "$(cat /etc/hosts)"

> 2.客户端

    curl http://服务端IP:8080

#### 安装依赖

> CentOS

    yum install -y nc

> debian

    apt-get install -y netcat

> ubuntu

    apt-get -y install netcat-traditional