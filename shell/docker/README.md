### Docker

> 安装 [ docker | docker-compose | harbor ]

    curl -sSL https://dwz.cn/XOJj0Njx -o install-docker.sh
    chmod +x install-docker.sh
    
      ./install-docker.sh -h
      ./install-docker.sh -i docker -v 19.03.3

    Example:
      bash <(curl -sSL https://dwz.cn/XOJj0Njx) -i docker
      bash <(curl -sSL https://dwz.cn/XOJj0Njx) -i compose
      bash <(curl -sSL https://dwz.cn/XOJj0Njx) -i harbor

> vscode

````shell
docker run -dit \
  --restart=always \
  --name vscode-server \
  -h vscode-server \
  -u "$(id -u):$(id -g)" \
  -p 8080:8080 \
  -v /home/vscode/data:/home/coder/project \
  -v /home/vscode/.config:/home/coder/.config \
  -e PASSWORD=123ABCFED \
  -e "DOCKER_USER=$USER" \
  -v /etc/localtime:/etc/localtime:ro \
  -v /usr/bin/docker:/usr/bin/docker \
  -v /var/run/docker.sock:/var/run/docker.sock \
codercom/code-server:latest --auth password
````

> jenkins

````shell

````

> gitlab

````shell

````
