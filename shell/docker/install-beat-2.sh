#!/bin/bash

set -x

function soft_info(){
    # get version
    # docker_ver=$(get_version docker docker-ce)

    # docker var
    SOURCE_DIR=/home
    DOCKER_DATA=/var/lib/docker
    DOCKER_HOME=/usr/local/docker
    INSECURE=192.168.2.30
    MIRROR=http://3272dd08.m.daocloud.io
    HARBOR_DATA_DIR=/home/harbor/data

    # date: 2019-09-25
    HARBOR_ADMIN_PASS=admin
    DOCKER_VER=${docker_ver:-18.09.8}
    COMPOSE_VER=${compose_ver:-1.24.1}
    HARBOR_VER=${harbor_ver:-v1.9.0}
}

# get github soft version
function get_version(){
    user=$1
    repo=$2
    repo_url="https://api.github.com/repos/$user/$repo/releases/latest"
    curl -sSL "$repo_url" | grep '"name"' | head -n 1 | awk -F ":" '{print $2}' | sed 's/\"//g;s/,//g;s/ //g'
}

function get_docker_version(){
    curl -s http://mirror.azure.cn/docker-ce/linux/static/stable/x86_64/ | egrep -i -o 'href="docker-[[:digit:]]+\.[[:digit:]]+\.[[:digit:]]+' | sed 's/href="docker-//' | tail -n1
}

function install_docker(){
    # stop selinux
    setenforce 0
    sed -i 's#SELINUX=.*#SELINUX=disabled#' /etc/selinux/config

    # 下载软件
    # DOCKER_URL="https://mirrors.tuna.tsinghua.edu.cn/docker-ce/linux/static/stable/x86_64/docker-${DOCKER_VER}.tgz"
    DOCKER_URL="http://mirror.azure.cn/docker-ce/linux/static/stable/x86_64/docker-${DOCKER_VER}.tgz"
    mkdir -p $SOURCE_DIR $DOCKER_HOME/bin
    cd $SOURCE_DIR
    if [[ -f "./docker-${DOCKER_VER}.tgz" ]];then
    	echo "[INFO] docker binaries already existed"
    else
    	echo -e "[INFO] \033[33mdownloading docker binaries\033[0m $DOCKER_VER"
    	curl -C- -O --retry 3 "$DOCKER_URL" --progress || { echo "[ERROR] downloading docker failed"; exit 1; }
    fi

    # 解压
    tar zxf $SOURCE_DIR/docker-${DOCKER_VER}.tgz -C $DOCKER_HOME/bin --strip-components 1
    ln -sf $DOCKER_HOME/bin/docker /bin/docker

    # 创建 docker 服务管理脚本
    echo "[INFO] generate docker service file"
    cat > /etc/systemd/system/docker.service <<-EOF
[Unit]
Description=Docker Application Container Engine
Documentation=http://docs.docker.io
[Service]
Environment="PATH=$DOCKER_HOME/bin:/bin:/sbin:/usr/bin:/usr/sbin"
ExecStart=$DOCKER_HOME/bin/dockerd
ExecStartPost=/sbin/iptables -I FORWARD -s 0.0.0.0/0 -j ACCEPT
ExecReload=/bin/kill -s HUP \$MAINPID
Restart=on-failure
RestartSec=5
LimitNOFILE=infinity
LimitNPROC=infinity
LimitCORE=infinity
Delegate=yes
KillMode=process
[Install]
WantedBy=multi-user.target
EOF

    # 创建docker配置文件
    echo "[INFO] generate docker config file"
    echo "[INFO] prepare register mirror for $REGISTRY_MIRROR"
    mkdir -p $DOCKER_DATA /etc/docker
    cat > /etc/docker/daemon.json <<-EOF
{
  "registry-mirrors": ["$MIRROR"],
  "insecure-registries": ["$INSECURE"],
  "max-concurrent-downloads": 10,
  "log-driver": "json-file",
  "log-level": "warn",
  "log-opts": {
    "max-size": "10m",
    "max-file": "3"
    },
  "data-root": "/var/lib/docker"
}
EOF

    # 启动服务
    echo "[INFO] enable and start docker"
    systemctl enable docker
    systemctl daemon-reload
    systemctl restart docker

    # 验证
    docker info
    docker --version

    # 命令补全
    echo "[INFO] install bash_completion"
    curl -sSL https://dwz.cn/ivKazMlX -o /etc/bash_completion.d/docker
    curl -sSL https://dwz.cn/SV7woGp8 -o /usr/share/bash-completion/bash_completion
    echo -e "\nsource /usr/share/bash-completion/bash_completion" >> ~/.bashrc
    echo -e "[INFO] \033[33msource ~/.bashrc \033[0m"
}

function install_compose(){
    # 下载 / 解压 docker-compose
    echo -e "[INFO] \033[33mdownloading docker-compose binaries\033[0m $COMPOSE_VER"
    COMPOSE_URL="https://get.daocloud.io/docker/compose/releases/download/$COMPOSE_VER/docker-compose-`uname -s`-`uname -m`"
    curl -L $COMPOSE_URL > /usr/local/bin/docker-compose --progress
    chmod +x /usr/local/bin/docker-compose
    docker-compose version

    # docker-compose 命令补全
    curl -sSL https://dwz.cn/2zSPvRrv -o /etc/bash_completion.d/docker-compose
}

function install_harbor(){
    mkdir -p $SOURCE_DIR

    # 获取本机 IP
    HOST_IF=$(ip route|grep default|cut -d' ' -f5)
    HOST_IP=$(ip a|grep "$HOST_IF$"|awk '{print $2}'|cut -d'/' -f1)

    # 下载 / 解压源码
    cd $SOURCE_DIR
    echo -e "[INFO] \033[33mdownloading harbor\033[0m $HARBOR_VER"
    VER="$(echo $HARBOR_VER | tr -d '[a-z]' | cut -d. -f1,2).0"
    curl -O https://storage.googleapis.com/harbor-releases/release-${VER}/harbor-online-installer-${HARBOR_VER}.tgz
    tar xvf $SOURCE_DIR/harbor-online-installer-${HARBOR_VER}.tgz -C $SOURCE_DIR

    # 替换配置文件
    sed -i "s#harbor_admin_password:.*#harbor_admin_password: $HARBOR_ADMIN_PASS#" $SOURCE_DIR/harbor/harbor.yml
    sed -i "s#hostname:.*#hostname: $HOST_IP#" $SOURCE_DIR/harbor/harbor.yml
    sed -i "s#data_volume:.*#data_volume: $HARBOR_DATA_DIR#" $SOURCE_DIR/harbor/harbor.yml

    # 安装
    echo -e "[INFO] \033[33m install harbor \033[0m"
    mkdir -p $HARBOR_DATA_DIR
    cd $SOURCE_DIR/harbor
    ./install.sh

    # info
    echo -e "[INFO] \033[33m\n \tUser: admin  Passwd: admin \033[0m"
}

case $1 in
    docker)
        soft_info
        docker_ver=$(get_docker_version)
        install_docker ;;
    compose)
        soft_info
        compose_ver=$(get_version docker compose)
        install_compose ;;
    harbor)
        soft_info
        install_docker
        compose_ver=$(get_version docker compose)
        install_compose
        harbor_ver=$(get_version goharbor harbor)
        install_harbor ;;
    *)
        echo "Usage: $0 {docker|compose|harbor}"
esac
