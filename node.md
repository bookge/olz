> 配置 Docker 镜像站

    # 亚马逊中国镜像站
    curl -sSL https://get.daocloud.io/daotools/set_mirror.sh | sh -s https://dockerhub.azk8s.cn

    # daocloud
    curl -sSL https://get.daocloud.io/daotools/set_mirror.sh | sh -s http://f1361db2.m.daocloud.io

    # docker 中国区
    curl -sSL https://get.daocloud.io/daotools/set_mirror.sh | sh -s https://registry.docker-cn.com

    # USTC
    curl -sSL https://get.daocloud.io/daotools/set_mirror.sh | sh -s https://docker.mirrors.ustc.edu.cn

    # 网易 163
    curl -sSL https://get.daocloud.io/daotools/set_mirror.sh | sh -s http://hub-mirror.c.163.com

> 读取变量替换配置文件

```shell
NAME=ZOOK
CONFIG=/usr/local/zookeeper/conf/zk.conf

for VAR in $(env); do
    if [[ ! -z "$(echo $VAR | grep -E ^${NAME}_)" ]]; then
        VAR_NAME=$(echo "$VAR" | sed -r "s/${NAME}_([^=]*)=.*/\1/g" | tr '[:upper:]' '[:lower:]' | sed -r "s/_/\./g")
        VAR_FULL_NAME=$(echo "$VAR" | sed -r "s/([^=]*)=.*/\1/g")
        # Config in xxx.conf
        if [[ ! -z "$(cat $CONFIG |grep -E "^(^|^#*|^#*\s*)$VAR_NAME")" ]]; then
            echo "$VAR_NAME=$(eval echo \$$VAR_FULL_NAME)"
            sed -r -i "s/(^#*\s*)($VAR_NAME)\s*=\s*(.*)/\2 = $(eval echo \$$VAR_FULL_NAME|sed -e 's/\//\\\//g')/g" $CONFIG
        else
            echo "$VAR_NAME=$(eval echo \$$VAR_FULL_NAME)" | tee -a $CONFIG
        fi
    fi
done
```

> 判断变量是否存在变量中

```shell
EXCLUSIONS="|KAFKA_VERSION|KAFKA_HOME|KAFKA_DEBUG|KAFKA_GC_LOG_OPTS|KAFKA_HEAP_OPTS|KAFKA_JMX_OPTS|KAFKA_JVM_PERFORMANCE_OPTS|KAFKA_LOG|KAFKA_OPTS|"
if [[ "$EXCLUSIONS" = *"|$env_var|"* ]]; then
    echo "Excluding $env_var from broker config"
    continue
fi
```

> hadoop 删除 hbase 数据(此操作将删除hbase所有数据)

    hadoop fs -rm -r /hbase

> zookeeper 删除 hbase 信息

    zkCli.sh rmr /hbase

> 当前脚本路径

    BASE_FOLDER="$(cd "$(dirname "$0")" && pwd)"
    BASE_FOLDER="$(cd "$(dirname "${BASH_SOURCE[0]}" )" && pwd)"

> 获取 文件参数绝对路径

    realpath() { [[ $1 = /* ]] && echo "$1" || echo "$PWD/${1#./}"; }
    BASE_FOLDER="$(cd "$(dirname $(realpath "$1"))" && pwd)"