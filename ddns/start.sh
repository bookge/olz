#!/bin/bash

tag=v2.9.0

# https://github.com/jeessy2/ddns-go
docker run -d --name ddns-go --restart=always -p 9876:9876 -v /home/ddns/ddns-go:/root jeessy/ddns-go:$tag
