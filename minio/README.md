[官方文档](https://docs.min.io/cn/)

0. 启动容器

```
docker-compose up -d
```

1. [客户端下载](https://docs.min.io/cn/minio-client-quickstart-guide.html)

> Liinux

```
https://dl.min.io/client/mc/release/linux-amd64/mc
```

> Windows

```
https://dl.min.io/client/mc/release/windows-amd64/mc.exe
```

2. 配置客户端

```
chmod +x mc
/bin/cp mc /usr/local/bin/mc
mc alias set minio http://192.168.2.77:9000 minioadmin minioadmin  --api S3v4
```

2. 账号密码

```
minioadmin/minioadmin
```

## mc 命令指南

```
ls       列出文件和文件夹。     
mb       创建一个存储桶或一个文件夹。     
cat      显示文件和对象内容。     
pipe     将一个STDIN重定向到一个对象或者文件或者STDOUT。     
share    生成用于共享的URL。     
cp       拷贝文件和对象。     
mirror   给存储桶和文件夹做镜像。     
find     基于参数查找文件。     
diff     对两个文件夹或者存储桶比较差异。     
rm       删除文件和对象。     
events   管理对象通知。     
watch    监听文件和对象的事件。     
policy   管理访问策略。     
session  为cp命令管理保存的会话。     
config   管理mc配置文件。     
update   检查软件更新。     
version  输出版本信息。
```

## mc 命令实践 

```
# 查看 minio 服务端配置     
mc config host ls     
     
# 添加服务端配置 名称为 minio
mc config host add minio  http://docker宿主机IP:9000  minioadmin minioadmin --api s3v4     
     
# 查看 minio bucket     
mc ls minio     
     
# 创建 bucket     
mc mb minio/backup     
     
# 上传本地目录(文件不加r)     
mc cp -r  ingress minio/backup/     
     
# 下载远程目录(文件不加r)     
mc cp -r  minio/backup .     
     
# 将一个本地文件夹镜像到minio(类似rsync)      
mc mirror localdir/ minio/backup/     
     
# 持续监听本地文件夹镜像到minio(类似rsync)      
mc mirror -w localdir/ minio/backup/     
     
# 持续从 minio 存储桶中查找所有 jpeg 图像，并复制到 minio "play/bucket" 存储桶     
mc find minio/bucket --name "*.jpg" --watch --exec "mc cp {} play/bucket"     
     
# 删除目录     
mc rm minio/backup/ingress --recursive --force     
     
# 删除文件     
mc rm minio/backup/service_minio.yaml     
     
# 从mybucket里删除所有未完整上传的对象     
mc rm  --incomplete --recursive --force play/mybucket     
     
# 删除7天前的对象     
mc rm --force --older-than=7 play/mybucket/oldsongs

# 压缩并上传
tar czvf - mango/uaa | mc pipe minio/backups/uaa.tar.gz    
     
# 将 MySQL 数据库 dump 文件输出到 minio     
mysqldump -u root -p ******* db | mc pipe minio/backups/backup.sql     
     
# mongodb 备份     
mongodump -h mongo-server1 -p 27017 -d blog-data --archive | mc pipe minio1/mongobkp/backups/mongo-blog-data-`date +%Y-%m-%d`.archive     

```