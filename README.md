# olz

#### 介绍
# 脚本远程下载 使用 curl 下载
# 短网址 (https://dwz.cn/)

#### SSH秘钥登录

    curl -sSL https://dwz.cn/UYkguaOp -o ssh-key-gen.sh
    chmod +x ssh-key-gen.sh

    Example:
      ./ssh-key-gen.sh 10.0.0.100 root $PASS
      ./ssh-key-gen.sh '192.168.0.71 192.168.0.72 192.168.0.73 192.168.0.74' root $PASS 22

#### Docker

> 安装 [ docker | docker-compose | harbor ]

    curl -sSL https://dwz.cn/XOJj0Njx -o install-docker.sh
    chmod +x install-docker.sh
    
      ./install-docker.sh -h
      ./install-docker.sh -i docker -v 19.03.3

    Example:
      bash <(curl -sSL https://gitee.com/yx571304/olz/raw/master/shell/docker/install.sh) -i docker
      bash <(curl -sSL https://gitee.com/yx571304/olz/raw/master/shell/docker/install.sh) -i compose
      bash <(curl -sSL https://gitee.com/yx571304/olz/raw/master/shell/docker/install.sh) -i harbor

    
#### MySQL

> mysql-5.7 安装(yum方式)

````shell
curl -sSL https://dwz.cn/gfcnHqGS -o install-mysql.sh
chmod +x install-mysql.sh


Example:
    ./install-mysql.sh --get-version         # 查看存储库中的软件版本
    ./install-mysql.sh --active install --data-dir /home/hadoop/mysql --version 5.7.23 --root-pass 123abc@DEF

      --help       # 查看帮助信息
      --data-dir   # mysql 数据存储目录默认为 /home/hadoop/mysql
      --version    # 指定安装版本
      --root-pass  # root 密码
      --bin-log    # 是否启用二进制日志(二进制日志只保留7天) 默认不启用
````

> 使用 xtrabackup 备份 MySQL

 - 安装 percona-xtrabackup

````shell
wget https://www.percona.com/downloads/Percona-XtraBackup-2.4/Percona-XtraBackup-2.4.15/binary/redhat/7/x86_64/percona-xtrabackup-24-2.4.15-1.el7.x86_64.rpm
yum localinstall percona-xtrabackup-24-2.4.15-1.el7.x86_64.rpm
````

 - 备份脚本

````shell
curl -sSL https://dwz.cn/8oTDN52s -o innobackup.sh              # 备份脚本
curl -sSL https://dwz.cn/pYZqbPiq -o send_mail.py               # 邮件告警
chmod +x innobackup.sh send_mail.py

Example:
    ./innobackup.sh --help

    ./innobackup.sh --mode 1 --data-dir /home/backup

    ./innobackup.sh --mode 1 --mail true --mail-addr example@domain.com

    ./innobackup.sh --mode 1 --remoute-bak true --remoute-server root@192.168.0.71 --remoute-dir /home/backupmysql
````

 - 加入定时任务

````shell
mkdir /script
mv innobackup.sh send_mail.py  /script
chmod +x /script/send_mail.py /script/innobackup.sh
crontab -l > /tmp/crontab.tmp
echo "10 1 * * * /script/innobackup.sh --mode 2 --data-dir /home/backup" >> /tmp/crontab.tmp
cat /tmp/crontab.tmp | uniq > /tmp/crontab
crontab /tmp/crontab
rm -f /tmp/crontab.tmp /tmp/crontab
````