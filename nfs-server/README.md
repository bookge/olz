- NFS Server Dockerfile <https://github.com/vgist/dockerfiles/tree/master/nfs-server>

- NFS Server Dockerfile <https://github.com/sjiveson/nfs-server-alpine>

- Docker Volumes Plugs Convoy <https://github.com/rancher/convoy>

- Blog idriss <https://www.jianshu.com/p/666a2d99f92b>

#### Environment:

|   Environment      |     Value     |
|--------------------|---------------|
| SHARED_DIRECTORY_0 | /nfs *(fsid=0,rw,sync,insecure,all_squash,no_subtree_check,nohide) |
| SHARED_DIRECTORY_1 | /nfs/share *(rw,sync,insecure,all_squash,anonuid=65534,anongid=65534,no_subtree_check,nohide) |
| SHARED_DIRECTORY_2 | /nfs/nfsfile *(rw,sync,insecure,all_squash,anonuid=65534,anongid=65534,no_subtree_check,nohide) |

#### Build image:

    docker build -t nfs-server:alpine .

#### Custom usage:

    docker run \
        -d \
        --name nfs-server \
        --cap-add=SYS_ADMIN,SETPCAP \
        -p 2049:2049 \
        -v /home/data:/nfs \
        -v /home/share:/nfs/share \
        -e "SHARED_DIRECTORY_0=/nfs *(fsid=0,rw,sync,insecure,all_squash,anonuid=65534,anongid=65534,no_subtree_check,nohide)"
        -e "SHARED_DIRECTORY_1=/nfs/share 192.168.2.0/24(rw,sync,no_wdelay,insecure,all_squash,anonuid=65534,anongid=65534,no_subtree_check,nohide)" 
        nfs-server:alpine

#### Compose example:

    nfs-server:
      build: .
      container_name: nfs-server
      restart: always
      privileged: true
      ports:
        - "2049:2049"
      cap_add:
        - SYS_ADMIN
        - SETPCAP
      environment:
        - SHARED_DIRECTORY_0=/nfs *(fsid=0,rw,sync,insecure,all_squash,anonuid=65534,anongid=65534,no_subtree_check,nohide)
        - SHARED_DIRECTORY_1=/data/share 192.168.2.0/24(rw,sync,no_wdelay,insecure,all_squash,anonuid=65534,anongid=65534,no_subtree_check,nohide)
        - SHARED_DIRECTORY_2=/data/nfsfile 192.168.2.0/24(rw,sync,no_wdelay,insecure,all_squash,anonuid=65534,anongid=65534,no_subtree_check,nohide)
      volumes:
        - /data/nfs:/nfs
        - /data/share:/data
    restart: always

#### Client:

    # nfs v4
    mount -v -t nfs -o vers=4,rw,port=2049 192.168.2.71:/ /mnt/nfs
    mount -v -t nfs -o vers=4,rw,port=2049 192.168.2.71:/share /mnt/share
    mount -v -t nfs -o vers=4,rw,port=2049 192.168.2.71:/nfsfile /mnt/nfsfile

    # mount
    mount --types nfs --options nolock,proto=tcp,port=2049 192.168.2.71:/share /mnt/share

    # fstab
    192.168.2.71:/share  /mnt/share            nfs defaults,_netdev    0 0

#### virtual machine

    # CentOS server
    yum -y install nfs-utils

    mkdir -p /data /test /nfsfile
    chown -R 65534:65534 /data /test /nfsfile

    echo '/data 192.168.2.*(rw,sync,no_wdelay,insecure,all_squash,anonuid=65534,anongid=65534,no_subtree_check,nohide)' > /etc/exports
    echo '/test 192.168.2.*(rw,sync,insecure ,all_squash,no_subtree_check)' >> /etc/exports
    echo '/nfsfile 192.168.2.*(rw,sync,insecure ,all_squash)' >> /etc/exports
    systemctl restart rpcbind

    # CentOS client
    mkdir -p /tmp/nfs1 /tmp/nfs2 /tmp/nfs3
    mount -v -o vers=4,rw 192.168.2.71:/data /tmp/nfs1
    mount -v -o vers=4,rw 192.168.2.71:/test /tmp/nfs2
    mount -v -o vers=4,rw 192.168.2.71:/nfsfile /tmp/nfs3
