### prometheus+grafana+alertmanager

### Server端运行

> 创建数据目录 && 授权

    mkdir -p /data/grafana /data/prometheus /data/alertmanager
    chown -Rf 472:472 /data/grafana
    chown -Rf 65534:65534 /data/prometheus /data/alertmanager

> 启动

    docker-compose -f docker-compose.yml up -d

### 被监控端(docker)

    docker-compose -f node-exporter-cadvisor.yml up -d

### Dashboard

    # node-exporter: 8919 
