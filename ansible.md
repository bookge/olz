### ansible

解析配置文件中主 机名/变量 生成hosts

> cat hosts

```conf
[zookeeper]
192.168.0.71 node_name=zk-1
192.168.0.72 node_name=zk-2
192.168.0.73 node_name=zk-3

[kafka]
192.168.0.81 node_name=kafka-1
192.168.0.82 node_name=kafka-2
192.168.0.83 node_name=kafka-3

[hadoop]
192.168.0.91 node_name=hdp-1
192.168.0.92 node_name=hdp-2
192.168.0.93 node_name=hdp-3
```

> 解析所有分组自动去重 # cat hosts.j2

```j2
127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
::1         localhost localhost.localdomain localhost6 localhost6.localdomain6

{% set node_name = {} %}
{% for group in group_names %}
  {% for host in groups[group] %}
     {% if "node_name" in hostvars[host] %}
       {% set _dummy = node_name.update({hostvars[host]['inventory_hostname']:hostvars[host].node_name}) %}
     {% endif %}
  {% endfor %}
{% endfor %}

# cluster
{% for ip, name in node_name.iteritems() %}
{{ ip }} {{ name }}
{% endfor %}
```

> 解析指定分组

```j2
{% for host in groups['nodename'] %}
{{ hostvars[host]['ansible_facts']['eth0']['ipv4']['address'] }} {{ hostvars[host].node_name }}
{% endfor %}
```

when 执行条件[官方文档](https://docs.ansible.com/ansible/latest/user_guide/playbooks_conditionals.html)

> 系统为 CentOS 并且系统版本为 6

```yml
tasks:
  - name: "shut down CentOS 6 systems"
    command: /sbin/shutdown -t now
    when:
      - ansible_facts['distribution'] == "CentOS"
      - ansible_facts['distribution_major_version'] == "6"
```

> hosts 文件存在 hadooo 分组存在并且分组下存在主机则引入 hadoop.yml

```yml
- import_tasks: hadoop.yml
  when:
    - "groups['hadoop'] is defined"
    - "groups['hadoop']|length > 0"
```

> 如果系统是 CentOS 版本等于 6 或者 系统是 Debian 版本等于 7 执行关机

```yml
tasks:
  - name: "shut down CentOS 6 and Debian 7 systems"
    command: /sbin/shutdown -t now
    when: (ansible_facts['distribution'] == "CentOS" and ansible_facts['distribution_major_version'] == "6") or
          (ansible_facts['distribution'] == "Debian" and ansible_facts['distribution_major_version'] == "7")
```

检测服务器IP

> ansible-playbook -i zabbix check.yml

```yml
---
- hosts: zabbix
  tasks:
  - name: Selinux dsiable
    lineinfile:
      dest: /etc/selinux/config
      regexp: '^SELINUX='
      line: 'SELINUX=disabled'

  - name: ansible_default_ipv4
    debug: msg="{{ ansible_host }} {{ hostvars[inventory_hostname].ansible_default_ipv4.address }} {{ hostvars[inventory_hostname].ansible_default_ipv4.macaddress }}"

  - name: ansible_eth0.ipv4
    debug: msg="{{ ansible_host }} {{ hostvars[inventory_hostname].ansible_eth0.ipv4.address }} {{ hostvars[inventory_hostname].ansible_eth0.macaddress }}"
```