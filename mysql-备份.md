> mysqldump

```shell
cat > /script/mysql-backup.sh <<'EOF'
#!/bin/bash

mysql_port=3306
mysql_user=root
mysql_passwd=xxxxx
mysql_server=localhost
mysql_backup_db=all
backup_storage_dir=/home/data/mysql_backup
backup_log_file=/home/data/mysql_backup/mysql-backup.txt
mysql_connect_parameter="mysqldump -h $mysql_server -P $mysql_port -u$mysql_user -p$mysql_passwd"

echo "----------------------------- Backup MySQL Time: $(date +'%F %T') -----------------------------" >> ${backup_log_file}
if [ "$mysql_backup_db" == 'all' ]; then
    echo "  DBName: $db" >> ${backup_log_file}
    echo "  Start Time: $(date +'%F %T')" >> ${backup_log_file}
    backup_file_name="all-$(date +%Y%m%d%H%M).sql.gz"
    $mysql_connect_parameter --flush-privileges --single-transaction --flush-logs --triggers --routines --events --hex-blob  --all-databases | gzip > $backup_storage_dir/${backup_file_name}
    if [ $? -eq 0 ]; then
        state=Success
    else
        state=Failed
    fi
    echo "  DBName: $db" >> ${backup_log_file}
    echo "  DBSize: $(ls $backup_storage_dir/${backup_file_name} -lh | awk '{print $5}')" >> ${backup_log_file}
    echo "  DBFile: ${backup_file_name}" >> ${backup_log_file}
    echo "  State: $state" >> ${backup_log_file}
    echo "  End Time: $(date +'%F %T')" >> ${backup_log_file}
else
    for db in $mysql_backup_db; do
        backup_file_name="${db}-$(date +%Y%m%d%H%M).sql.gz"
        echo "  DBName: $db" >> ${backup_log_file}
        echo "  Start Time: $(date +'%F %T')" >> ${backup_log_file}
        $mysql_connect_parameter --flush-privileges --single-transaction --flush-logs --triggers --routines --events --hex-blob -B $db | gzip > $backup_storage_dir/${backup_file_name}
        if [ $? -eq 0 ]; then
            state=Success
        else
            state=Failed
        fi
        echo "  DBName: $db" >> ${backup_log_file}
        echo "  DBSize: $(ls $backup_storage_dir/${backup_file_name} -lh | awk '{print $5}')" >> ${backup_log_file}
        echo "  DBFile: ${backup_file_name}" >> ${backup_log_file}
        echo "  State: $state" >> ${backup_log_file}
        echo "  End Time: $(date +'%F %T')" >> ${backup_log_file}
    done
fi

echo "" >> ${backup_log_file}
echo "" >> ${backup_log_file}

# 还原
# gunzip < db.sql.gz | mysql -hlocalhost -uroot -pxxxxx

# crontab -e
# 0 1 * * * /script/mysql-backup.sh
EOF

chmod +x /script/mysql-backup.sh
```