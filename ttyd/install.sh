#!/bin/bash

wget -k https://github.com/tsl0922/ttyd/releases/download/1.6.3/ttyd.x86_64
chmod +x ttyd.x86_64
cp ./ttyd.x86_64 /usr/local/bin/ttyd

cat <<EOF > /etc/systemd/system/ttyd.service
[Unit]
Description=TTYD
After=syslog.target
After=network.target

[Service]
ExecStart=/usr/local/bin/ttyd --port 7681 -P 10 -s 9 --base-path /home -t 'theme={"foreground": "green"}' -t cursorStyle=bar -t lineHeight=1.3 -t fontSize=18 -t titleFixed="bash" -t cursorBlink=true -t cursorStyle=200 -c admin:admin login
Type=simple
Restart=always
User=root
Group=root

[Install]
WantedBy=multi-user.target
EOF

echo "http://0.0.0.0:7681/home"
echo "admin/admin"
